Traceit - Personal Budget system

Requirements:
 
* MariaDB latest, https://mariadb.com/downloads

* NodeJS latest which comes with npm

* Gradle latest, from https://gradle.org/releases (in case your IDE does not have it)
    * install it and configure IDE to use it

* create database, user and grant options (API user for queries);  
	    CREATE DATABASE kotlin_db default CHARACTER set utf8   default COLLATE utf8_general_ci;  
		CREATE USER 'kotlin'@'localhost' IDENTIFIED BY 'kotlin';  
		GRANT SELECT, INSERT, UPDATE, DELETE, ALTER, INDEX, DROP, CREATE ON kotlin_db.* TO 'kotlin'@'localhost';
		

Worth reading:

* https://medium.com/@fastlane80/setup-react-js-with-npm-babel-6-and-webpack-in-under-1-hour-1a714f973506

* https://www.fullstackreact.com/articles/what-are-babel-plugins-and-presets/

* conditional rendering: https://reactjs.org/docs/conditional-rendering.html

* form validator: https://www.npmjs.com/package/react-material-ui-form-validator

* icons: https://material.io/tools/icons/?style=baseline

* good real world example: 
   - https://github.com/gothinkster/react-redux-realworld-example-app
   - https://scotch.io/tutorials/create-a-simple-to-do-app-with-react
   - https://react.rocks/tag/MaterialDesign
   - https://rafaelhz.github.io/react-material-admin-template/#/dashboard?_k=0cwse8
   - https://github.com/rafaelhz/react-material-admin-template
   - http://jasonwatmore.com/post/2017/09/16/react-redux-user-registration-and-login-tutorial-example#private-route-jsx
   - social authentication - https://codeburst.io/react-authentication-with-twitter-google-facebook-and-github-862d59583105
   