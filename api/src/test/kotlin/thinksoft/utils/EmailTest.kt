package thinksoft.utils

import org.junit.Assert.assertEquals
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import thinksoft.entities.User
import thinksoft.entities.dto.UserDTO
import thinksoft.thread.EmailThread

//@RunWith(SpringRunner::class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmailTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    internal val AUTH_URL = "https://not-to-be-used.co.nz/"

//    @Test
    fun testSendEmailViaSendGrid() {
        val emailTo = "ander.dev@gmail.com"
        val emailFrom = "contact@traceit.nz"
        val nameFrom = "Anderson Santos"
        val subject = "Hello World"
        val body = "My first email with SendGrid Java!"

        Utils.sendGridEmail(emailTo, emailFrom, nameFrom, subject, body)
    }

//    @Test
    fun testEMailSuperUser() {

        val getUserResponse = testRestTemplate.withBasicAuth("test", "test").getForEntity("/rest/user/getbyemail/contact@thinksoft.co.nz", UserDTO::class.java)
        val userDTO = getUserResponse.body
        val user = User.fromDTO(userDTO)
        user.authenticationUrl = AUTH_URL

        val response = EmailThread.email(user, "SUPER_USER")
        assertEquals(response.code, 200 )
    }

//    @Test
    fun testEMailUser() {

        val getUserResponse = testRestTemplate.withBasicAuth("test", "test").getForEntity("/rest/user/getbyemail/contact@thinksoft.co.nz", UserDTO::class.java)
        val userDTO = getUserResponse.body
        val user = User.fromDTO(userDTO)
        user.authenticationUrl = AUTH_URL

        val response = EmailThread.email(user, "USER")
        assertEquals(response.code, 200 )
    }
}