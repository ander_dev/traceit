package thinksoft.utils

import org.junit.Test

//@RunWith(SpringRunner::class)
class CryptTest {

    @Test
    fun testCryptography() {
        val toEncrypt = "123456"
        val encrypted = Crypt.encrypt(toEncrypt)
        assert(encrypted == "MTIzNDU2")
        val decrypted = Crypt.decrypt(encrypted)
        assert(decrypted == toEncrypt)
    }
}