package thinksoft

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import thinksoft.entities.Currency
import thinksoft.entities.TypeAccount
import thinksoft.entities.TypeClosure
import thinksoft.entities.User
import thinksoft.entities.dto.*
import java.io.File
import java.nio.file.FileSystems

open class CucumberRoot {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    companion object {
        lateinit var userDTO: UserDTO
        var superUserDTO: SuperUserDTO? = null
        lateinit var responseDTO: ResponseDTO
        lateinit var accountDTO: AccountDTO
        lateinit var descriptionDTO: DescriptionDTO
        lateinit var creditCardDTO: CreditCardDTO
        lateinit var registerDTO: RegisterDTO
    }

    private val headers = HttpHeaders()
    private val stringLength = 10
    private val credentials = listOf("test","test")

    private var charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    val mapper = jacksonObjectMapper().registerModule(JavaTimeModule()).disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)!!

    private fun mapJsonToObject(fileName: String): Any {
        val path = FileSystems.getDefault().getPath("src/test/kotlin/thinksoft/data", "$fileName.json")

        fileName.split("-").forEach {

            return when (it) {
                "user" -> mapper.readValue(File(path.toString())) as UserDTO
                "account" -> mapper.readValue(File(path.toString())) as AccountDTO
                "description" -> mapper.readValue(File(path.toString())) as DescriptionDTO
                "creditcard" -> mapper.readValue(File(path.toString())) as CreditCardDTO
                "register" -> mapper.readValue(File(path.toString())) as RegisterDTO
                "income" -> mapper.readValue(File(path.toString())) as TypeAccount
                "expense" -> mapper.readValue(File(path.toString())) as TypeAccount
                "group" -> mapper.readValue(File(path.toString())) as TypeAccount
                "supergroup" -> mapper.readValue(File(path.toString())) as TypeAccount
                else -> ""
            }
        }
        return ""
    }

    fun getDTO(fileName: String): Any {
        return mapJsonToObject(fileName)
    }

    fun randomString(): String {
        return (1..stringLength)
                .map { i -> kotlin.random.Random.nextInt(0, charPool.size) }
                .map(charPool::get)
                .joinToString("")
    }

    fun <T : Any> T?.notNull(f: (it: T) -> Unit) {
        if (this != null) f(this)
    }

    fun post(obj: Any, url: String): ResponseDTO{
        headers.contentType = MediaType.APPLICATION_JSON
        val entity = HttpEntity(obj, headers)
        var result = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).postForEntity(url, entity, ResponseDTO::class.java)
        return result.body as ResponseDTO
    }

    fun delete(id: Long, userId: Long, url: String): ResponseDTO{
        var result = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).exchange("$url/$id/$userId", HttpMethod.DELETE, HttpEntity(User), ResponseDTO::class.java)
        return  result.body as ResponseDTO
    }

    @Deprecated("use delete with userId as param")
    fun delete(id: Long, url: String): ResponseDTO{
        var result = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).exchange("$url/$id", HttpMethod.DELETE, HttpEntity(User), ResponseDTO::class.java)
        return  result.body as ResponseDTO
    }

    fun listUsers(id: Long, url: String): MutableList<UserDTO>{
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$id", List::class.java)

        var retUsers: MutableList<UserDTO> = ArrayList()
        resp.body.forEach {
            val json = mapper.writeValueAsBytes(it)
            var dto = mapper.readValue(json, UserDTO::class.java)
            retUsers.add(dto)
        }
        return retUsers
    }

    fun listAccountsByUserId(id: Long, url: String): MutableList<AccountDTO>{
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$id", List::class.java)

        var retAccounts: MutableList<AccountDTO> = ArrayList()
        resp.body.forEach {
            val json = mapper.writeValueAsBytes(it)
            var dto = mapper.readValue(json, AccountDTO::class.java)
            retAccounts.add(dto)
        }
        return retAccounts
    }

    fun listRegistersByUserId(id: Long, url: String): MutableList<RegisterDTO>{
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$id", List::class.java)

        var retRegisters: MutableList<RegisterDTO> = ArrayList()
        resp.body.forEach {
            val json = mapper.writeValueAsBytes(it)
            var dto = mapper.readValue(json, RegisterDTO::class.java)
            retRegisters.add(dto)
        }
        return retRegisters
    }

    fun listRegistersByUserIdAndInstalmentId(userId: Long, instalmentId: Long, url: String): MutableList<RegisterDTO>{
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$userId/$instalmentId", List::class.java)

        val retRegisters: MutableList<RegisterDTO> = ArrayList()
        resp.body.forEach {
            val json = mapper.writeValueAsBytes(it)
            var dto = mapper.readValue(json, RegisterDTO::class.java)
            retRegisters.add(dto)
        }
        return retRegisters
    }

    fun listDescriptionsByUserId(id: Long, url: String): MutableList<DescriptionDTO>{
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$id", List::class.java)

        val retDescriptions: MutableList<DescriptionDTO> = ArrayList()
        resp.body.forEach {
            val json = mapper.writeValueAsBytes(it)
            var dto = mapper.readValue(json, DescriptionDTO::class.java)
            retDescriptions.add(dto)
        }
        return retDescriptions
    }

    fun listCreditCardByUserId(id: Long, url: String): MutableList<CreditCardDTO>{
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$id", List::class.java)

        var retDescritions: MutableList<CreditCardDTO> = ArrayList()
        resp.body.forEach {
            val json = mapper.writeValueAsBytes(it)
            var dto = mapper.readValue(json, CreditCardDTO::class.java)
            retDescritions.add(dto)
        }
        return retDescritions
    }

    fun listDescriptionsByUserIdAndDescription(id: Long, description: String, url: String): ResponseDTO {
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$id/$description", ResponseDTO::class.java)

        return resp.body as ResponseDTO
    }

    fun findUserById(id: Long, url: String): UserDTO {
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$id", UserDTO::class.java)
        return resp.body as UserDTO
    }

    fun findTypeAccountById(id: Long, url: String): TypeAccount {
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$id", TypeAccount::class.java)
        return resp.body as TypeAccount
    }

    fun findTypeClosureByDescription(description: String, url: String): TypeClosure {
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$description", TypeClosure::class.java)
        return resp.body as TypeClosure
    }

    fun findCurrencyByAcronAndLocale(acronym: String, locale: String, url: String): Currency {
        val resp = testRestTemplate.withBasicAuth(credentials[0], credentials[1]).getForEntity("$url/$acronym/$locale", Currency::class.java)
        return resp.body as Currency
    }

    fun getUserId(userType: String): Long {
        return when (userType) {
            "SUPER_USER" -> {
                superUserDTO!!.id!!
            }
            else -> {
                userDTO!!.id!!
            }
        }
    }

    fun saveUserByType(userType: String): UserDTO {
        var retDTO: UserDTO
        when (userType) {
            "SUPER_USER" -> {
                retDTO = UserDTO.toDTO(superUserDTO!!)
                retDTO.superUser = null
            }
            else -> {
                retDTO = userDTO
            }
        }
        return retDTO
    }

    fun deserializeObjectWithList(obj: Any, objectReturn: String): Any {

        val json = mapper.writeValueAsBytes(obj)

        return when (objectReturn) {
            "DESCRIPTION" -> {
                mapper.readValue(json, DescriptionDTO::class.java)
            } else -> ""
        }
    }

}