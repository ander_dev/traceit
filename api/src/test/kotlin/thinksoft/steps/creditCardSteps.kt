package thinksoft.steps

import cucumber.api.Scenario
import cucumber.api.java8.En
import org.junit.Assert
import org.junit.Assert.assertNotSame
import org.junit.Assert.assertSame
import thinksoft.entities.dto.CreditCardDTO
import thinksoft.entities.dto.UserDTO
import thinksoft.service.CreditCardStepsService

var creditCardLastInstance: CreditCardSteps? = null

class CreditCardSteps : En, CreditCardStepsService() {
    var userCreditCards: MutableList<CreditCardDTO> = arrayListOf()

    var tempUserDTO: UserDTO? = null

    init {

        Before { scenario: Scenario ->
            assertNotSame(this, creditCardLastInstance)
            creditCardLastInstance = this
        }

        After { scenario: Scenario ->
            assertSame(this, creditCardLastInstance)
            creditCardLastInstance = this
        }

        Given("a new credit card is created using {string}") { minimum: String ->
            creditCardDTO = getDTO(minimum) as CreditCardDTO
        }

        When("credit card is saved") {
            responseDTO = save(creditCardDTO)
        }

        When("new credit card user is equals to {string}") {userType: String ->
            var userId = getUserId(userType)
            creditCardDTO.user = UserDTO(userId)
        }

        Given("new credit card for {string} is created") { userName: String ->
            var userId =  when (userName) {
                "Unit Test - Super User" -> {
                    1 // Unit test super User
                }
                else -> {
                    2 //Unit test User
                }
            }
            if(userId != null){
                tempUserDTO = findUserById(userId.toLong())
                creditCardDTO = getDTO("creditcard-minimum") as CreditCardDTO
                creditCardDTO.user = tempUserDTO
                creditCardDTO.description = "Test CreditCard"
            }

        }

        When("{string} list his credit cards") { userType: String ->
            var userId = getUserId(userType)
            userCreditCards = listCreditCardsByUserId(userId)
        }

        Then("user can only see {int} credit card") { creditCardsTotal: Int ->
            Assert.assertEquals(creditCardsTotal, userCreditCards.size)
            cleanUpTempUser(tempUserDTO!!)
        }
    }
}

