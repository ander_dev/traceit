package thinksoft.steps

import cucumber.api.Scenario
import cucumber.api.java8.En
import org.junit.Assert
import org.junit.Assert.assertNotSame
import org.junit.Assert.assertSame
import thinksoft.entities.dto.AccountDTO
import thinksoft.entities.dto.UserDTO
import thinksoft.service.AccountStepsService

var accountLastInstance: AccountSteps? = null

class AccountSteps : En, AccountStepsService() {
    var userAccounts: MutableList<AccountDTO> = arrayListOf()

    var tempUserDTO: UserDTO? = null

    init {

        Before { scenario: Scenario ->
            assertNotSame(this, accountLastInstance)
            accountLastInstance = this
        }

        After { scenario: Scenario ->
            assertSame(this, accountLastInstance)
            accountLastInstance = this
        }

        Given("a new account is created using {string}") { minimum: String ->
            accountDTO = getDTO(minimum) as AccountDTO
        }

        When("account is saved") {
            responseDTO = save(accountDTO)
        }

        When("new account user is equals to {string}") {userType: String ->
            var userId = getUserId(userType)
            accountDTO.user = UserDTO(userId)
        }

        Given("new account currency acronym is equals to {string}") { currencyAcron: String ->
            var userDTO = findUserById(accountDTO.user!!.id!!)
            accountDTO.currency = findCurrencyByAcronAndLocale(currencyAcron, userDTO.language!!)
        }

        Given("new account typeClosure is equals to {string}") { typeClosure: String ->
            accountDTO.typeClosure = findTypeClosureByDescription(typeClosure)
        }

        Given("new account for {string} is created") { userName: String ->
            var userId =  when (userName) {
                "Unit Test - Super User" -> {
                    1 // Unit test super User
                }
                else -> {
                    2 //Unit test User
                }
            }
            if(userId != null){
                tempUserDTO = findUserById(userId.toLong())
                accountDTO = getDTO("account-minimum") as AccountDTO
                accountDTO.user = tempUserDTO
                accountDTO.description = "Savings"
                accountDTO.currency = findCurrencyByAcronAndLocale("NZ$", tempUserDTO!!.language!!)
                accountDTO.typeClosure = findTypeClosureByDescription("Monthly")
            }

        }

        When("{string} list his accounts") { userType: String ->
            var userId = getUserId(userType)
            userAccounts = listAccountsByUserId(userId)
        }

        Then("user can only see {int} account") { accountsTotal: Int ->
            Assert.assertEquals(accountsTotal, userAccounts.size)
            cleanUpTempUser(tempUserDTO!!)
        }


    }
}

