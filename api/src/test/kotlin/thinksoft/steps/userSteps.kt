package thinksoft.steps

import cucumber.api.Scenario
import cucumber.api.java8.En
import org.junit.Assert
import org.junit.Assert.assertNotSame
import org.junit.Assert.assertSame
import thinksoft.entities.Role
import thinksoft.entities.dto.SuperUserDTO
import thinksoft.entities.dto.UserDTO
import thinksoft.service.UserStepsService

var userStepsLastInstance: UserSteps? = null

class UserSteps : En, UserStepsService() {

    init {

        Before { scenario: Scenario ->
            assertNotSame(this, userStepsLastInstance)
            userStepsLastInstance = this
        }

        After { scenario: Scenario ->
            assertSame(this, userStepsLastInstance)
            userStepsLastInstance = this
            cleanUp(superUserDTO!!.id!!)
            superUserDTO = null
        }

        Given("a new user is created using {string} as my user form") { minimum: String ->
            userDTO = getDTO(minimum) as UserDTO
        }

        Given("a new child user is created using {string} as my user form") { minimum: String ->
            val random = randomString()
            userDTO = getDTO(minimum) as UserDTO
            userDTO.name = "$random child user"
            userDTO.email = randomString() + "." + randomString() + "@gmail.com"
            userDTO.superUser = superUserDTO
        }

        When("user registration is submitted") {
            responseDTO = save(userDTO)
            if (responseDTO.obj != null){
                var localDTO = responseDTO.obj as UserDTO

                if (localDTO.superUser != null) {
                    superUserDTO = localDTO.superUser as SuperUserDTO
                    userDTO = localDTO
                } else {
                    superUserDTO ?: run {
                        superUserDTO = SuperUserDTO.toDTO(responseDTO.obj as UserDTO)
                    }
                }
            } else {
                Assert.fail(responseDTO.message)
            }
        }

        Then("user will receive a message saying {string} with statusCode {int}") { message: String, statusCode: Int ->
            Assert.assertEquals(responseDTO.statusCode, statusCode)
            Assert.assertEquals(responseDTO.message, message)
        }

        Then("user role should be {string}") { expectedRole: String ->
            val role = (responseDTO.obj as UserDTO).role as Role
            Assert.assertEquals(role.role, expectedRole)
        }

        Then("confirm that listing users of {string} we got a list size of {int}") { userType: String, sizeOf: Int ->
            var users: MutableList<UserDTO> = listUsersById(getUserId(userType))

            Assert.assertEquals(users.size, sizeOf)
        }

        Then("confirm that child users of parent user can be deleted") {
            var users: MutableList<UserDTO> = listUsersById(superUserDTO!!.id!!)
            Assert.assertTrue(users.size > 1)

            users.forEach {
                if (it.superUser != null) {
                    del(it.id!!)
                }
            }

            users = listUsersById(superUserDTO!!.id!!)
            Assert.assertTrue(users.size == 1)
        }

        When("name of {string} is set to {string}") { userType: String, value: String ->
            var localUserDTO: UserDTO = findById(getUserId(userType))
            localUserDTO.name = value

            responseDTO = save(localUserDTO)

            Assert.assertEquals(responseDTO.statusCode, 200)
        }

        Then("confirm that name of {string} is {string}") { userType: String, value: String ->
            var localUserDTO: UserDTO = findById(getUserId(userType))

            Assert.assertEquals(localUserDTO.name, value)
        }

        Given("email of {string} is set to {string}") { userType: String, value: String ->
            when (userType) {
                "SUPER_USER" -> {
                    superUserDTO!!.email = value
                }
                else -> {
                    userDTO.email = value
                }
            }
        }

        Given("password of {string} is set to {string}") { userType: String, value: String ->
            when (userType) {
                "SUPER_USER" -> {
                    superUserDTO!!.password = value
                }
                else -> {
                    userDTO.password = value
                }
            }
        }

        Then("defaultPassword of {string} is {string}") { userType: String, value: String ->
            var localUserDTO: UserDTO = findById(getUserId(userType))

            Assert.assertEquals(localUserDTO.defaultPassword.toString(), value)
        }

        Given("passwordConfirmation of {string} is set to {string}") { userType: String, value: String ->
            when (userType) {
                "SUPER_USER" -> {
                    superUserDTO!!.confirmPassword = value
                }
                else -> {
                    userDTO.confirmPassword = value
                }
            }
        }

        When("{string} is saved") { userType: String ->
            responseDTO = save(saveUserByType(userType))
        }

        Then("emailVerified of {string} is {string}") { userType: String, value: String ->
            var localUserDTO: UserDTO = findById(getUserId(userType))

            Assert.assertEquals(localUserDTO.emailVerified, value.toBoolean())
        }
    }
}

