package thinksoft.steps

import cucumber.api.Scenario
import cucumber.api.java8.En
import org.junit.Assert
import org.junit.Assert.assertNotSame
import org.junit.Assert.assertSame
import thinksoft.entities.dto.*
import thinksoft.service.RegisterStepsService
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

var registerLastInstance: RegisterSteps? = null

class RegisterSteps : En, RegisterStepsService() {
    var tempUserDTO: UserDTO? = null

    var tempRegisterDTO: RegisterDTO? = null

    var tempAccountDTO: AccountDTO? = null

    init {

        Before { scenario: Scenario ->
            assertNotSame(this, registerLastInstance)
            registerLastInstance = this
        }

        After { scenario: Scenario ->
            assertSame(this, registerLastInstance)
            if(tempRegisterDTO != null) del(tempRegisterDTO!!.id!!, tempRegisterDTO!!.user!!.id!!)
            if(tempAccountDTO != null) delAccount(tempAccountDTO!!.id!!, tempRegisterDTO!!.user!!.id!!)
            registerLastInstance = this
        }

        Given("a new register is created using {string}") { minimum: String ->
            registerDTO = getDTO(minimum) as RegisterDTO
        }

        When("register is saved") {
            responseDTO = save(registerDTO)
            if (responseDTO.obj != null ) registerDTO = responseDTO.obj as RegisterDTO
        }

        Given("new register user is equals to {string}") { userType: String ->
            var userId = getUserId(userType)
            registerDTO.user = UserDTO(userId)
        }

        Given("new register account is created with a total balance of {double}") { balance: Double ->
            var accountDTO = getDTO("account-test") as AccountDTO
            accountDTO.user = registerDTO.user
            accountDTO.totalGeneral = balance.toBigDecimal()
            responseDTO = saveAccount(accountDTO)
            registerDTO.account = responseDTO.obj as AccountDTO
        }

        Given("register credit card is created using {string} for current user") { minimum: String ->
            val creditCardDTO = getDTO(minimum) as CreditCardDTO
            creditCardDTO.user = registerDTO.user
            responseDTO = saveCreditcard(creditCardDTO)
            registerDTO.creditCard = responseDTO.obj as CreditCardDTO
        }

        Given("typeAccount of the register is {string}") { typeAccount: String ->
            when (typeAccount) {
                "EXPENSE" -> {
                    registerDTO.typeAccount = findTypeAccountById(2L)
                }
                "INCOME" -> {
                    registerDTO.typeAccount = findTypeAccountById(1L)
                }
                else -> Assert.fail("typeAccount does not match")
            }

        }

        Given("register amount has value of {double}") { amount: Double ->
            registerDTO.amount = amount.toBigDecimal()
        }

        Given("register description is {string}") { description: String ->
            registerDTO.description = DescriptionDTO.toDTO(description)
        }

        Given("register group is {string}") { group: String ->
            registerDTO.group = DescriptionDTO.toDTO(group)
        }

        Given("register super group is {string}") { superGroup: String ->
            registerDTO.superGroup = DescriptionDTO.toDTO(superGroup)
        }

        Given("register instalments is equals to {int}") { instalments: Int ->
            registerDTO.nrInstalment = instalments
        }

        Given("register saved contains {int} instalments each one with amount equals to {double} divided by instalments") { instalments: Int, amount: Double ->
            var register = responseDTO.obj as RegisterDTO
            var registers = listRegistersByUserIdAndInstalmentId(register.user!!.id!!, register.instalment!!.id!!)

            Assert.assertTrue(registers.size == instalments)

            registers.forEach {
                Assert.assertEquals(it.amount!!.setScale(2, RoundingMode.HALF_UP), amount.toBigDecimal().divide(BigDecimal(instalments), 2, RoundingMode.HALF_UP))
            }
        }

        Given("register saved contains {int} monthly instalments from {string}") { instalments: Int, today: String ->
            val register = responseDTO.obj as RegisterDTO
            val registers = listRegistersByUserIdAndInstalmentId(register.user!!.id!!, register.instalment!!.id!!)

            var formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
            var date = LocalDate.parse(today, formatter)
            val today = date.atStartOfDay(ZoneId.of(register.user!!.timeZoneName))

            val dates: MutableList<ZonedDateTime> = ArrayList()
            for (x in 0 until registers.size){
                if(x == 0) dates.add(today)
                else dates.add( today.plusMonths(x.toLong()))
            }

            Assert.assertTrue(registers.size == instalments)
            for (x in 0 until registers.size){
                val testedDate = registers[x].date!!.withZoneSameInstant(ZoneId.of(register.user!!.timeZoneName))
                println("testedDate: $testedDate")
                Assert.assertEquals(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(testedDate), DateTimeFormatter.ofPattern("dd/MM/yyyy").format(dates[x]))
            }
        }

        Given("register date is {string}") { date: String ->
            val user = findUserById(registerDTO.user!!.id!!)
            val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
            val date = LocalDate.parse(date, formatter)
            val parsed = date.atStartOfDay(ZoneId.of(user.timeZoneName))
            registerDTO.date = parsed
        }

        Given("register saved date is equals to {string}") { date: String ->
            val register = responseDTO.obj as RegisterDTO
            register.date = register.date!!.withZoneSameInstant(ZoneId.of(register.user!!.timeZoneName))
            val registerDate = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(register.date)
            Assert.assertEquals(registerDate, date)
        }

        Given("register saved accounted is equals to {string}") { accounted: String ->
            Assert.assertEquals(registerDTO.accounted, accounted.toBoolean())
        }

        Given("account balance is equals to {double}") { balance: Double ->
            Assert.assertEquals(registerDTO.account!!.totalGeneral!!.setScale(2), balance.toBigDecimal().setScale(2))
        }

        Given("list of registers for {string} should contain {int} registers") { userType: String, nrRegisters: Int ->
            var userId = getUserId(userType)
            var registers = listRegistersByUserId(userId)
            Assert.assertEquals(registers.size, nrRegisters)
        }

        Given("register amount is updated to {double}") { value: Double ->
            registerDTO.amount = value.toBigDecimal()
        }

        Given("new register for {string} is created") { userName: String ->
            var userId =  when (userName) {
                "Unit Test - Super User" -> {
                    1 // Unit test super User
                }
                else -> {
                    2 //Unit test User
                }
            }
            tempUserDTO = findUserById(userId.toLong())
            tempRegisterDTO = getDTO("register-minimum") as RegisterDTO
            tempRegisterDTO!!.user = tempUserDTO
            tempRegisterDTO!!.description = DescriptionDTO.toDTO("Description test User")
            tempRegisterDTO!!.group = DescriptionDTO.toDTO("Description group test User")
            tempRegisterDTO!!.superGroup = DescriptionDTO.toDTO("Description super group test User")
            tempAccountDTO = getDTO("account-test") as AccountDTO
            tempAccountDTO!!.user = tempUserDTO
            tempAccountDTO!!.totalGeneral = BigDecimal(1000.00)
            responseDTO = saveAccount(tempAccountDTO!!)
            tempAccountDTO = responseDTO.obj as AccountDTO
            tempRegisterDTO!!.account = responseDTO.obj as AccountDTO
            tempRegisterDTO!!.typeAccount = findTypeAccountById(2L)
            tempRegisterDTO!!.amount = BigDecimal(207.77)
            responseDTO = save(tempRegisterDTO!!)
            tempRegisterDTO = responseDTO.obj as RegisterDTO
        }
    }
}

