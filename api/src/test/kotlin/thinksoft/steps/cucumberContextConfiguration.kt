package thinksoft.steps

import cucumber.api.Scenario
import cucumber.api.java8.En
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import thinksoft.Application

@SpringBootTest(classes = arrayOf(Application::class), webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration
class CucumberContextConfiguration : En {

    init {
        Before { scenario: Scenario ->
            // just a hack to get SpringContext working for every step definition
        }
    }
}

