package thinksoft.steps

import cucumber.api.Scenario
import cucumber.api.java8.En
import org.junit.Assert
import org.junit.Assert.assertNotSame
import org.junit.Assert.assertSame
import thinksoft.entities.TypeAccount
import thinksoft.entities.dto.DescriptionDTO
import thinksoft.entities.dto.UserDTO
import thinksoft.service.DescriptionStepsService

var descriptionLastInstance: DescriptionSteps? = null

class DescriptionSteps : En, DescriptionStepsService() {
    var userDescriptions: MutableList<DescriptionDTO> = arrayListOf()

    var tempUserDTO: UserDTO? = null

    init {

        Before { scenario: Scenario ->
            assertNotSame(this, descriptionLastInstance)
            descriptionLastInstance = this
        }

        After { scenario: Scenario ->
            assertSame(this, descriptionLastInstance)
            descriptionLastInstance = this
        }

        Given("a new description is created using {string}") { minimum: String ->
            descriptionDTO = getDTO(minimum) as DescriptionDTO
        }

        When("description is saved") {
            responseDTO = save(descriptionDTO)
        }

        When("new description user is equals to {string}") {userType: String ->
            var userId = getUserId(userType)
            descriptionDTO.user = UserDTO(userId)
        }

        Given("new description typeAccount is equals to {string}") { typeAccount: String ->
            descriptionDTO.typeAccount = getTypeAccount(typeAccount)
        }

        Given("new description for {string} is created") { userName: String ->
            var userId =  when (userName) {
                "Unit Test - Super User" -> {
                    1 // Unit test super User
                }
                else -> {
                    2 //Unit test User
                }
            }
            if(userId != null){
                tempUserDTO = findUserById(userId.toLong())
                descriptionDTO = getDTO("description-minimum") as DescriptionDTO
                descriptionDTO.user = tempUserDTO
                descriptionDTO.description = "Test Description"
            }

        }

        When("{string} list his descriptions") { userType: String ->
            var userId = getUserId(userType)
            userDescriptions = listDescriptionsByUserId(userId)
        }

        Then("user can only see {int} description") { descriptionsTotal: Int ->
            Assert.assertEquals(descriptionsTotal, userDescriptions.size)
        }

        Given("new description typeAccountIdList contains {string}") { typeAccounts: String ->
            val parts = typeAccounts.split(",")
            val typeAccounts =  arrayListOf<TypeAccount>()
            parts.forEach { typeAccounts.add(getTypeAccount(it)) }
            descriptionDTO.typeAccountList = typeAccounts
        }

        Then("when {string} list description just created he will get {int} items, one for each typeAccount") { userType: String, items: Int ->
            var userId = getUserId(userType)
            var description = (responseDTO.obj as DescriptionDTO).description
            var respDTO = findDescriptionsByUserIdAndDescription(userId, description!!)

            var descriptionDTO = deserializeObjectWithList(respDTO.obj!!, "DESCRIPTION") as DescriptionDTO

            Assert.assertEquals(descriptionDTO.typeAccountList!!.size, items)
        }

        Then("description {string} exists for {string} and typeAccount {string}") { description: String, userType: String, typeAccount: String ->
            var userId = getUserId(userType)
            var respDTO = findDescriptionsByUserIdAndDescription(userId, description!!)
            var descriptionDTO = deserializeObjectWithList(respDTO.obj!!, "DESCRIPTION") as DescriptionDTO
            Assert.assertEquals(descriptionDTO.description, description)
            Assert.assertEquals(descriptionDTO.typeAccount!!, getTypeAccount(typeAccount))
        }

        Then("description {string} does not exists for {string} and {string} will be thrown") { description: String, userType: String, message: String ->
            var userId = getUserId(userType)
            var dto = findDescriptionsByUserIdAndDescription(userId, description!!)
            Assert.assertEquals(dto.message , message)
            Assert.assertEquals(dto.statusCode, 400)
        }

        Given("new description description is equals to {string}") { description: String ->
            descriptionDTO.description = description
        }

    }
}

