package thinksoft.service

import thinksoft.CucumberRoot
import thinksoft.entities.TypeAccount
import thinksoft.entities.dto.DescriptionDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.entities.dto.UserDTO

open class DescriptionStepsService : CucumberRoot() {

    fun save(descriptionDTO: DescriptionDTO): ResponseDTO {
        var response = post(descriptionDTO, BASE_DESCRIPTION_URL)
        val json = mapper.writeValueAsBytes(response.obj)
        val pojo = mapper.readValue(json, DescriptionDTO::class.java)
        response.obj = pojo
        return response
    }

    private fun del(id: Long, userId: Long): ResponseDTO {
        return delete(id, userId, BASE_DESCRIPTION_URL)
    }

    fun findUserById(id: Long): UserDTO {
        return findUserById(id, "$BASE_USER_URL/by-id")
    }

    fun findDescriptionsByUserIdAndDescription(id: Long, description: String): ResponseDTO {
        return listDescriptionsByUserIdAndDescription(id, description, "$BASE_DESCRIPTION_URL/by-description")
    }

    fun listDescriptionsByUserId(id: Long): MutableList<DescriptionDTO>{
        return listDescriptionsByUserId(id, "$BASE_DESCRIPTION_URL/by-user")
    }

    fun findTypeAccountById(id: Long): TypeAccount {
        return findTypeAccountById(id, "$BASE_TYPE_ACCOUNT_URL")
    }

    fun getTypeAccount(typeAccount: String): TypeAccount{
        return when (typeAccount) {
            "INCOME" -> getDTO("income-typeaccount") as TypeAccount
            "EXPENSE" -> getDTO("expense-typeaccount") as TypeAccount
            "GROUP" -> getDTO("group-typeaccount") as TypeAccount
            "SUPER_GROUP" -> getDTO("supergroup-typeaccount") as TypeAccount
            else -> getDTO("expense-typeaccount") as TypeAccount
        }
    }

    companion object {
        const val BASE_DESCRIPTION_URL = "/traceit/description"
        const val BASE_TYPE_ACCOUNT_URL = "/traceit/type-account"
        const val BASE_USER_URL = "/traceit/user"
    }
}