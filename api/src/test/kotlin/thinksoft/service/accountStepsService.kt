package thinksoft.service

import thinksoft.CucumberRoot
import thinksoft.entities.Currency
import thinksoft.entities.TypeClosure
import thinksoft.entities.dto.AccountDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.entities.dto.UserDTO

open class AccountStepsService : CucumberRoot() {

    fun save(accountDTO: AccountDTO): ResponseDTO {
        var response = post(accountDTO, BASE_ACCOUNT_URL)
        val json = mapper.writeValueAsBytes(response.obj)
        val pojo = mapper.readValue(json, AccountDTO::class.java)
        response.obj = pojo
        return response
    }

    private fun del(id: Long, userId: Long): ResponseDTO {
        return delete(id, userId, BASE_ACCOUNT_URL)
    }

    fun listAccountsByUserId(id: Long): MutableList<AccountDTO> {

        return listAccountsByUserId(id, "$BASE_ACCOUNT_URL/by-user")
    }

    fun cleanUpTempUser(userDTO: UserDTO) {

        if(userDTO != null){
            var accounts: MutableList<AccountDTO> = listAccountsByUserId(userDTO.id!!)

            accounts.forEach {
                del(it.id!!, userDTO.id!!)
            }
        }
    }

    fun findTypeClosureByDescription(description: String): TypeClosure {
        return findTypeClosureByDescription(description, "$BASE_TYPE_CLOSURE_URL/by-description")
    }

    fun findCurrencyByAcronAndLocale(acromym: String, locale: String): Currency {
        var currency = findCurrencyByAcronAndLocale(acromym, locale, "$BASE_CURRENCY_URL/by-acronym-locale")
        return currency
    }

    fun findUserById(id: Long): UserDTO {
        return findUserById(id, "$BASE_USER_URL/by-id")
    }

    companion object {
        const val BASE_ACCOUNT_URL = "/traceit/account"
        const val BASE_USER_URL = "/traceit/user"
        const val BASE_TYPE_CLOSURE_URL = "/traceit/type-closure"
        const val BASE_CURRENCY_URL = "/traceit/currency"
    }
}