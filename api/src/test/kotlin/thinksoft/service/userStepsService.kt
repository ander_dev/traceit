package thinksoft.service

import thinksoft.CucumberRoot
import thinksoft.entities.dto.ResponseDTO
import thinksoft.entities.dto.UserDTO

open class UserStepsService: CucumberRoot() {

    fun save(userDTO: UserDTO): ResponseDTO {
        var response = post(userDTO, BASE_URL)
        val json = mapper.writeValueAsBytes(response.obj)
        val pojo = mapper.readValue(json, UserDTO::class.java)
        response.obj = pojo
        return response
    }

    fun del(id: Long): ResponseDTO {
        return delete(id, BASE_URL)
    }

    fun cleanUp(userId: Long){

        var users: MutableList<UserDTO>  = listUsersById(userId)

        users.forEach {
            if (it.superUser != null) {
                del(it.id!!)
            }
        }
        del(userId)
    }

    fun listUsersById(id: Long): MutableList<UserDTO> {

        return listUsers(id, BASE_URL)
    }

    fun findById(id: Long): UserDTO {
        return findUserById(id, "$BASE_URL/by-id")
    }

    companion object {
        const val BASE_URL = "/traceit/user"
    }
}