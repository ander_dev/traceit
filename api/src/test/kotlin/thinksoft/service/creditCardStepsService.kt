package thinksoft.service

import thinksoft.CucumberRoot
import thinksoft.entities.dto.CreditCardDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.entities.dto.UserDTO

open class CreditCardStepsService : CucumberRoot() {

    fun save(creditCardDTO: CreditCardDTO): ResponseDTO {
        var response = post(creditCardDTO, BASE_CREDIT_CARD_URL)
        val json = mapper.writeValueAsBytes(response.obj)
        val pojo = mapper.readValue(json, CreditCardDTO::class.java)
        response.obj = pojo
        return response
    }

    private fun del(id: Long, userId: Long): ResponseDTO {
        return delete(id, userId, BASE_CREDIT_CARD_URL)
    }

    fun findUserById(id: Long): UserDTO {
        return findUserById(id, "$BASE_USER_URL/by-id")
    }

    fun listCreditCardsByUserId(id: Long): MutableList<CreditCardDTO>{
        return listCreditCardByUserId(id, "$BASE_CREDIT_CARD_URL/by-user")
    }

    fun cleanUpTempUser(userDTO: UserDTO) {

        if(userDTO != null){
            var cards: MutableList<CreditCardDTO> = listCreditCardsByUserId(userDTO.id!!)

            cards.forEach {
                del(it.id!!, userDTO.id!!)
            }
        }
    }

    companion object {
        const val BASE_CREDIT_CARD_URL = "/traceit/credit-card"
        const val BASE_USER_URL = "/traceit/user"
    }
}