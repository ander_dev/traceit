package thinksoft.service

import thinksoft.CucumberRoot
import thinksoft.entities.TypeAccount
import thinksoft.entities.dto.*

open class RegisterStepsService : CucumberRoot() {

    fun save(registerDTO: RegisterDTO): ResponseDTO {
        var response = post(registerDTO, BASE_REGISTER_URL)
        val json = mapper.writeValueAsBytes(response.obj)
        val pojo = mapper.readValue(json, RegisterDTO::class.java)
        response.obj = pojo
        return response
    }

    fun saveAccount(accountDTO: AccountDTO): ResponseDTO {
        var response = post(accountDTO, AccountStepsService.BASE_ACCOUNT_URL)
        val json = mapper.writeValueAsBytes(response.obj)
        val pojo = mapper.readValue(json, AccountDTO::class.java)
        response.obj = pojo
        return response
    }

    fun saveCreditcard(creditCardDTO: CreditCardDTO): ResponseDTO {
        var response = post(creditCardDTO, CreditCardStepsService.BASE_CREDIT_CARD_URL)
        val json = mapper.writeValueAsBytes(response.obj)
        val pojo = mapper.readValue(json, CreditCardDTO::class.java)
        response.obj = pojo
        return response
    }

    fun del(id: Long, userId: Long): ResponseDTO {
        return delete(id, userId, BASE_REGISTER_URL)
    }

    fun delAccount(id: Long, userId: Long): ResponseDTO {
        return delete(id, userId, BASE_ACCOUNT_URL)
    }

    fun delDescriptions(id: Long, userId: Long): ResponseDTO {
        return  delete(id, userId, BASE_DESCRIPTION_URL)
    }

    fun listRegistersByUserId(id: Long): MutableList<RegisterDTO> {

        return listRegistersByUserId(id, "$BASE_REGISTER_URL/by-user")
    }

    fun listRegistersByUserIdAndInstalmentId(userId: Long, instalmentId: Long): MutableList<RegisterDTO> {

        return listRegistersByUserIdAndInstalmentId(userId, instalmentId, "$BASE_REGISTER_URL/by-instalment")
    }

    fun findUserById(id: Long): UserDTO {
        return findUserById(id, "$BASE_USER_URL/by-id")
    }

    fun findTypeAccountById(id: Long): TypeAccount {
        return findTypeAccountById(id, "$BASE_TYPE_ACCOUNT_URL")
    }

    companion object {
        const val BASE_REGISTER_URL = "/traceit/register"
        const val BASE_USER_URL = "/traceit/user"
        const val BASE_TYPE_ACCOUNT_URL = "/traceit/type-account"
        const val BASE_ACCOUNT_URL = "/traceit/account"
        const val BASE_DESCRIPTION_URL = "/traceit/description"
    }
}