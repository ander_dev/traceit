import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(Cucumber::class)
@CucumberOptions(
        plugin = ["pretty"],
        features = ["classpath:thinksoft/features"],
        glue = ["thinksoft.steps"],
        tags = ["not @ignore"]
)
class RunTest