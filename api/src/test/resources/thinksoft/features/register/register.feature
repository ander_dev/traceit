@register
Feature: Registering Income or Expense tests

  Background: Create new user in the system, as parent user (SUPER_USER)
    Given a new user is created using "user-minimum" as my user form
    When user registration is submitted

  Scenario: Create new Register without User
    Given a new register is created using "register-minimum"
    When register is saved
    Then user will receive a message saying "User not found! Register can't be created without user." with statusCode 400

  Scenario: Create new income register without typeAccount
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    When register is saved
    Then user will receive a message saying "Type Account not found!" with statusCode 400

  Scenario: Create new income register without description
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And typeAccount of the register is "INCOME"
    When register is saved
    Then user will receive a message saying "Description not found!" with statusCode 400

  Scenario: Create new expense register without description
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And typeAccount of the register is "EXPENSE"
    When register is saved
    Then user will receive a message saying "Description not found!" with statusCode 400

  Scenario: Create new expense register without group
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Description Expense"
    When register is saved
    Then user will receive a message saying "Group not found!" with statusCode 400

  Scenario: Create new expense register without super group
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Description expense"
    And register group is "Group Description test"
    When register is saved
    Then user will receive a message saying "Super Group not found!" with statusCode 400

  Scenario: Create new expense register without account
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Description expense"
    And register group is "Group Description test"
    And register super group is "Super group Description test"
    When register is saved
    Then user will receive a message saying "Account not found!" with statusCode 400

  Scenario: Create new expense register without amount
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Description expense"
    And register group is "Group Description test"
    And register super group is "Super group Description test"
    When register is saved
    Then user will receive a message saying "Amount not informed!" with statusCode 400

  Scenario: Create new expense register with non existent description, group, super group
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Description expense"
    And register group is "Group Description test"
    And register super group is "Super group Description test"
    And register amount has value of 47.35
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And description "Test Description expense" exists for "SUPER_USER" and typeAccount "EXPENSE"
    And description "Group Description test" exists for "SUPER_USER" and typeAccount "GROUP"
    And description "Super group Description test" exists for "SUPER_USER" and typeAccount "SUPER_GROUP"

  Scenario: Create new expense register with existent description, group, super group
    Given a new description is created using "description-minimum"
    And new description user is equals to "SUPER_USER"
    And new description typeAccount is equals to "INCOME"
    And new description description is equals to "Test Income description Existent"
    And description is saved
    And a new description is created using "description-minimum"
    And new description user is equals to "SUPER_USER"
    And new description typeAccount is equals to "GROUP"
    And new description description is equals to "Test Income Group Existent"
    And description is saved
    And a new description is created using "description-minimum"
    And new description user is equals to "SUPER_USER"
    And new description typeAccount is equals to "SUPER_GROUP"
    And new description description is equals to "Test Income Super Group Existent"
    And description is saved
    And a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "INCOME"
    And register description is "Test Income description Existent"
    And register group is "Test Income Group Existent"
    And register super group is "Test Income Super Group Existent"
    And register amount has value of 47.35
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And description "Test Income description Existent" exists for "SUPER_USER" and typeAccount "INCOME"
    And description "Test Income Group Existent" exists for "SUPER_USER" and typeAccount "GROUP"
    And description "Test Income Super Group Existent" exists for "SUPER_USER" and typeAccount "SUPER_GROUP"

  Scenario: Create new expense register with 5 instalments not using credit card with monthly account
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with 5 instalments"
    And register group is "Test Expense Group with 5 instalments"
    And register super group is "Test Expense Super Group with 5 instalments"
    And register amount has value of 47.35
    And register instalments is equals to 5
    And register date is "03/05/2019"
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved contains 5 instalments each one with amount equals to 47.35 divided by instalments
    And register saved contains 5 monthly instalments from "03/05/2019"

  Scenario: Create new expense register using credit card with monthly account before the best day to buy
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And register credit card is created using "creditcard-minimum" for current user
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 57.35
    And register date is "02/05/2019"
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved date is equals to "27/05/2019"
    And register saved accounted is equals to "false"

  Scenario: Create new expense register using credit card with monthly account in the best day to buy
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And register credit card is created using "creditcard-minimum" for current user
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 100.35
    And register date is "03/05/2019"
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved date is equals to "27/05/2019"
    And register saved accounted is equals to "false"

  Scenario: Create new expense register using credit card with monthly account after the best day to buy
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And register credit card is created using "creditcard-minimum" for current user
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 100.35
    And register date is "04/05/2019"
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved date is equals to "27/06/2019"
    And register saved accounted is equals to "false"

  Scenario: Create new expense register using credit card with monthly account far after the best day to buy (15)
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And register credit card is created using "creditcard-minimum" for current user
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 100.35
    And register date is "15/05/2019"
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved date is equals to "27/06/2019"
    And register saved accounted is equals to "false"

  Scenario: Create new expense register using credit card with monthly account far after the best day to buy (30)
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And register credit card is created using "creditcard-minimum" for current user
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 100.35
    And register date is "30/05/2019"
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved date is equals to "27/06/2019"
    And register saved accounted is equals to "false"

  Scenario: Create new expense register using credit card with monthly account before the best day to buy with 6 instalments
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And register credit card is created using "creditcard-minimum" for current user
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 600.60
    And register date is "02/05/2019"
    And register instalments is equals to 6
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved contains 6 instalments each one with amount equals to 600.60 divided by instalments
    And register saved contains 6 monthly instalments from "27/05/2019"

  Scenario: Create new expense register using credit card with monthly account on the best day to buy with 6 instalments
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And register credit card is created using "creditcard-minimum" for current user
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 600.60
    And register date is "03/05/2019"
    And register instalments is equals to 6
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved contains 6 instalments each one with amount equals to 600.60 divided by instalments
    And register saved contains 6 monthly instalments from "27/05/2019"

  Scenario: Create new expense register using credit card with monthly account after the best day to buy with 6 instalments
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And register credit card is created using "creditcard-minimum" for current user
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 600.60
    And register date is "15/05/2019"
    And register instalments is equals to 6
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved contains 6 instalments each one with amount equals to 600.60 divided by instalments
    And register saved contains 6 monthly instalments from "27/06/2019"

  Scenario: New expense register deduction amount from account balance
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense with creditcard"
    And register group is "Test Expense Group with creditcard"
    And register super group is "Test Expense Super Group with creditcard"
    And register amount has value of 600.60
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved accounted is equals to "true"
    And account balance is equals to 399.40

  Scenario: New income register adding up amount from account balance
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "INCOME"
    And register description is "Test Income"
    And register group is "Test Income Group"
    And register super group is "Test Income Super Group"
    And register amount has value of 600.60
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved accounted is equals to "true"
    And account balance is equals to 1600.60

  Scenario: New expense register with 6 instalment deduction amount from account balance just from first instalment
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense without creditcard"
    And register group is "Test Expense Group without creditcard"
    And register super group is "Test Expense Super Group without creditcard"
    And register amount has value of 600.60
    And register instalments is equals to 6
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved accounted is equals to "true"
    And account balance is equals to 899.90

  Scenario: New expense register with 6 instalment adding up amount from account balance just from first instalment
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "INCOME"
    And register description is "Test Income without creditcard"
    And register group is "Test Income Group without creditcard"
    And register super group is "Test Income Super Group without creditcard"
    And register amount has value of 600.60
    And register instalments is equals to 6
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And register saved accounted is equals to "true"
    And account balance is equals to 1100.10

  Scenario: List registers for user
    Given new register for "Unit Test - Super User" is created
    And a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense without creditcard"
    And register group is "Test Expense Group without creditcard"
    And register super group is "Test Expense Super Group without creditcard"
    And register amount has value of 600.60
    And register instalments is equals to 6
    When register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And list of registers for "SUPER_USER" should contain 6 registers

  @workingOn
  Scenario: Edit single expense register and balance out account
    Given a new register is created using "register-minimum"
    And new register user is equals to "SUPER_USER"
    And new register account is created with a total balance of 1000.00
    And typeAccount of the register is "EXPENSE"
    And register description is "Test Expense without creditcard"
    And register group is "Test Expense Group without creditcard"
    And register super group is "Test Expense Super Group without creditcard"
    And register amount has value of 600.60
    And register is saved
    And register saved accounted is equals to "true"
    And account balance is equals to 399.40
    When register amount is updated to 300.05
    And register is saved
    Then user will receive a message saying "Register successfully saved!" with statusCode 200
    And account balance is equals to 699.95