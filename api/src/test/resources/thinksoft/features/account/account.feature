@account
Feature: Account tests

  Background: Create new user in the system, as parent user (SUPER_USER)
    Given a new user is created using "user-minimum" as my user form
    When user registration is submitted

  Scenario: Create new account without User
    Given a new account is created using "account-minimum"
    When  account is saved
    Then user will receive a message saying "User not found! Account can't be created without user." with statusCode 400

  Scenario: Create new account without Currency
    Given a new account is created using "account-minimum"
    And new account user is equals to "SUPER_USER"
    When account is saved
    Then user will receive a message saying "Currency not found! Account can't be created without currency." with statusCode 400

  Scenario: Create new account without TypeClosure
    Given a new account is created using "account-minimum"
    And new account user is equals to "SUPER_USER"
    And new account currency acronym is equals to "NZ$"
    When account is saved
    Then user will receive a message saying "Type Closure not found! Account can't be created without type of closure." with statusCode 400

  Scenario: Create new account
    Given a new account is created using "account-minimum"
    And new account user is equals to "SUPER_USER"
    And new account currency acronym is equals to "NZ$"
    And new account typeClosure is equals to "Monthly"
    When account is saved
    Then user will receive a message saying "Account registration successfully!" with statusCode 200

  Scenario: Super User can only have access to accounts that he has created
    Given a new account is created using "account-minimum"
    And new account user is equals to "SUPER_USER"
    And new account currency acronym is equals to "NZ$"
    And new account typeClosure is equals to "Monthly"
    And account is saved
    And new account for "Unit Test - Super User" is created
    And account is saved
    When "SUPER_USER" list his accounts
    Then user can only see 1 account