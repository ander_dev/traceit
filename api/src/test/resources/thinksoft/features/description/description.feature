@description
Feature: Description tests

  Background: Create new user in the system, as parent user (SUPER_USER)
    Given a new user is created using "user-minimum" as my user form
    When user registration is submitted

  Scenario: Create new description without User
    Given a new description is created using "description-minimum"
    When  description is saved
    Then user will receive a message saying "User not found! Description can't be created without user." with statusCode 400

  Scenario: Create new description without TypeAccount
    Given a new description is created using "description-minimum"
    And new description user is equals to "SUPER_USER"
    When description is saved
    Then user will receive a message saying "Type Account must be informed!" with statusCode 400

  Scenario: Create new description for credit
    Given a new description is created using "description-minimum"
    And new description user is equals to "SUPER_USER"
    And new description typeAccount is equals to "INCOME"
    When description is saved
    Then user will receive a message saying "Description registration successfully!" with statusCode 200

  Scenario: Create new description using list of typeAccounts "Income, Expense"
    Given a new description is created using "description-minimum"
    And new description user is equals to "SUPER_USER"
    And new description typeAccountIdList contains "INCOME,EXPENSE"
    When description is saved
    Then user will receive a message saying "Description registration successfully!" with statusCode 200
    And when "SUPER_USER" list description just created he will get 2 items, one for each typeAccount

  Scenario: Super User can only have access to descriptions that he has created
    Given a new description is created using "description-minimum"
    And new description user is equals to "SUPER_USER"
    And new description typeAccount is equals to "EXPENSE"
    When description is saved
    And new description for "Unit Test - Super User" is created
    And description is saved
    When "SUPER_USER" list his descriptions
    Then user can only see 1 description

  Scenario: Description not existent must return an Error
    Given a new description is created using "description-minimum"
    And new description user is equals to "SUPER_USER"
    And new description typeAccount is equals to "EXPENSE"
    When description is saved
    Then description "Test Expense description not Existent" does not exists for "SUPER_USER" and "Description not found!" will be thrown