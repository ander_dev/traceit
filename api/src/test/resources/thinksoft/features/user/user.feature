@user
Feature: User tests

  Background: Create new user in the system, as parent user (SUPER_USER)
    Given a new user is created using "user-minimum" as my user form
    When user registration is submitted

  Scenario: Testing new user (parent user) just created
    Then user will receive a message saying "User registration successfully, please check your e-mail to conclude your registration." with statusCode 200
    And user role should be "SUPER_USER"
    And emailVerified of "SUPER_USER" is "false"
    And defaultPassword of "SUPER_USER" is "false"

  Scenario: Create new user child for the parent user create in the background
    Given a new child user is created using "user-minimum" as my user form
    When user registration is submitted
    Then user will receive a message saying "User registration successfully, he/she will receive an e-mail shortly to finish the setup." with statusCode 200
    And user role should be "USER"
    And emailVerified of "USER" is "false"
    And defaultPassword of "USER" is "true"

  Scenario: As a parent user, I would like to be able to list users under my user
    Given a new child user is created using "user-minimum" as my user form
    And user registration is submitted
    And a new child user is created using "user-minimum" as my user form
    And user registration is submitted
    Then confirm that listing users of "SUPER_USER" we got a list size of 3

  Scenario: As a parent user, I would like to be able to delete users under my user
    Given a new child user is created using "user-minimum" as my user form
    And user registration is submitted
    And a new child user is created using "user-minimum" as my user form
    And user registration is submitted
    Then confirm that child users of parent user can be deleted

  Scenario: As a parent user, I would like to be able to edit a user selected from the list of users under my user
    Given a new child user is created using "user-minimum" as my user form
    When user registration is submitted
    And name of "USER" is set to "Child User edited"
    Then confirm that name of "USER" is "Child User edited"

  Scenario: As a user, I would like to be able to edit my email
    Given email of "SUPER_USER" is set to "new.email@gmail.com"
    And "SUPER_USER" is saved
    Then user will receive a message saying "You will need to confirm your new email again or access wont be granted next time you try to login." with statusCode 200
    And emailVerified of "SUPER_USER" is "false"

  Scenario: As a user, I would like to be able to edit my email with an email already in use
    Given email of "SUPER_USER" is set to "unit.test.super-user@thinksoft.co.nz"
    And "SUPER_USER" is saved
    Then user will receive a message saying "This e-mail is already been in use!" with statusCode 404

  Scenario: As a user, I would like to be able to change my password but wont inform confirmation
    Given password of "SUPER_USER" is set to "123456"
    When "SUPER_USER" is saved
    Then user will receive a message saying "Password does not match!" with statusCode 404

  Scenario: As a user, I would like to be able to change my password matching confirmation
    Given password of "SUPER_USER" is set to "123456"
    And passwordConfirmation of "SUPER_USER" is set to "123456"
    When "SUPER_USER" is saved
    Then user will receive a message saying "Your password has been updated." with statusCode 200