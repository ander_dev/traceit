@credit-card
Feature: Credit Card tests

  Background: Create new user in the system, as parent user (SUPER_USER)
    Given a new user is created using "user-minimum" as my user form
    When user registration is submitted

  Scenario: Create new Credit Card without User
    Given a new credit card is created using "creditcard-minimum"
    When  credit card is saved
    Then user will receive a message saying "User not found! Credit Card can't be created without user." with statusCode 400

  Scenario: Create new credit card
    Given a new credit card is created using "creditcard-minimum"
    And new credit card user is equals to "SUPER_USER"
    When credit card is saved
    Then user will receive a message saying "Credit Card registration successfully!" with statusCode 200

  Scenario: Super User can only have access to credit cards that he has created
    Given a new credit card is created using "creditcard-minimum"
    And new credit card user is equals to "SUPER_USER"
    And credit card is saved
    And new credit card for "Unit Test - Super User" is created
    And credit card is saved
    When "SUPER_USER" list his credit cards
    Then user can only see 1 credit card