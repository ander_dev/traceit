package thinksoft.utils

object Constants {
    const val JWT_SECRET = "Em3u7dCZ2QSvSGSGSRFUT@frwgu3WjfU2rHZxSjNSqU5x89"
    const val JWT_ISSUER = "Thinksoft"
    const val CRYPT_ALGORITHM = "AES"
    const val CRYPT_KEY = "AKL112013MM00987"
}