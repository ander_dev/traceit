package thinksoft.utils

import org.apache.commons.codec.binary.Base64
import java.security.Key
import java.security.NoSuchAlgorithmException
import javax.crypto.Cipher
import javax.crypto.NoSuchPaddingException
import javax.crypto.spec.SecretKeySpec

object Crypt {

    private var key: Key? = null
    private var c: Cipher? = null

    init {
        key = SecretKeySpec(Constants.CRYPT_KEY.toByteArray(), Constants.CRYPT_ALGORITHM)
        try {
            c = Cipher.getInstance(Constants.CRYPT_ALGORITHM)
        } catch (e: Exception ) {
            when(e) {
                is NoSuchAlgorithmException,
                        is NoSuchPaddingException -> {
                    e.printStackTrace()
                } else -> throw e
            }
        }
    }

    @Throws(Exception::class)
    fun encrypt(valueToEnc: String): String {
        c!!.init(Cipher.ENCRYPT_MODE, key)
        val encValue = valueToEnc.toByteArray()
        return Base64.encodeBase64String(encValue)
    }

    @Throws(Exception::class)
    fun decrypt(encryptedValue: String): String {
        c!!.init(Cipher.DECRYPT_MODE, key)
        val enctVal = encryptedValue.toByteArray()
        val decordedValue = Base64().decode(enctVal)
        return String(decordedValue)
    }
}