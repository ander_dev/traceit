package thinksoft.utils

import com.sendgrid.SendGrid

object Utils {

    @Throws(Exception::class)
    fun sendGridEmail(emailTo: String, emailFrom: String, nameFrom: String, subject: String, body: String) : SendGrid.Response{
        val sendgrid = SendGrid("traceitmail", "@Tracepass79")

        val email = SendGrid.Email()
        email.addTo(emailTo)
        email.from = emailFrom
        email.fromName = nameFrom
        email.subject = subject
        email.html = body

        return sendgrid.send(email)
    }
}