package thinksoft.utils

import java.util.*

object MessageFactory {
    private var bundle: ResourceBundle? = null
    private var locale: Locale? = null

    fun getMessage(key: String, language: String?): String {
        var language = language
        if (language == null || "pt_BR" != language) {
            language = "en"
        }
        locale = Locale(language)
        bundle = ResourceBundle.getBundle("thinksoft.bundle.Messages", locale!!)
        return bundle!!.getString(key)
    }
}