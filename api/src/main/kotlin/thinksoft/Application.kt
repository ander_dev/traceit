package thinksoft

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("thinksoft")
class Application

    /**
     * Run the application
     * @param args The command line arguments
     */
    fun main(args: Array<String>) {
        SpringApplication.run(Application::class.java, *args)
    }