package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.TypeAccount

@Repository("typeAccountRepository")
interface TypeAccountRepository : JpaRepository<TypeAccount, Long>{

    fun findByLocale(locale: String): MutableList<TypeAccount>

    fun findByDescription(description: String): TypeAccount
}