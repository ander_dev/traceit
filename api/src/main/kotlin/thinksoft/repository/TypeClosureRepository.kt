package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.TypeClosure

@Repository("typeClosureRepository")
interface TypeClosureRepository : JpaRepository<TypeClosure, Long> {

    fun findByLocale(locale: String): MutableList<TypeClosure>

    fun findByDescription(description: String): TypeClosure
}