package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import thinksoft.entities.User

@Repository("userRepository")
interface UserRepository : JpaRepository<User, Long> {

    fun findByEmail(email: String) : User

    @Query(value = "SELECT * FROM user WHERE ID = ?1 OR SUPER_USER_ID = ?1", nativeQuery = true)
    fun findAllById(id: Long) : MutableList<User>

}