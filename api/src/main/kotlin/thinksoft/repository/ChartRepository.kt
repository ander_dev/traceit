package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.Chart
import thinksoft.entities.Description
import thinksoft.entities.User

@Repository("chartRepository")
interface ChartRepository : JpaRepository<Chart, Long>{

    fun findByUser(user: User): MutableList<Chart>

    fun findByDescriptionAndUser(description: Description, user: User): MutableList<Chart>
}