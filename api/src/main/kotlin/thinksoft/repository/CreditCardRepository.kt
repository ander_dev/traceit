package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.CreditCard
import thinksoft.entities.User

@Repository("creditCardRepository")
interface CreditCardRepository : JpaRepository<CreditCard, Long>{

    fun findByUser(user: User): MutableList<CreditCard>

    fun findByDescription(description: String): CreditCard
}