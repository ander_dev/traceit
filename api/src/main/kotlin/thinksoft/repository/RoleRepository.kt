package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.Role

@Repository("roleRepository")
interface RoleRepository : JpaRepository<Role, Long>