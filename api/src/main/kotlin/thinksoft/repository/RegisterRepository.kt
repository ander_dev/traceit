package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.Instalment
import thinksoft.entities.Register
import thinksoft.entities.User

@Repository("registerRepository")
interface RegisterRepository : JpaRepository<Register, Long> {

    fun findByUser(user: User): MutableList<Register>

    fun findByUserAndInstalmentOrderByDate(user: User, instalment: Instalment): MutableList<Register>
}