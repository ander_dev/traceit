package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.Description
import thinksoft.entities.TypeAccount
import thinksoft.entities.User

@Repository("descriptionRepository")
interface DescriptionRepository : JpaRepository<Description, Long> {

    fun findByUser( user: User): MutableList<Description>

    fun findByTypeAccountAndUser(typeAccount: TypeAccount, user: User): MutableList<Description>

    fun findByDescriptionAndUser(description: String, user: User):  List<Description>

    fun findByDescriptionAndTypeAccountAndUser(description: String, typeAccount: TypeAccount, user: User): Description
}