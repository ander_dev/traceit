package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.Currency

@Repository("currencyRepository")
interface CurrencyRepository : JpaRepository<Currency, Long>{

    fun findByLocale(locale: String): MutableList<Currency>

    fun findByDescriptionAndLocale(description: String, locale: String): Currency

    fun findByAcronymAndLocale(acronym: String, locale: String): Currency
}