package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.Instalment
import thinksoft.entities.User

@Repository("instalmentRepository")
interface InstalmentRepository : JpaRepository<Instalment, Long> {
    fun findByUser(user: User): MutableList<Instalment>
}