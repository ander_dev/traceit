package thinksoft.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import thinksoft.entities.Account
import thinksoft.entities.User

@Repository("accountRepository")
interface AccountRepository : JpaRepository<Account, Long> {

    fun findByUser(user: User): MutableList<Account>

    fun findByDefaultAccountAndUser(defaultAccount: Boolean, user: User): Account
}