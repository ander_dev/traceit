package thinksoft.entities

import java.math.BigDecimal
import javax.persistence.*

@Entity
@Table(name = "instalment")
data class Instalment(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        var nrInstalments: Int,
        var amount: BigDecimal,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = User::class)
        @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_INSTALMENT_USER"))
        var user: User
) {
    override fun toString(): String {
        return "Instalment (id=$id, nrInstalments=$nrInstalments, user=$user)"
    }

    companion object {

        fun new(instalment: Int, amount: BigDecimal, user: User) = Instalment (
                nrInstalments = instalment,
                amount = amount,
                user = user
        )
    }
}