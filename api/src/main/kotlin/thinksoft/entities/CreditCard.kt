package thinksoft.entities

import thinksoft.entities.dto.CreditCardDTO
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "credit_card",
        uniqueConstraints = arrayOf(
                UniqueConstraint(name = "unique_credit_card_user", columnNames = arrayOf("description", "user_id"))
        ),
        indexes = arrayOf(
                Index(name = "idx_credit_card_description", columnList = "description")
        )
)
data class CreditCard (

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    var description: String,
    var payDay: Int,
    var bestDay: Int,
    var expireDate: Calendar,
    @Transient
    var expire: String? = null,
    @ManyToOne(cascade = arrayOf(CascadeType.REFRESH), targetEntity = User::class)
    @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_CREDIT_CARD_USER"))
    var user: User
) {
    override fun toString(): String {
        return "CreditCard (id=$id, description=$description, payDay=$payDay, bestDay=$bestDay, expireDate=$expireDate)"
    }

    fun toDTO(): CreditCardDTO = CreditCardDTO(
            id = this.id!!,
            description = this.description,
            payDay = this.payDay,
            bestDay = this.bestDay,
            expireDate = this.expireDate,
            expire = this.expire,
            user = this.user.toDTO()
    )

    companion object {

        fun fromDTO(dto: CreditCardDTO) = CreditCard(
                id = dto.id,
                description = dto.description!!,
                payDay = dto.payDay!!,
                bestDay = dto.bestDay!!,
                expireDate = dto.expireDate!!,
                expire = dto.expire,
                user = User.fromDTO(dto.user!!))
    }
}