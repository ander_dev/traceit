package thinksoft.entities

import thinksoft.entities.dto.RegisterDTO
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.*

@Entity
@Table(name = "register",
        indexes = arrayOf(
                Index(name = "idx_register_date",  columnList="date")
        )
)
data class Register(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        var date: ZonedDateTime? = null,
        var amount: BigDecimal,
        @Column(columnDefinition="bit default 0")
        var accounted: Boolean = false,
        @Transient
        var nrInstalment: Int? = null,
        @ManyToOne(cascade = [CascadeType.PERSIST], targetEntity = Description::class)
        @JoinColumn(name = "description_id", foreignKey = ForeignKey(name = "FK_REGISTER_DESCRIPTION"))
        var description: Description? = null,
        @ManyToOne(cascade = [CascadeType.PERSIST], targetEntity = Description::class)
        @JoinColumn(name = "group_id", foreignKey = ForeignKey(name = "FK_REGISTER_GROUP"))
        var group: Description? = null,
        @ManyToOne(cascade = [CascadeType.PERSIST], targetEntity = Description::class)
        @JoinColumn(name = "supergroup_id", foreignKey = ForeignKey(name = "FK_REGISTER_SGROUP"))
        var superGroup: Description? = null,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = Account::class)
        @JoinColumn(name = "account_id", foreignKey = ForeignKey(name = "FK_REGISTER_ACCOUNT"))
        var account: Account,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = TypeAccount::class)
        @JoinColumn(name = "typeaccount_id", foreignKey = ForeignKey(name = "FK_REGISTER_TYPEACCOUNT"))
        var typeAccount: TypeAccount,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = Instalment::class)
        @JoinColumn(name = "instalment_id", foreignKey = ForeignKey(name = "FK_REGISTER_INSTALMENT"))
        var instalment: Instalment? = null,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = CreditCard::class)
        @JoinColumn(name = "creditcard_id", foreignKey = ForeignKey(name = "FK_REGISTER_CREDIT_CARD"))
        var creditCard: CreditCard? = null,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = User::class)
        @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_REGISTER_USER"))
        var user: User
) {
    override fun toString(): String {
        return "Register (id=$id, date=$date, amount=$amount, nrInstalment=$nrInstalment)"
    }

    fun toDTO(): RegisterDTO = RegisterDTO(
            id = this.id!!,
            date = this.date,
            amount = this.amount,
            accounted = this.accounted,
            nrInstalment = this.nrInstalment,
            description = this.description!!.toDTO(),
            group = this.group!!.toDTO(),
            superGroup = this.superGroup!!.toDTO(),
            account = this.account!!.toDTO(),
            typeAccount = this.typeAccount,
            instalment = this.instalment,
            creditCard = this.creditCard?.let { this.creditCard!!.toDTO() },
            user = this.user.toDTO()
    )

    companion object {

        fun fromDTO(dto: RegisterDTO) = Register(
                id = dto.id,
                date = dto.date!!,
                amount = dto.amount!!,
                accounted = dto.accounted,
                nrInstalment = dto.nrInstalment,
                description = dto.description?.let { Description.fromDTO(it) },
                group = dto.group?.let { Description.fromDTO(it) },
                superGroup = dto.superGroup?.let { Description.fromDTO(it) },
                account = Account.fromDTO(dto.account!!),
                typeAccount = dto.typeAccount!!,
                instalment = dto.instalment,
                creditCard = dto.creditCard?.let { CreditCard.fromDTO(dto.creditCard!!) },
                user = User.fromDTO(dto.user!!)
        )

        fun newObject(register: Register) = Register(
                date = register.date!!,
                amount = register.amount!!,
                accounted = register.accounted,
                nrInstalment = register.nrInstalment,
                description = register.description,
                group = register.group,
                superGroup = register.superGroup,
                account = register.account,
                typeAccount = register.typeAccount!!,
                instalment = register.instalment,
                creditCard = register.creditCard,
                user = register.user
        )
    }
}
