package thinksoft.entities

import javax.persistence.*

@Entity
@Table(name = "type_closure",
        uniqueConstraints = arrayOf(
                UniqueConstraint(name = "unique_type_closure", columnNames = arrayOf("description", "locale"))
        ),
        indexes = arrayOf(
                Index(name = "idx_type_account_description", columnList = "description", unique = true)
        )
)
data class TypeClosure(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        var description: String? = null,
        var locale: String? = null,
        var commonDescription: String? = null
) {
    override fun toString(): String {
        return "TypeClosure (id=$id, description=$description, locale=$locale, commonDescription=$commonDescription)"
    }
}