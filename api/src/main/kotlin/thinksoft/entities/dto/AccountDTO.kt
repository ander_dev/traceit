package thinksoft.entities.dto

import thinksoft.entities.Closure
import thinksoft.entities.Currency
import thinksoft.entities.TypeClosure
import java.math.BigDecimal
import java.util.*

data class AccountDTO (
        var id : Long? = null,
        var description: String? = null,
        var startDate: Calendar? = null,
        var endDate: Calendar? = null,
        var typeClosure: TypeClosure? = null,
        var currency: Currency? = null,
        var totalCredit: BigDecimal? = null,
        var totalDebit: BigDecimal? = null,
        var totalGeneral: BigDecimal? = null,
        var user: UserDTO? = null,
        var defaultAccount: Boolean = false,
        var closureList: List<Closure>? = null
){
    override fun toString(): String {
        return "AccountDTO (id=$id, description=$description, typeClosure=$typeClosure, user=$user)"
    }
}