package thinksoft.entities.dto

import java.util.*

data class CreditCardDTO(
        var id : Long? = null,
        var description: String? = null,
        var payDay: Int? = null,
        var bestDay: Int? = null,
        var expireDate: Calendar? = null,
        var expire: String? = null,
        var user: UserDTO? = null
        ) {
    override fun toString(): String {
        return "CreditCard (id=$id, description=$description, payDay=$payDay, bestDay=$bestDay, expireDate=$expireDate)"
    }
}