package thinksoft.entities.dto

import thinksoft.entities.Instalment
import thinksoft.entities.TypeAccount
import java.math.BigDecimal
import java.time.ZonedDateTime

data class RegisterDTO(
        var id: Long? = null,
        var date: ZonedDateTime? = null,
        var amount: BigDecimal? = null,
        var nrInstalment: Int? = null,
        var description: DescriptionDTO? = null,
        var group: DescriptionDTO? = null,
        var superGroup: DescriptionDTO? = null,
        var account: AccountDTO? = null,
        var typeAccount: TypeAccount? = null,
        var instalment: Instalment? = null,
        var accounted: Boolean = false,
        var creditCard: CreditCardDTO? = null,
        var user: UserDTO? = null
) {
    override fun toString(): String {
        return "Register (id=$id, amount=$amount, nrInstalment=$nrInstalment, user=$user)"
    }
}