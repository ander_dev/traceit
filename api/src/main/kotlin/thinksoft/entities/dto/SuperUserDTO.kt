package thinksoft.entities.dto

import thinksoft.entities.Role
import thinksoft.entities.User
import java.util.*

data class SuperUserDTO(
        var id: Long? = null,
        var name: String? = null,
        var email: String? = null,
        var password: String? = null,
        var oldPassword: String? = null,
        var excluded: Boolean = false,
        var credentialExpired: Boolean = false,
        var language: String? = null,
        var timeZoneName: String? = null,
        var defaultPassword: Boolean = false,
        var registered: Calendar? = null,
        var emailVerified: Boolean = false,
        var role: Role? = null,
        var superUser: User? = null,
        var admin: Boolean = false,
        var token: String? = null,
        var confirmPassword: String? = null,
        var authenticationUrl: String? = null
) {
    companion object {
        fun toDTO(user: User) = SuperUserDTO(
                id = user.id,
                name = user.name,
                email = user.email,
                password = user.password,
                oldPassword = user.oldPassword,
                excluded =  user.excluded,
                credentialExpired = user.credentialExpired,
                language = user.language,
                timeZoneName = user.timeZoneName,
                defaultPassword = user.defaultPassword,
                registered = user.registered,
                emailVerified = user.emailVerified,
                role = user.role,
                superUser = user.superUser,
                admin = user.admin,
                token = user.token,
                confirmPassword = user.confirmPassword,
                authenticationUrl = user.authenticationUrl)

        fun toDTO(user: UserDTO) = SuperUserDTO(
                id = user.id,
                name = user.name,
                email = user.email,
                password = user.password,
                oldPassword = user.oldPassword,
                excluded =  user.excluded,
                credentialExpired = user.credentialExpired,
                language = user.language,
                timeZoneName = user.timeZoneName,
                defaultPassword = user.defaultPassword,
                registered = user.registered,
                emailVerified = user.emailVerified,
                role = user.role,
                superUser = null,
                admin = user.admin,
                token = user.token,
                confirmPassword = user.confirmPassword,
                authenticationUrl = user.authenticationUrl)
    }
}