package thinksoft.entities.dto

import java.math.BigDecimal
import java.util.*

data class ChartDTO (

        var id: Long? = null,
        var validFor: Calendar? = null,
        var description: DescriptionDTO? = null,
        var user: UserDTO? = null,
        var visible: Boolean? = null,
        var paid: Boolean? = null,
        var isCredit: Boolean? = null,
        var value: BigDecimal? = null,
        var balance: BigDecimal? = null
)