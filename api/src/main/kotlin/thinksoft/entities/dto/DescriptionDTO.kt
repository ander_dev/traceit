package thinksoft.entities.dto

import thinksoft.entities.TypeAccount

data class DescriptionDTO(
        var id: Long? = null,
        var description: String? = null,
        var typeAccount: TypeAccount? = null,
        var user: UserDTO? = null,
        var typeAccountList: List<TypeAccount>? = null
) {
    override fun toString(): String {
        return "DescriptionDTO (id=$id, description=$description, typeAccountList=$typeAccountList, typeAccount=$typeAccount)"
    }

    companion object {
        fun toDTO(desc: String) = DescriptionDTO(
                description = desc
        )
    }
}