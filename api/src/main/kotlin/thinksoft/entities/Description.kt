package thinksoft.entities

import thinksoft.entities.dto.DescriptionDTO
import thinksoft.utils.Crypt
import javax.persistence.*

@Entity
@Table(name = "description",
        uniqueConstraints = arrayOf(
            UniqueConstraint(name = "unique_descr_type_acc_user",columnNames = arrayOf("description", "type_account_id", "user_id"))
        ),
        indexes = arrayOf(
                Index(name = "idx_description_description",  columnList="description")
        )
)
data class Description(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        var description: String?,
        @ManyToOne(cascade = arrayOf(CascadeType.REFRESH), targetEntity = TypeAccount::class)
        @JoinColumn(name = "type_account_id", foreignKey = ForeignKey(name = "FK_DESCRIPTION_TYPE_ACCOUNT"))
        var typeAccount: TypeAccount? = null,
        @ManyToOne(cascade = arrayOf(CascadeType.PERSIST), targetEntity = User::class, fetch = FetchType.EAGER)
        @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_DESCRIPTION_USER"))
        var user: User?,
        @Transient
        var typeAccountList: List<TypeAccount>? = null
) {
    override fun toString(): String {
        return "Description (id=$id, description=$description, typeAccountList=$typeAccountList, typeAccount=$typeAccount)"
    }
    fun toDTO(): DescriptionDTO = DescriptionDTO(
            id = this.id!!,
            description = Crypt.decrypt(this.description!!),
            typeAccount = this.typeAccount,
            user = this.user?.toDTO(),
            typeAccountList = this.typeAccountList
    )


    companion object {

        fun fromDTO(dto: DescriptionDTO) = Description(
                id = dto.id,
                description = Crypt.encrypt(dto.description!!),
                typeAccount = dto.typeAccount,
                user = User.fromDTO(dto.user!!),
                typeAccountList = dto.typeAccountList)
    }
}