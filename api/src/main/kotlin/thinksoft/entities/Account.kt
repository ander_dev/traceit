package thinksoft.entities

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import thinksoft.entities.dto.AccountDTO
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "account",
        uniqueConstraints = arrayOf(
                UniqueConstraint(name = "unique_account_user", columnNames = arrayOf("description", "user_id")),
                UniqueConstraint(name = "unique_account_defaultAccount", columnNames = arrayOf("defaultAccount", "user_id"))
        ),
        indexes = arrayOf(
                Index(name = "idx_account_description", columnList = "description")
        )
)
data class Account(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        var description: String,
        var startDate: Calendar,
        var endDate: Calendar? = null,
        @ManyToOne(cascade = arrayOf(CascadeType.REFRESH), targetEntity = TypeClosure::class)
        @JoinColumn(name = "type_closure_id", foreignKey = ForeignKey(name = "FK_ACCOUNT_TYPE_CLOSURE"))
        var typeClosure: TypeClosure,
        @ManyToOne(cascade = arrayOf(CascadeType.REFRESH), targetEntity = Currency::class)
        @JoinColumn(name = "currency_id", foreignKey = ForeignKey(name = "FK_ACCOUNT_CURRENCY"))
        var currency: Currency,
        @Column(length = 13, precision = 13, scale = 2, columnDefinition="double default 0")
        var totalCredit: BigDecimal? = null,
        @Column(length = 13, precision = 13, scale = 2, columnDefinition="double default 0")
        var totalDebit: BigDecimal? = null,
        @Column(length = 13, precision = 13, scale = 2, columnDefinition="double default 0")
        var totalGeneral: BigDecimal? = null,
        @ManyToOne(cascade = arrayOf(CascadeType.PERSIST), targetEntity = User::class, fetch = FetchType.EAGER)
        @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_ACCOUNT_USER"))
        var user: User,
        @Column(columnDefinition = "bit default 0")
        var defaultAccount: Boolean = false,
        @OneToMany(cascade = arrayOf(CascadeType.REFRESH), fetch = FetchType.EAGER)
        @JoinColumn(name = "account_id", foreignKey = ForeignKey(name = "FK_ACCOUNT_CLOSURE"))
        @Fetch(value = FetchMode.SUBSELECT)
        var closureList: List<Closure>? = null
) {
    override fun toString(): String {
        return "Account (id=$id, description=$description, typeClosure=$typeClosure)"
    }

    fun toDTO(): AccountDTO = AccountDTO(
            id = this.id!!,
            description = this.description,
            startDate = this.startDate,
            endDate = this.endDate,
            typeClosure = this.typeClosure,
            currency = this.currency,
            totalCredit = this.totalCredit,
            totalDebit = this.totalDebit,
            totalGeneral = this.totalGeneral,
            user = this.user.toDTO(),
            defaultAccount = this.defaultAccount,
            closureList = this.closureList
    )

    companion object {

        fun fromDTO(dto: AccountDTO) = Account(
                id = dto.id,
                description = dto.description!!,
                startDate = dto.startDate!!,
                endDate = dto.endDate,
                typeClosure = dto.typeClosure!!,
                currency = dto.currency!!,
                totalCredit = dto.totalCredit,
                totalDebit = dto.totalDebit,
                totalGeneral = dto.totalGeneral,
                user = User.fromDTO(dto.user!!),
                defaultAccount = dto.defaultAccount,
                closureList = dto.closureList)
    }

}