package thinksoft.entities

import javax.persistence.*

@Entity
@Table(name = "currency",
        uniqueConstraints = arrayOf(
            UniqueConstraint(name = "unique_currency_locale",columnNames = arrayOf("description", "locale"))
        ),
        indexes = arrayOf(
                Index(name = "idx_currency_description",  columnList="description"),
                Index(name = "idx_currency_locale",  columnList="locale")
        )
)
data class Currency(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        var description: String? = null,
        var acronym: String? = null,
        var locale: String? = null
) {
    override fun toString(): String {
        return "Currency (id=$id, description=$description, acronym=$acronym, locale=$locale)"
    }
}