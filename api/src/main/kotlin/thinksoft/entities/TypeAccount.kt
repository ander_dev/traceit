package thinksoft.entities

import javax.persistence.*

@Entity
@Table(name = "type_account",
        uniqueConstraints = arrayOf(
            UniqueConstraint(name = "unique_type_acct_descr_locale",columnNames = arrayOf("description", "locale"))
        ),
        indexes = arrayOf(
                Index(name = "idx_type_acct_description",  columnList="description", unique = true)
        )
)
data class TypeAccount(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id : Long? = null,
        var description: String? = null,
        var locale: String? = null,
        var showType: Boolean? = null,
        var commonDescription: String? = null
) {
    override fun toString(): String {
        return "TypeAccount (id=$id, description=$description, commonDescription=$commonDescription, locale=$locale, showType=$showType)"
    }
    companion object {

        fun new(id: Long) = TypeAccount(
                id = id )
    }
}