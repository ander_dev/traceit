package thinksoft.entities

import thinksoft.entities.dto.SuperUserDTO
import thinksoft.entities.dto.UserDTO
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "user",
        uniqueConstraints = arrayOf(
            UniqueConstraint(name = "unique_user_email", columnNames = arrayOf("email"))
        ),
        indexes = arrayOf(
                Index(name = "idx_user_email",  columnList="email", unique = true)
        )
)
@NamedQueries(NamedQuery(name = "user.findByEmail", query = "SELECT us FROM User us WHERE us.email = :email"))
data class User(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        var name: String,
        var email: String,
        var password: String,
        var oldPassword: String? = null,
        var language: String,
        var timeZoneName: String,
        @Column(columnDefinition="bit default 0")
        var excluded: Boolean = false,
        @Column(columnDefinition="bit default 0")
        var credentialExpired: Boolean = false,
        @Column(columnDefinition="bit default 0")
        var defaultPassword: Boolean = false,
        var registered: Calendar? = null,
        @Column(columnDefinition="bit default 0")
        var emailVerified: Boolean = false,
        @Column(columnDefinition="bit default 0")
        var admin: Boolean = false,
        @ManyToOne(targetEntity = Role :: class, fetch = FetchType.EAGER)
        @JoinColumn(name="role_id", foreignKey = ForeignKey(name = "FK_USER_ROLE"))
        var role: Role,
        @ManyToOne(cascade = arrayOf(CascadeType.PERSIST), targetEntity = User::class, fetch = FetchType.EAGER)
        @JoinColumn(name = "super_user_id", foreignKey = ForeignKey(name = "FK_USER_USER"))
        var superUser: User? = null,
        @Transient
        var token: String? = null,
        @Transient
        var confirmPassword: String? = null,
        @Transient
        var authenticationUrl: String? = null
) {
    override fun toString(): String {
        return "User (id=$id, name=$name, registered=$registered, password=$password, excluded=$excluded, credentialExpired=$credentialExpired, role=$role)"
    }

    fun toDTO(): UserDTO = UserDTO(
            id = this.id!!,
            name = this.name,
            email = this.email,
            password = this.password,
            oldPassword = this.oldPassword,
            excluded = this.excluded,
            credentialExpired = this.credentialExpired,
            language = this.language,
            timeZoneName = this.timeZoneName,
            defaultPassword = this.defaultPassword,
            registered = this.registered,
            emailVerified = this.emailVerified,
            role = this.role,
            superUser = this.superUser?.let { SuperUserDTO.toDTO(it) },
            admin = this.admin,
            token = this.token,
            confirmPassword = this.confirmPassword,
            authenticationUrl = this.authenticationUrl
    )

    companion object {

        fun fromDTO(dto: UserDTO) = User(
                id = dto.id,
                name = dto.name!!,
                email = dto.email!!,
                password = dto.password!!,
                oldPassword = dto.oldPassword,
                excluded =  dto.excluded,
                credentialExpired = dto.credentialExpired,
                language = dto.language!!,
                timeZoneName = dto.timeZoneName!!,
                defaultPassword = dto.defaultPassword,
                registered = dto.registered,
                emailVerified = dto.emailVerified,
                role = dto.role!!,
                admin = dto.admin,
                token = dto.token,
                confirmPassword = dto.confirmPassword,
                authenticationUrl = dto.authenticationUrl)
    }
}
