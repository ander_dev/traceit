package thinksoft.entities

import thinksoft.entities.dto.ChartDTO
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "chart",
        uniqueConstraints = arrayOf(
                UniqueConstraint(name = "unique_chart_user_validFor", columnNames = arrayOf("description_id", "user_id", "validFor"))
        ),
        indexes = arrayOf(
                Index(name = "idx_chart_description", columnList = "description_id"),
                Index(name = "idx_chart_id", columnList = "id")
        )
)
data class Chart (

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    var validFor: Calendar,
    @ManyToOne(cascade = arrayOf(CascadeType.REFRESH), targetEntity = Description::class)
    @JoinColumn(name = "description_id", foreignKey = ForeignKey(name = "FK_CHART_DESCRIPTION"))
    var description: Description,
    @ManyToOne(cascade = arrayOf(CascadeType.REFRESH), targetEntity = User::class)
    @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_CHART_USER"))
    var user: User,
    @Column(name = "visible", columnDefinition = "bit(1) default false")
    var visible: Boolean,
    @Column(name = "paid", columnDefinition = "bit(1) default false")
    var paid: Boolean? = null,
    @Column(name = "isCredit", columnDefinition = "bit(1) default false")
    var isCredit: Boolean,
    @Column(length = 13, precision = 13, scale = 2, columnDefinition="double default 0")
    var value: BigDecimal? = null,
    @Column(length = 13, precision = 13, scale = 2, columnDefinition="double default 0")
    var balance: BigDecimal? = null
){
    override fun toString(): String {
        return "Chart (id=$id, description=$description, validFor=$validFor, user=$user)"
    }

    fun toDTO(): ChartDTO = ChartDTO(
            id = this.id!!,
            validFor = this.validFor,
            description = this.description.toDTO(),
            user = this.user.toDTO(),
            visible = this.visible,
            paid = this.paid,
            isCredit = this.isCredit,
            value = this.value,
            balance = this.balance
    )

    companion object {

        fun fromDTO(dto: ChartDTO) = Chart(
                id = dto.id,
                validFor = dto.validFor!!,
                description = Description.fromDTO(dto.description!!),
                user = User.fromDTO(dto.user!!),
                visible = dto.visible!!,
                paid = dto.paid,
                isCredit = dto.isCredit!!,
                value = dto.value!!,
                balance = dto.balance)
    }

}