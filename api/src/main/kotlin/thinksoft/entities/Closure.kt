package thinksoft.entities

import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "closure",
        uniqueConstraints = arrayOf(
            UniqueConstraint(name = "unique_closure",columnNames = arrayOf("date", "start_date", "account_id"))
        ),
        indexes = arrayOf(
                Index(name = "idx_closure_startDate",  columnList = "start_date")
        )
)
data class Closure(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long? = null,
        var date: Calendar? = null,
        @Column(name = "start_date")
        var startDate: Calendar? = null,
        @Column(name = "end_date")
        var endDate: Calendar? = null,
        @Column(length = 13, precision = 13, scale = 2)
        var totalCredit: BigDecimal? = null,
        @Column(length = 13, precision = 13, scale = 2)
        var totalDebit: BigDecimal? = null,
        @Column(length = 13, precision = 13, scale = 2)
        var totalGeneral: BigDecimal? = null,
        @ManyToOne(cascade = arrayOf(CascadeType.REFRESH), targetEntity = Account::class)
        @JoinColumn(name = "account_id", foreignKey = ForeignKey(name = "FK_CLOSURE_ACCOUNT"))
        //@JsonIgnore
        var account: Account? = null
) {
    override fun toString(): String {
        return "Currency (id=$id, date=$date, startDate=$startDate, endDate=$endDate)"
    }
}