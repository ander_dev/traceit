package thinksoft.thread

import com.sendgrid.SendGrid
import thinksoft.entities.User
import thinksoft.utils.Crypt
import thinksoft.utils.MessageFactory
import thinksoft.utils.Utils
import java.net.URI
import java.time.LocalDateTime

object EmailThread {

    @Throws(Exception::class)
    fun email (user: User, userType: String) : SendGrid.Response {
        val emailTo = user.email
        val emailFrom = "contact@traceit.nz"
        var nameFrom = "Traceit"
        var emailSubject = MessageFactory.getMessage("lb_email_subject", user.language)

        val body = StringBuilder()

        body.append(MessageFactory.getMessage("lb_email_hello", user.language)).append(" <b>").append(user.name).append("</b>,<br/><br/>")
        if (userType == "USER") {
            nameFrom = user.superUser!!.name
            if (user.password != null && user.defaultPassword) {

                body.append("<b>").append(user.superUser!!.name).append("</b>").append(MessageFactory.getMessage("lb_email_first_line", user.language)).append("<br/><br/>")
                body.append(MessageFactory.getMessage("lb_email_second_line", user.language)).append(" <b>").append("</b><br/><br/>")

                body.append(MessageFactory.getMessage("lb_email_first_login", user.language)).append("<br/><br/>")
                body.append(MessageFactory.getMessage("lb_email_user", user.language)).append(" <b>").append(user.email).append("</b><br/>")
                body.append(MessageFactory.getMessage("lb_email_password", user.language)).append(" <b>").append(Crypt.decrypt(user.password)).append("</b><br/><br/>")
                body.append(MessageFactory.getMessage("lb_email_link", user.language)).append(" <b>").append("<a href='" + user.authenticationUrl + "' target'_blank'>www.traceit.nz</a>").append("</b><br/><br/>")
                body.append(MessageFactory.getMessage("lb_email_next_page", user.language)).append(" <b>").append("</b><br/><br/><br/>")
            }
        } else if (userType == "SUPER_USER" || userType =="ADMIN") {
            body.append(MessageFactory.getMessage("lb_email_confirmation", user.language)).append("<br/><br/>")
            val date = LocalDateTime.now()
            val token = Crypt.encrypt(user.id.toString() + "#" + date)
            val tokenURL = user.authenticationUrl + "/" + token
            body.append(MessageFactory.getMessage("lb_email_link", user.language)).append(" <b>").append("<a href=$tokenURL>$tokenURL</a>").append("</b><br/><br/>")
        } else if (userType == "RESET_PASSWORD") {
            val date = LocalDateTime.now()
            val token = Crypt.encrypt(user.id.toString() + "#" + date)
            val confirmURL = URI.create(user.authenticationUrl + "/" + token)
            emailSubject = MessageFactory.getMessage("lb_email_subject_reset_password", user.language)

            body.append(MessageFactory.getMessage("lb_email_reset_password_first_line", user.language)).append("<br/><br/>")
            body.append(MessageFactory.getMessage("lb_email_reset_password_second_line", user.language)).append(" <b>").append("</b><br/><br/>")

            body.append(MessageFactory.getMessage("lb_email_user", user.language)).append(" <b>").append(user.email).append("</b><br/>")
            body.append(MessageFactory.getMessage("lb_email_password", user.language)).append(" <b>").append(Crypt.decrypt(user.password)).append("</b><br/><br/>")
            body.append(MessageFactory.getMessage("lb_email_link", user.language)).append(" <b>").append("<a href='" + confirmURL + "' target'_blank'>" + confirmURL.toString() + "</a>").append("</b><br/><br/>")
            body.append(MessageFactory.getMessage("lb_email_next_page", user.language)).append(" <b>").append("</b><br/><br/><br/>")

        }

        body.append(MessageFactory.getMessage("lb_email_thanks", user.language)).append("<br/><br/>")
        body.append("<b>").append(MessageFactory.getMessage("lb_email_signature", user.language)).append("</b>")

        return Utils.sendGridEmail(emailTo, emailFrom, nameFrom, emailSubject, body.toString())
    }
}