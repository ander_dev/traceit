package thinksoft.service.impl

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import thinksoft.entities.User
import thinksoft.entities.dto.ResponseDTO
import thinksoft.entities.dto.UserDTO
import thinksoft.repository.*
import thinksoft.service.UserService
import thinksoft.thread.EmailThread
import thinksoft.utils.Constants
import thinksoft.utils.Crypt
import thinksoft.utils.MessageFactory
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import javax.security.auth.login.CredentialNotFoundException

@Service("userService")
class UserServiceImpl: UserService {

    @Qualifier("userRepository")
    @Autowired
    lateinit var userRepository: UserRepository

    @Qualifier("roleRepository")
    @Autowired
    lateinit var roleRepository: RoleRepository

    @Qualifier("accountRepository")
    @Autowired
    lateinit var accountRepository: AccountRepository

    @Qualifier("descriptionRepository")
    @Autowired
    lateinit var descriptionRepository: DescriptionRepository

    @Qualifier("creditCardRepository")
    @Autowired
    lateinit var creditCardRepository: CreditCardRepository

    @Qualifier("registerRepository")
    @Autowired
    lateinit var registerRepository: RegisterRepository

    @Qualifier("instalmentRepository")
    @Autowired
    lateinit var instalmentRepository: InstalmentRepository

    override fun findByEmail(email: String): UserDTO {

        var user: User = userRepository.findByEmail(email)

        if (user == null)throw CredentialNotFoundException("Credentials Not Found Exception")

        return user.toDTO()
    }

    override fun findById(id: Long): UserDTO {

        var user: User = userRepository.findById(id).get()

        if (user == null)throw CredentialNotFoundException("User Not Found Exception")

        return user.toDTO()
    }

    fun checkEmailAlreadyInUse(email: String): Boolean {
        try {
            userRepository.findByEmail(email)
            return true
        } catch (e: EmptyResultDataAccessException) {
            return false
        }
    }

    @Transactional
    override fun save(userDTO: UserDTO): ResponseDTO {
        var roleId:Long
        var savedMessage = ""
        //TODO email is not been verified
        if (userDTO.id == null) {
            if(checkEmailAlreadyInUse(userDTO.email!!)){
                return ResponseDTO(MessageFactory.getMessage("lb_email_in_use", userDTO.language), 404, null)
            }

            if (userDTO.language == null) {
                return ResponseDTO(MessageFactory.getMessage("lb_user_language_not_null", "en"), 404, null)
            }

            if (userDTO.superUser == null && (userDTO.password != userDTO.confirmPassword)) {
                return ResponseDTO(MessageFactory.getMessage("lb_password_not_match", userDTO.language), 404, null)
            }

            userDTO.password = BCryptPasswordEncoder().encode(userDTO.password)

            when {
                //TODO work out a solution to make user admin
                userDTO.superUser == null -> {
                    roleId = 2 //SUPER_USER
                    userDTO.confirmPassword = userDTO.password
                    userDTO.defaultPassword = false
                    userDTO.emailVerified = false
                    savedMessage = MessageFactory.getMessage("lb_user_saved_super_user", userDTO.language)
                }
                else -> {
                    roleId = 3//USER
                    userDTO.password = Crypt.encrypt(userDTO.password!!)
                    userDTO.oldPassword = userDTO.password
                    userDTO.confirmPassword = userDTO.password
                    userDTO.defaultPassword = true
                    userDTO.emailVerified = false
                    savedMessage = MessageFactory.getMessage("lb_user_saved_user", userDTO.language)
                }
            }

            userDTO.role = roleRepository.findById(roleId).get()

            TimeZone.setDefault(TimeZone.getTimeZone(userDTO.timeZoneName))
            userDTO.registered = Calendar.getInstance()
        }  else {
            var localUserDTO = userRepository.findById(userDTO.id).get()
            if (localUserDTO.email != userDTO.email) {
                if(checkEmailAlreadyInUse(userDTO.email!!)){
                    return ResponseDTO(MessageFactory.getMessage("lb_email_in_use", userDTO.language), 404, null)
                } else {
                    userDTO.emailVerified = false
                    savedMessage = MessageFactory.getMessage("lb_user_email_updated", userDTO.language)
                }
            }
            if(localUserDTO.password != userDTO.password) {
                if (userDTO.password != userDTO.confirmPassword) {
                    return ResponseDTO(MessageFactory.getMessage("lb_password_not_match", userDTO.language), 404, null)
                } else {
                    savedMessage = MessageFactory.getMessage("lb_user_password_updated", userDTO.language)
                }
            }
        }

        var user = User.fromDTO(userDTO)

        if(userDTO.superUser != null) user.superUser = userRepository.findById(userDTO.superUser!!.id).get()

        try {
            userRepository.save(user)
        } catch (e: Exception) {
            //todo work logs out
            return ResponseDTO(e.message!!, 400, null)
        }

        if (!userDTO.emailVerified) {
            GlobalScope.launch {
                CoroutineStart.DEFAULT
                EmailThread.email(user, user.role.role!!)
            }
        }

        return ResponseDTO(savedMessage, 200, user.toDTO())
    }

    override fun login(email: String, password: String): ResponseDTO? {
        var user = userRepository.findByEmail(email)
        if (user != null && user.emailVerified){
            if (BCrypt.checkpw(password, user.password)) {
                return ResponseDTO(MessageFactory.getMessage("lb_login_success", user.language), 200, updateToken(user).toDTO())
            }
        } else if (user != null && !user.emailVerified){
            return ResponseDTO(MessageFactory.getMessage("lb_user_email_not_verified", user.language), 400, null)
        }
        throw return ResponseDTO("Bad Credentials", 400, null)
    }

    private fun newToken(user: User): String {
        return Jwts.builder()
                .setIssuedAt(Date())
                .setSubject(user.email)
                .setIssuer(Constants.JWT_ISSUER)
                .setExpiration(Date(System.currentTimeMillis() + 10 * 24 * 60 * 60 * 1000)) // 10 days
                .signWith(SignatureAlgorithm.HS256, Constants.JWT_SECRET).compact()
    }

    override fun loginByToken(token: String): ResponseDTO? {
        val claims = Jwts.parser().setSigningKey(Constants.JWT_SECRET).parseClaimsJws(token).body
        val user = userRepository.findByEmail(claims.subject)
        if (user != null && user.emailVerified) {
            user.token = token
            val valid = validToken(user)
            if (valid) {
                return ResponseDTO(MessageFactory.getMessage("lb_login_success", user.language), 200, updateToken(user).toDTO())
            }
        } else if (user != null && !user.emailVerified) {
            user.token = token
            val valid = validToken(user)
            if (valid) {
                user.emailVerified = true
                userRepository.save(user)
                return ResponseDTO(MessageFactory.getMessage("lb_login_success", user.language), 200, updateToken(user).toDTO())
            } else {
                return ResponseDTO(MessageFactory.getMessage("lb_user_authorization_expired", user.language), 400, updateToken(user).toDTO())
            }
        }
        throw return ResponseDTO("Credentials Expired", 400, null)

    }

    private fun validToken(user: User): Boolean {
        val claims = Jwts.parser().setSigningKey(Constants.JWT_SECRET).parseClaimsJws(user.token).body
        return claims.subject == user.email && claims.issuer == Constants.JWT_ISSUER && Date().before(claims.expiration)
    }

    private fun updateToken(user: User): User {
        if (user.token == null) {
            user.token = newToken(user)
        } else {
            if(!validToken(user)) {
                user.token = newToken(user)
            }
        }
        return userRepository.save(user)
    }

    override fun delete(id: Long): ResponseDTO {
        var user = userRepository.findById(id).get()

        registerRepository.findByUser(user).forEach {
            registerRepository.delete(it)
        }

        instalmentRepository.findByUser(user).forEach {
            instalmentRepository.delete(it)
        }

        accountRepository.findByUser(user).forEach {
            accountRepository.delete(it)
        }

        descriptionRepository.findByUser(user).forEach {
            descriptionRepository.delete(it)
        }

        creditCardRepository.findByUser(user).forEach {
            creditCardRepository.delete(it)
        }

        userRepository.delete(user)
        return ResponseDTO(MessageFactory.getMessage("lb_user_deleted", user.language), 200, null)
    }

    override fun findAll() : MutableList<User>{
        return userRepository.findAll()
    }

    override fun findAllById(id: Long): MutableList<User> {
        return userRepository.findAllById(id)
    }

    override fun forgetPassword(email: String, redirectURL: String): ResponseDTO {
        var user = userRepository.findByEmail(email)

        if (user == null) return ResponseDTO("Credentials Not Found Exception", 404, null)

        user.password = Crypt.encrypt("NeedToChangeIt")
        user.oldPassword = user.password
        user.confirmPassword = user.password
        user.defaultPassword = true
        user.emailVerified = true
        user.authenticationUrl = redirectURL
        user = userRepository.save(user)

        GlobalScope.launch {
            CoroutineStart.DEFAULT
            EmailThread.email(user, "RESET_PASSWORD")
        }
        return ResponseDTO(MessageFactory.getMessage("lb_user_reset_password", user.language), 200, null)
    }

    @Transactional
    override fun newPassword(userDTO: UserDTO): ResponseDTO? {
        val decryptedToken = Crypt.decrypt(userDTO.token!!)
        val tokenArray = decryptedToken.split("#")
        var dbUser = userRepository.getOne(java.lang.Long.valueOf(tokenArray[0]))

        //TODO need test on the date of the token to confirm it is still valid
        if (dbUser != null) {
            val str = tokenArray[1]
            val tokenDate = LocalDateTime.parse(str,  DateTimeFormatter.ISO_DATE_TIME)
            if (!tokenDate.isBefore(LocalDateTime.now().minusDays(2)) || tokenDate == LocalDateTime.now()) {
                if (dbUser.defaultPassword && Crypt.encrypt(userDTO.oldPassword!!) == dbUser.oldPassword) {
                    if (userDTO.password ==userDTO.confirmPassword) {
                        dbUser.password = BCryptPasswordEncoder().encode(userDTO.password)
                        dbUser.defaultPassword = false

                        dbUser = userRepository.save(dbUser)

                        return login(dbUser!!.email, userDTO.password!!)
                    } else {
                        return ResponseDTO(MessageFactory.getMessage("lb_password_not_match", dbUser.language), 404, null)
                    }
                } else {
                    return ResponseDTO(MessageFactory.getMessage("lb_wrong_password_review_email", dbUser.language), 404, null)
                }
            } else {
                return ResponseDTO(MessageFactory.getMessage("lb_expired_temporary_password", dbUser.language), 404, null)
            }
        } else {
            return ResponseDTO(MessageFactory.getMessage("lb_expired_temporary_password", "en"), 404, null)
        }
    }
}