package thinksoft.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import thinksoft.entities.CreditCard
import thinksoft.entities.User
import thinksoft.entities.dto.CreditCardDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.repository.CreditCardRepository
import thinksoft.repository.UserRepository
import thinksoft.service.CreditCardService
import thinksoft.utils.MessageFactory
import java.text.SimpleDateFormat
import java.util.*

@Service("creditCardService")
class CreditCardServiceImpl: CreditCardService {

    @Qualifier("creditCardRepository")
    @Autowired
    lateinit var creditCardRepository: CreditCardRepository

    @Qualifier("userRepository")
    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    override fun save(creditCardDTO: CreditCardDTO): ResponseDTO {
        if (creditCardDTO.user == null) return ResponseDTO(MessageFactory.getMessage("lb_user_not_found_for_creditcard", "en"), 400, null)
        var user = getSuperUser(userRepository.findById(creditCardDTO.user?.id).get())
        creditCardDTO.user = user.toDTO()

        if(creditCardDTO.expireDate == null) {

            val sdf = SimpleDateFormat("dd/MM/yyyy")
            val date = sdf.parse(creditCardDTO.expire)
            var current = Calendar.getInstance()
            current.time = date
            creditCardDTO.expireDate = current
        }

        var creditCard = CreditCard.fromDTO(creditCardDTO)
        creditCard.user = user

        return ResponseDTO(MessageFactory.getMessage("lb_creditCard_saved", user.language),200, creditCardRepository.save(creditCard).toDTO())
    }

    override fun findById(id: Long, userId: Long): ResponseDTO {
        var creditCardDTO = creditCardRepository.findById(id).get().toDTO()
        if (creditCardDTO.user!!.id != userId) return ResponseDTO("Bad Request", 400, null)
        return ResponseDTO("Success!!!", 200, creditCardDTO)
    }

    override fun findByUser(userId: Long): MutableList<CreditCard> {
        var user = userRepository.findById(userId).get()

        user = getSuperUser(user)

        return creditCardRepository.findByUser(user)
    }

    override fun delete(id: Long, userId: Long): ResponseDTO {
        var account = creditCardRepository.findById(id).get()
        if (account.user!!.id != userId) return ResponseDTO("Bad Request", 400, null)
        creditCardRepository.delete(account)
        return ResponseDTO(MessageFactory.getMessage("lb_creditCard_deleted", account.user.language), 200, null)
    }

    private fun getSuperUser(user: User): User {
        var retUser = user
        if (retUser == null) {
            throw Exception("User not found!")
        } else {
            if (user.superUser != null) retUser = user.superUser!!
        }
        return retUser
    }
}