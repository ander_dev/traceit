package thinksoft.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service
import thinksoft.entities.*
import thinksoft.entities.dto.DescriptionDTO
import thinksoft.entities.dto.RegisterDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.repository.*
import thinksoft.service.RegisterService
import thinksoft.utils.Crypt
import thinksoft.utils.MessageFactory
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.ZoneId
import java.time.ZonedDateTime

@Service("registerService")
class RegisterServiceImpl : RegisterService {

    @Qualifier("registerRepository")
    @Autowired
    lateinit var registerRepository: RegisterRepository

    @Qualifier("userRepository")
    @Autowired
    lateinit var userRepository: UserRepository

    @Qualifier("descriptionRepository")
    @Autowired
    lateinit var descriptionRepository: DescriptionRepository

    @Qualifier("instalmentRepository")
    @Autowired
    lateinit var instalmentRepository: InstalmentRepository

    @Qualifier("accountRepository")
    @Autowired
    lateinit var accountRepository: AccountRepository

    override fun save(registerDTO: RegisterDTO): ResponseDTO {
        if (registerDTO.user == null || registerDTO.user!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_user_not_found_for_register", "en"), 400, null)
        var user = getSuperUser(userRepository.findById(registerDTO.user?.id).get())
        registerDTO.user = user.toDTO()

        if (registerDTO.typeAccount == null || registerDTO.typeAccount!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_typeaccount_not_found", registerDTO.user!!.language), 400, null)
        if (registerDTO.description == null || registerDTO.description!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_description_not_found", registerDTO.user!!.language), 400, null)
        if (registerDTO.group == null || registerDTO.group!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_group_not_found", registerDTO.user!!.language), 400, null)
        if (registerDTO.superGroup == null || registerDTO.superGroup!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_super_group_not_found", registerDTO.user!!.language), 400, null)
        if (registerDTO.account == null || registerDTO.account!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_account_not_found", registerDTO.user!!.language), 400, null)
        if (registerDTO.amount == null || registerDTO.amount!! == BigDecimal.ZERO) return ResponseDTO(MessageFactory.getMessage("lb_amount_not_informed", registerDTO.user!!.language), 400, null)
        if (registerDTO.id != null && registerDTO.date == null) return ResponseDTO(MessageFactory.getMessage("lb_date_is_null", registerDTO.user!!.language), 400, null)

        if (registerDTO.id == null && registerDTO.date == null) {
            registerDTO.date = ZonedDateTime.now(ZoneId.of(user.timeZoneName))
        } else {
            registerDTO.date = registerDTO.date!!.withZoneSameInstant(ZoneId.of(user.timeZoneName))
        }

        registerDTO.description = getDescription(registerDTO.description!!, user, registerDTO.typeAccount!!.description!!.capitalize())
        registerDTO.group = getDescription(registerDTO.group!!, user, "group".capitalize())
        registerDTO.superGroup = getDescription(registerDTO.superGroup!!, user, "super-group".capitalize())

        var register = Register.fromDTO(registerDTO)
        register.user = user
        register.description = descriptionRepository.getOne(registerDTO.description!!.id)
        register.group = descriptionRepository.getOne(registerDTO.group!!.id)
        register.superGroup = descriptionRepository.getOne(registerDTO.superGroup!!.id)

        var newRegisterDTO: RegisterDTO? = null

        if (register.nrInstalment != null && register.nrInstalment!! > 1) {
            var instalment: Instalment? = null
            for (x in 1..register.nrInstalment!!) {
                if (x != 1) {
                    register = Register.newObject(register)
                    register.date = getInstalmentDate(register)
                } else {
                    setupCreditCardDate(register)
                }

                if (instalment == null) {
                    instalment = instalmentRepository.save(Instalment.new(register.nrInstalment!!, register.amount, register.user))
                }
                register.amount = instalment!!.amount.divide(BigDecimal(instalment.nrInstalments), 2, RoundingMode.HALF_UP)
                if (x == 1) balanceAccount(register)
                register.instalment = instalment
                newRegisterDTO = registerRepository.save(register).toDTO()
            }
        } else {
            setupCreditCardDate(register)
            balanceAccount(register)
            newRegisterDTO = registerRepository.save(register).toDTO()
        }

        return ResponseDTO(MessageFactory.getMessage("lb_register_saved", registerDTO.user!!.language), 200, newRegisterDTO)
    }

    private fun balanceAccount(register: Register) {
        if (register.creditCard == null && register.id == null && !register.accounted) {
            val balance = register.account.totalGeneral
            if (register.typeAccount.commonDescription == "E"){
                register.account.totalGeneral = balance!!.minus(register.amount)
            } else if (register.typeAccount.commonDescription == "I") {
                register.account.totalGeneral = balance!!.plus(register.amount)
            }
            register.accounted = true
            accountRepository.save(register.account)
        } else if (register.creditCard == null && register.id != null && register.accounted) {
            val registerDB = registerRepository.findById(register.id).get()

            var balance = registerDB.account.totalGeneral

            if (register.typeAccount.commonDescription == "E" && registerDB.amount != register.amount){
                balance = balance!!.plus(registerDB.amount)
                register.account.totalGeneral = balance!!.minus(register.amount)
            } else if (register.typeAccount.commonDescription == "I") {
                balance = balance!!.minus(registerDB.amount)
                register.account.totalGeneral = balance!!.plus(register.amount)
            }
            register.accounted = true
            accountRepository.save(register.account)
        }
    }

    private fun getInstalmentDate(register: Register): ZonedDateTime {

        return when (register.account.typeClosure.commonDescription) {
            "M" -> register.date!!.plusMonths(1).withZoneSameInstant(ZoneId.of(register.user.timeZoneName))
            "F" -> register.date!!.plusWeeks(2).withZoneSameInstant(ZoneId.of(register.user.timeZoneName))
            "W" -> register.date!!.plusWeeks(1).withZoneSameInstant(ZoneId.of(register.user.timeZoneName))
            "Y" -> register.date!!.plusYears(1).withZoneSameInstant(ZoneId.of(register.user.timeZoneName))
            "D" -> register.date!!.plusDays(1).withZoneSameInstant(ZoneId.of(register.user.timeZoneName))
            else -> register.date!!
        }

    }

    private fun setupCreditCardDate(register: Register) {
        if(register.creditCard != null){
            val dayOfBuy = register.date!!.dayOfMonth
            if (dayOfBuy <= register.creditCard!!.bestDay) {
                register.date = register.date!!.withDayOfMonth(register.creditCard!!.payDay).withZoneSameInstant(ZoneId.of(register.user.timeZoneName))
            } else {
                register.date = register.date!!.withDayOfMonth(register.creditCard!!.payDay).plusMonths(1).withZoneSameInstant(ZoneId.of(register.user.timeZoneName))
            }
        }
    }

    private fun getDescription(descriptionDTO: DescriptionDTO, user: User, typeAccount: String?): DescriptionDTO {
        val typeAccountId = getTypeAccountId(typeAccount!!, user!!.language!!)
        return try {
            val description = descriptionRepository.findByDescriptionAndTypeAccountAndUser(Crypt.encrypt(descriptionDTO.description!!), TypeAccount.new(typeAccountId.toLong()), user)
            description.toDTO()
        } catch (ex: EmptyResultDataAccessException) {
            descriptionDTO.typeAccount = TypeAccount(typeAccountId.toLong())
            descriptionDTO.user = user.toDTO()
            val desc = Description.fromDTO(descriptionDTO)
            desc.user = user
            val description = descriptionRepository.save(desc)
            description.toDTO()
        }
    }

    private fun getTypeAccountId(typeAccount: String, userLanguage: String): Int {
        var typeAccountId = 0
        when (typeAccount) {
            "expense".capitalize() -> typeAccountId = if (userLanguage == "pt_BR") 6 else 2
            "income".capitalize() -> typeAccountId = if (userLanguage == "pt_BR") 5 else 1
            "group".capitalize() -> typeAccountId = if (userLanguage == "pt_BR") 7 else 3
            "super-group".capitalize() -> typeAccountId = if (userLanguage == "pt_BR") 8 else 4
        }

        return typeAccountId
    }

    override fun findById(id: Long, userId: Long): ResponseDTO {
        val registerDTO = registerRepository.findById(id).get().toDTO()
        if (registerDTO.user!!.id != userId) return ResponseDTO("Bad Request", 400, null)
        return ResponseDTO("Success!!!", 200, registerDTO)
    }

    override fun findByUser(userId: Long): MutableList<Register> {
        var user = userRepository.findById(userId).get()

        user = getSuperUser(user)

        return registerRepository.findByUser(user)
    }

    override fun findByUserAndInstalment(userId: Long, instalmentId: Long): MutableList<Register> {

        var user = userRepository.findById(userId).get()

        user = getSuperUser(user)

        var instalment = instalmentRepository.findById(instalmentId)

        if (!instalment.isPresent) throw Exception("opsy")

        return registerRepository.findByUserAndInstalmentOrderByDate(user, instalment.get())
    }

    override fun delete(id: Long, userId: Long): ResponseDTO {
        var register = registerRepository.findById(id).get()
        if (register.user!!.id != userId) return ResponseDTO("Bad Request", 400, null)
        registerRepository.delete(register)
        return ResponseDTO(MessageFactory.getMessage("lb_register_deleted", register.user.language), 200, null)
    }

    private fun getSuperUser(user: User): User {
        var retUser = user
        if (retUser == null) {
            throw Exception("User not found!")
        } else {
            if (user.superUser != null) retUser = user.superUser!!
        }
        return retUser
    }
}