package thinksoft.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import thinksoft.entities.TypeClosure
import thinksoft.repository.TypeClosureRepository
import thinksoft.service.TypeClosureService

@Service("typeClosureService")
class TypeClosureServiceImpl: TypeClosureService {

    @Qualifier("typeClosureRepository")
    @Autowired
    lateinit var typeClosureRepository: TypeClosureRepository

    override fun findByLocale(locale: String): MutableList<TypeClosure> {
        var locale = locale
        if (locale == null || "pt_BR" != locale) {
            locale = "en"
        }
        return  typeClosureRepository.findByLocale(locale)
    }

    override fun findById(id: Long): TypeClosure {
        return typeClosureRepository.findById(id).get()
    }

    override fun findByDescription(description: String): TypeClosure {
        return typeClosureRepository.findByDescription(description)
    }
}