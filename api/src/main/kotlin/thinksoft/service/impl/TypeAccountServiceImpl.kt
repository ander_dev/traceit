package thinksoft.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import thinksoft.entities.TypeAccount
import thinksoft.repository.TypeAccountRepository
import thinksoft.service.TypeAccountService

@Service("typeAccountService")
class TypeAccountServiceImpl: TypeAccountService {

    @Qualifier("typeAccountRepository")
    @Autowired
    lateinit var typeAccountRepository: TypeAccountRepository

    override fun findByLocale(locale: String): MutableList<TypeAccount> {
        var finalLocale = locale
        if (locale == null || "pt_BR" != locale) {
            finalLocale = "en"
        }
        return typeAccountRepository.findByLocale(finalLocale)
    }

    override fun findById(id: Long): TypeAccount {
        return typeAccountRepository.findById(id).get()
    }

    override fun findByDescription(description: String): TypeAccount {
        return typeAccountRepository.findByDescription(description)
    }
}