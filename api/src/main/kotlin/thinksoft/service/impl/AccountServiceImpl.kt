package thinksoft.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service
import thinksoft.entities.Account
import thinksoft.entities.User
import thinksoft.entities.dto.AccountDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.repository.AccountRepository
import thinksoft.repository.CurrencyRepository
import thinksoft.repository.TypeClosureRepository
import thinksoft.repository.UserRepository
import thinksoft.service.AccountService
import thinksoft.utils.MessageFactory
import java.util.*

@Service("accountService")
class AccountServiceImpl: AccountService {

    @Qualifier("accountRepository")
    @Autowired
    lateinit var accountRepository: AccountRepository

    @Qualifier("userRepository")
    @Autowired
    lateinit var userRepository: UserRepository

    @Qualifier("currencyRepository")
    @Autowired
    lateinit var currencyRepository: CurrencyRepository

    @Qualifier("typeClosureRepository")
    @Autowired
    lateinit var typeClosureRepository: TypeClosureRepository

    override fun save(accountDTO: AccountDTO): ResponseDTO {
        if (accountDTO.user == null || accountDTO.user!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_user_not_found_for_account", "en"), 400, null)
        var user = getSuperUser(userRepository.findById(accountDTO.user?.id).get())
        accountDTO.user = user.toDTO()

        if (accountDTO.currency == null || accountDTO.currency!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_currency_not_found", accountDTO.user!!.language), 400, null)
        accountDTO.currency = currencyRepository.findById(accountDTO.currency?.id).get()

        if (accountDTO.typeClosure == null|| accountDTO.typeClosure!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_typeClosure_not_found", accountDTO.user!!.language), 400, null)
        accountDTO.typeClosure = typeClosureRepository.findById(accountDTO.typeClosure?.id).get()

        if(accountDTO.id == null){
            TimeZone.setDefault(TimeZone.getTimeZone(user.timeZoneName))
            accountDTO.startDate = Calendar.getInstance()
        }

        try {
            var currentDefault = accountRepository.findByDefaultAccountAndUser(true, user)
            if(accountDTO.defaultAccount){
                currentDefault.defaultAccount = false
                accountRepository.save(currentDefault)
            }
        } catch (ex: EmptyResultDataAccessException){
            accountDTO.defaultAccount = true
        }


        var account = Account.fromDTO(accountDTO)
        account.user = user

        var newAccountDTO = accountRepository.save(account).toDTO()

        return ResponseDTO(MessageFactory.getMessage("lb_account_saved", accountDTO.user!!.language), 200, newAccountDTO)
    }

    override fun findById(id: Long, userId: Long): ResponseDTO {
        val accountDTO = accountRepository.findById(id).get().toDTO()
        if (accountDTO.user!!.id != userId) return ResponseDTO("Bad Request", 400, null)
        return ResponseDTO("Success!!!", 200, accountDTO)

    }

    override fun findByUser(userId: Long): MutableList<Account> {
        var user = userRepository.findById(userId).get()

        user = getSuperUser(user)

        return accountRepository.findByUser(user)
    }

    override fun delete(id: Long, userId: Long): ResponseDTO {
        var account = accountRepository.findById(id).get()
        if (account.user.id != userId) return ResponseDTO("Bad Request", 400, null)
        accountRepository.delete(account)
        return ResponseDTO(MessageFactory.getMessage("lb_account_deleted", account.user.language), 200, null)
    }

    private fun getSuperUser(user: User): User {
        var retUser = user
        if (retUser == null) {
            throw Exception("User not found!")
        } else {
            if (user.superUser != null) retUser = user.superUser!!
        }
        return retUser
    }
}