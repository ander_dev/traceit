package thinksoft.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import thinksoft.entities.Currency
import thinksoft.repository.CurrencyRepository
import thinksoft.service.CurrencyService

@Service("currencyService")
class CurrencyServiceImpl: CurrencyService {

    @Qualifier("currencyRepository")
    @Autowired
    lateinit var currencyRepository: CurrencyRepository

    override fun findByLocale(locale: String): MutableList<Currency> {
        var finalLocale = locale
        if (locale == null || "pt_BR" != locale) {
            finalLocale = "en"
        }
        return currencyRepository.findByLocale(finalLocale)
    }

    override fun findById(id: Long): Currency {
        return currencyRepository.findById(id).get()
    }

    override fun findByDescriptionAndLocale(description: String, locale: String): Currency {
        return currencyRepository.findByDescriptionAndLocale(description, locale)
    }

    override fun findByAcronymAndLocale(acronym: String, locale: String): Currency {
        return currencyRepository.findByAcronymAndLocale(acronym, locale)
    }
}