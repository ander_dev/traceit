package thinksoft.service.impl

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import thinksoft.entities.Chart
import thinksoft.entities.User
import thinksoft.entities.dto.ChartDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.repository.ChartRepository
import thinksoft.repository.DescriptionRepository
import thinksoft.repository.UserRepository
import thinksoft.service.ChartService
import thinksoft.utils.MessageFactory

@Service("chartService")
class ChartServiceImpl: ChartService {

    @Qualifier("chartRepository")
    @Autowired
    lateinit var chartRepository: ChartRepository

    @Qualifier("userRepository")
    @Autowired
    lateinit var userRepository: UserRepository

    @Qualifier("descriptionRepository")
    @Autowired
    lateinit var descriptionRepository: DescriptionRepository

    override fun save(chartDTO: ChartDTO): ChartDTO {
        if (chartDTO.user?.id == null) throw Exception("User not found!")
        var user = getSuperUser(userRepository.findById(chartDTO.user?.id).get())
        chartDTO.user = user.toDTO()

        if (chartDTO.description?.id == null) throw Exception("Description not found!")
        var description = descriptionRepository.findById(chartDTO.description?.id).get()
        chartDTO.description = description.toDTO()

        chartDTO.isCredit = description.typeAccount?.description == "Credit" || description.typeAccount?.description == "Crédito"

        val chart = Chart.fromDTO(chartDTO)
        chart.user = user
        chart.description = description

        return chartRepository.save(chart).toDTO()
    }

    override fun findById(id: Long, userId: Long): ResponseDTO {
        val chartDTO = chartRepository.findById(id).get().toDTO()
        if(chartDTO.user!!.id != userId) return ResponseDTO("Bad Request", 400, null)
        return ResponseDTO("sucess!!!", 200, chartRepository.findById(id).get().toDTO())
    }

    override fun findByUser(userId: Long): MutableList<ChartDTO> {
        var user = userRepository.findById(userId).get()

        user = getSuperUser(user)
        var chartListDTO: MutableList<ChartDTO> = arrayListOf()
        val charts = chartRepository.findByUser(user)
        charts.forEach { chartListDTO.add(it.toDTO()) }

        return chartListDTO
    }

    override fun findByDescription(chartDTO: ChartDTO): MutableList<Chart> {
        if (chartDTO.user?.id == null) throw Exception("User not found!")
        var user = getSuperUser(userRepository.findById(chartDTO.user?.id).get())

        if (chartDTO.description?.id == null) throw Exception("Description not found!")
        var description = descriptionRepository.findById(chartDTO.description?.id).get()

        return chartRepository.findByDescriptionAndUser(description, user)
    }

    override fun findByDescriptionAndDate(chartDTO: ChartDTO): ResponseDTO {
        if (chartDTO.user?.id == null) throw Exception("User not found!")
        var user = getSuperUser(userRepository.findById(chartDTO.user?.id).get())

        if (chartDTO.description?.id == null) throw Exception("Description not found!")
        var description = descriptionRepository.findById(chartDTO.description?.id).get()

        var list = chartRepository.findByDescriptionAndUser(description, user)

        list.iterator().forEach {
            val chart = ObjectMapper().readValue(ObjectMapper().writeValueAsBytes(it), Chart::class.java)
            if(chart.validFor == chartDTO.validFor){
                return ResponseDTO("sucess!!!", 200, chart.toDTO())
            }
        }
        return ResponseDTO("Error!!!", 400, null)
    }

    override fun delete(id: Long, userId: Long): ResponseDTO {
        var chart = chartRepository.findById(id).get()
        if (chart.user.id != userId) return ResponseDTO("Bad Request", 400, null)
        chartRepository.delete(chart)
        return ResponseDTO(MessageFactory.getMessage("lb_chart_deleted", chart.user.language), 200, null)
    }

    private fun getSuperUser(user: User): User {
        var retUser = user
        if (retUser == null) {
            throw Exception("User not found!")
        } else {
            if (user.superUser != null) retUser = user.superUser!!
        }
        return retUser
    }
}