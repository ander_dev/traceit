package thinksoft.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import thinksoft.entities.Description
import thinksoft.entities.TypeAccount
import thinksoft.entities.User
import thinksoft.entities.dto.DescriptionDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.repository.DescriptionRepository
import thinksoft.repository.TypeAccountRepository
import thinksoft.repository.UserRepository
import thinksoft.service.DescriptionService
import thinksoft.utils.Crypt
import thinksoft.utils.MessageFactory

@Service("descriptionService")
class DescriptionServiceImpl: DescriptionService {

    @Qualifier("userRepository")
    @Autowired
    lateinit var userRepository: UserRepository

    @Qualifier("typeAccountRepository")
    @Autowired
    lateinit var typeAccountRepository: TypeAccountRepository

    @Qualifier( "descriptionRepository")
    @Autowired
    lateinit var descriptionRepository: DescriptionRepository

    @Transactional
    override fun save(descriptionDTO: DescriptionDTO): ResponseDTO {
        var typeAccount: TypeAccount
        if (descriptionDTO.user == null || descriptionDTO.user!!.id == 0L) return ResponseDTO(MessageFactory.getMessage("lb_user_not_found_for_description", "en"), 400, null)
        var user = userRepository.findById(descriptionDTO.user?.id).get()
        if(descriptionDTO.description.isNullOrBlank()) return ResponseDTO(MessageFactory.getMessage("lb_description_not_informed", user.language), 400, null)

        user = getSuperUser(user)

        descriptionDTO.user = user.toDTO()

        var description = Description.fromDTO(descriptionDTO)

        if (descriptionDTO.typeAccountList.isNullOrEmpty() && descriptionDTO.typeAccount != null) {
            var typeAccountOptional = typeAccountRepository.findById(descriptionDTO.typeAccount?.id)
            if (!typeAccountOptional.isPresent) return ResponseDTO(MessageFactory.getMessage("lb_typeaccount_mandatory", user.language), 400, null)
            typeAccount = typeAccountOptional.get()
            description.typeAccount = typeAccount
            description.user = user

            description = descriptionRepository.save(description)
        } else if (!descriptionDTO.typeAccountList.isNullOrEmpty()) {
            var savedDescription = ""
            if (descriptionDTO.id != null) savedDescription = descriptionRepository.findById(descriptionDTO.id!!).get().description!!

            var accountIterator = descriptionDTO.typeAccountList!!.iterator()
            var listTested = false
            accountIterator.forEach {

                if(descriptionDTO.id == null) description = Description.fromDTO(descriptionDTO)

                typeAccount = typeAccountRepository.findById( it.id ).get()
                description.user = user

                if (descriptionDTO.id != null) {
                    var desc = descriptionRepository.findByDescriptionAndTypeAccountAndUser(savedDescription, typeAccount, user)

                    if(!listTested){
                        var originalDescription = getDescriptionByDescription(user.id!!, Crypt.decrypt(desc.description!!))
                        if(originalDescription.typeAccountList!!.size != descriptionDTO.typeAccountList!!.size){
                            originalDescription.typeAccountList!!.iterator().forEach { it ->
                                if(!descriptionDTO.typeAccountList!!.contains(it)) {
                                    var typeAccountHelper = typeAccountRepository.findById( it.id ).get()
                                    var descToBeDeleted = descriptionRepository.findByDescriptionAndTypeAccountAndUser(Crypt.encrypt(originalDescription.description!!), typeAccountHelper, user)
                                    delete(descToBeDeleted.id!!, originalDescription.user!!.id!!)
                                }
                            }
                        }
                        listTested = true
                    }

                    description = descriptionRepository.findByDescriptionAndTypeAccountAndUser(desc.description!!, typeAccount, user)
                    description.description = Crypt.encrypt(descriptionDTO.description!!)
                }
                description.typeAccount = typeAccount
                description = descriptionRepository.save(description)
            }
        }
        else {
            throw Exception("Either TypeAccount or TypeAccountIdList must be informed!")
        }
        return ResponseDTO(MessageFactory.getMessage("lb_description_saved", description.toDTO().user!!.language), 200, description.toDTO())
    }

    override fun delete(id: Long, userId: Long): ResponseDTO {
        var description = descriptionRepository.findById(id).get()
        if (description.user!!.id != userId) return ResponseDTO("Bad Request", 400, null)
        descriptionRepository.delete(description)
        return ResponseDTO("Success!!!", 200, null)
    }

    override fun findByUser(userId: Long): MutableList<Description> {

        var user = userRepository.findById(userId).get()

        user = getSuperUser(user)

        var list = descriptionRepository.findByUser(user)
        return list
    }

    override fun findByTypeAccountAndUser(userId: Long, typeAccountId: Long): MutableList<Description> {
        var user = userRepository.findById(userId).get()

        user = getSuperUser(user)

        var typeAccount = typeAccountRepository.findById(typeAccountId).get()

        var list = descriptionRepository.findByTypeAccountAndUser(typeAccount, user)
        return list
    }

    override fun findById(id: Long, userId: Long): ResponseDTO {
        var description = descriptionRepository.findById(id).get()
        if (description.user!!.id != userId) return ResponseDTO("Bad Request", 400, null)
        return ResponseDTO("Success!!!", 200, description.toDTO())
    }

    private fun getDescriptionByDescription(userId: Long, description: String): DescriptionDTO {
        var userOptional = userRepository.findById(userId)
        if (!userOptional.isPresent) throw Exception(MessageFactory.getMessage("lb_user_mandatory", "en"))

        var user = getSuperUser(userOptional.get())

        var desc = descriptionRepository.findByDescriptionAndUser(Crypt.encrypt(description), user)

        if (desc.isEmpty()) throw Exception(MessageFactory.getMessage("lb_description_not_found", user!!.language))
        if (desc.size > 1) {
            var dto = desc[0].toDTO()
            dto.typeAccount = null
            val typeAccounts = mutableListOf<TypeAccount>()
            desc.iterator().forEach {
                typeAccounts.run { add(it.typeAccount!!) }
            }
            dto.typeAccountList = typeAccounts
            return dto
        }
        return desc[0].toDTO()
    }

    override fun findByDescription(userId: Long, description: String): ResponseDTO {
        return try {
            var descriptionDTO = getDescriptionByDescription(userId, description)
            ResponseDTO(MessageFactory.getMessage("lb_description_saved", descriptionDTO.user!!.language), 200, descriptionDTO)
        } catch (ex: Exception) {
            ResponseDTO(ex.message!!, 400, null)
        }
    }

    private fun getSuperUser(user: User): User{
        var retUser = user
        if (retUser == null) {
            throw Exception("User not found!")
        } else {
            if (user.superUser != null) retUser = user.superUser!!
        }
        return retUser
    }
}