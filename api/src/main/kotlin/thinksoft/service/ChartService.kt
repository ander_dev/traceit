package thinksoft.service

import thinksoft.entities.Chart
import thinksoft.entities.dto.ChartDTO
import thinksoft.entities.dto.ResponseDTO

interface ChartService {

    fun findById(id: Long, userId: Long): ResponseDTO

    fun save(chartDTO: ChartDTO): ChartDTO

    fun findByDescription(chartDTO: ChartDTO): MutableList<Chart>

    fun findByDescriptionAndDate(chart: ChartDTO): ResponseDTO

    fun findByUser(userId: Long): MutableList<ChartDTO>

    fun delete(id: Long, userId: Long): ResponseDTO
}