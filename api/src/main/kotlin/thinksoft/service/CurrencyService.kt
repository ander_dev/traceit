package thinksoft.service

import thinksoft.entities.Currency

interface CurrencyService {

    fun findByLocale(locale: String): MutableList<Currency>

    fun findById(id: Long): Currency

    fun findByDescriptionAndLocale(description: String, locale: String): Currency

    fun findByAcronymAndLocale(acronym: String, locale: String): Currency
}