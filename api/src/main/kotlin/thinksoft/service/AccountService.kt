package thinksoft.service

import thinksoft.entities.Account
import thinksoft.entities.dto.AccountDTO
import thinksoft.entities.dto.ResponseDTO

interface AccountService {

    fun findById(id: Long, userId: Long): ResponseDTO

    fun save(accountDTO: AccountDTO): ResponseDTO

    fun findByUser(userId: Long): MutableList<Account>

    fun delete(id: Long, userId: Long): ResponseDTO
}