package thinksoft.service

import thinksoft.entities.Description
import thinksoft.entities.dto.DescriptionDTO
import thinksoft.entities.dto.ResponseDTO

interface DescriptionService {

    fun save(descriptionDTO: DescriptionDTO): ResponseDTO

    fun delete(id: Long, userId: Long): ResponseDTO

    fun findByUser(userId: Long): MutableList<Description>

    fun findByTypeAccountAndUser(userId: Long, typeAccountId: Long): MutableList<Description>

    fun findById(id: Long, userId: Long): ResponseDTO

    fun findByDescription(userId: Long, description: String): ResponseDTO
}