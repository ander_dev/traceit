package thinksoft.service

import thinksoft.entities.TypeClosure

interface TypeClosureService {

    fun findByLocale(locale: String): MutableList<TypeClosure>

    fun findById(id: Long): TypeClosure

    fun findByDescription(description: String): TypeClosure
}