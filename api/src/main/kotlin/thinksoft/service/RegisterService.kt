package thinksoft.service

import thinksoft.entities.Register
import thinksoft.entities.dto.RegisterDTO
import thinksoft.entities.dto.ResponseDTO

interface RegisterService {

    fun findById(id: Long, userId: Long): ResponseDTO

    fun save(accountDTO: RegisterDTO): ResponseDTO

    fun findByUser(userId: Long): MutableList<Register>

    fun findByUserAndInstalment(userId: Long, instalmentId: Long): MutableList<Register>

    fun delete(id: Long, userId: Long): ResponseDTO
}