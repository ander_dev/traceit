package thinksoft.service

import thinksoft.entities.CreditCard
import thinksoft.entities.dto.CreditCardDTO
import thinksoft.entities.dto.ResponseDTO

interface CreditCardService {

    fun findById(id: Long, userId: Long): ResponseDTO

    fun save(creditCardDTO: CreditCardDTO): ResponseDTO

    fun findByUser(userId: Long): MutableList<CreditCard>

    fun delete(id: Long, userId: Long): ResponseDTO
}