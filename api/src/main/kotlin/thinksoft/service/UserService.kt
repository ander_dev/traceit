package thinksoft.service

import thinksoft.entities.User
import thinksoft.entities.dto.ResponseDTO
import thinksoft.entities.dto.UserDTO
import javax.security.auth.login.CredentialNotFoundException

interface UserService {

    fun findByEmail(email: String) : UserDTO

    fun findById(id: Long) : UserDTO

    fun save(userDTO: UserDTO) : ResponseDTO

    fun login(email: String, password: String) : ResponseDTO?

    fun delete(id: Long): ResponseDTO

    fun findAll() : MutableList<User>

    fun findAllById(id: Long) : MutableList<User>

    fun loginByToken(token: String): ResponseDTO?

    @Throws(CredentialNotFoundException::class)
    fun forgetPassword(email: String, redirectURL: String) : ResponseDTO

    @Throws(CredentialNotFoundException::class)
    fun newPassword(userDTO: UserDTO): ResponseDTO?
}

