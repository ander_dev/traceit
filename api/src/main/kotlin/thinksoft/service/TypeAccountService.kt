package thinksoft.service

import thinksoft.entities.TypeAccount

interface TypeAccountService {

    fun findByLocale(locale: String): MutableList<TypeAccount>

    fun findById(id: Long): TypeAccount

    fun findByDescription(description: String): TypeAccount
}