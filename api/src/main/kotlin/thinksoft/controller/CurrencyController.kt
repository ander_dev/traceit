package thinksoft.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import thinksoft.entities.Currency
import thinksoft.service.CurrencyService
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/traceit/currency"], produces = [MediaType.APPLICATION_JSON])
class CurrencyController {

    @Autowired
    lateinit var currencyService: CurrencyService

    @GetMapping("/by-locale/{locale}")
    fun findAllByLocale(@PathVariable("locale") locale: String): MutableList<Currency> = currencyService.findByLocale(locale)

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: Long): Currency = currencyService.findById(id)

    @GetMapping("/by-description-locale/{description}/{locale}")
    fun findAllByDescriptionAndLocale(@PathVariable("description") description: String, @PathVariable("locale") locale: String): Currency = currencyService.findByDescriptionAndLocale(description, locale)

    @GetMapping("/by-acronym-locale/{acronym}/{locale}")
    fun findAllByAcronymAndLocale(@PathVariable("acronym") acronym: String, @PathVariable("locale") locale: String): Currency = currencyService.findByAcronymAndLocale(acronym, locale)
}