package thinksoft.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import thinksoft.entities.CreditCard
import thinksoft.entities.dto.CreditCardDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.service.CreditCardService
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/traceit/credit-card"], produces = [MediaType.APPLICATION_JSON])
class CreditCardController {

    @Autowired
    lateinit var creditCardService: CreditCardService

    @RequestMapping( method = [RequestMethod.POST])
    fun save(@RequestBody creditCardDTO: CreditCardDTO): Any = creditCardService.save(creditCardDTO)

    @GetMapping("/{id}/{userId}")
    fun findById(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = creditCardService.findById(id, userId)

    @GetMapping("/by-user/{userId}")
    fun findByUser(@PathVariable("userId") userId: Long):  MutableList<CreditCard> = creditCardService.findByUser(userId)

    @RequestMapping("/{id}/{userId}", method = [RequestMethod.DELETE])
    fun delete(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = creditCardService.delete(id, userId)
}