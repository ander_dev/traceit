package thinksoft.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import thinksoft.entities.TypeAccount
import thinksoft.service.TypeAccountService
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = arrayOf("/traceit/type-account"), produces = arrayOf(MediaType.APPLICATION_JSON))
class TypeAccountController {

    @Autowired
    lateinit var typeAccountService: TypeAccountService

    @GetMapping("/by-locale/{locale}")
    fun findAllByLocale(@PathVariable("locale") locale: String): MutableList<TypeAccount> = typeAccountService.findByLocale(locale)

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: Long): TypeAccount = typeAccountService.findById(id)

    @GetMapping("/by-description/{description}")
    fun findAllByDescription(@PathVariable("description") description: String):  TypeAccount = typeAccountService.findByDescription(description)
}