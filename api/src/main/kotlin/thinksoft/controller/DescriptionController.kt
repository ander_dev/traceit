package thinksoft.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import thinksoft.entities.Description
import thinksoft.entities.dto.DescriptionDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.service.DescriptionService
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/traceit/description"], produces = [MediaType.APPLICATION_JSON])
class DescriptionController {

    @Autowired
    lateinit var descriptionService: DescriptionService

    @RequestMapping( method = [RequestMethod.POST])
    fun save(@RequestBody descriptionDTO: DescriptionDTO): ResponseDTO = descriptionService.save(descriptionDTO)

    @RequestMapping("/{id}/{userId}", method = [RequestMethod.DELETE])
    fun delete(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = descriptionService.delete(id, userId)

    @GetMapping("/by-user/{userId}")
    fun findAllByUser(@PathVariable("userId") userId: Long): MutableList<Description> = descriptionService.findByUser(userId)

    @GetMapping("/by-type-account")
    fun findAllByTypeAccountAnd(@PathVariable("userId") userId: Long, @PathVariable("typeAccountId") typeAccountId: Long): MutableList<Description> = descriptionService.findByTypeAccountAndUser(userId, typeAccountId)

    @GetMapping("/{id}/{userId}")
    fun findById(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = descriptionService.findById(id, userId)

    @GetMapping("/by-description/{userId}/{description}")
    fun findAllByDescription(@PathVariable("userId") userId: Long, @PathVariable("description") description: String):  ResponseDTO = descriptionService.findByDescription(userId, description)
}