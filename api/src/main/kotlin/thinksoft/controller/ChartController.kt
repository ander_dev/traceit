package thinksoft.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import thinksoft.entities.dto.ChartDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.service.ChartService
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/traceit/chart"], produces = [MediaType.APPLICATION_JSON])
class ChartController {

    @Autowired
    lateinit var chartService: ChartService

    @RequestMapping( method = [RequestMethod.POST])
    fun save(@RequestBody chartDTO: ChartDTO): Any = chartService.save(chartDTO)

    @GetMapping("/{id}/{userId}")
    fun findById(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = chartService.findById(id, userId)

    @GetMapping("/by-user/{userId}")
    fun findByUser(@PathVariable("userId") userId: Long):  MutableList<ChartDTO> = chartService.findByUser(userId)

//    @RequestMapping(value = ["/by-description"], method = arrayOf(RequestMethod.PUT))
//    fun findByDescription(@RequestBody chartDTO: ChartDTO): MutableList<ChartDTO> {
//        if (chartDTO.validFor != null ) {
//            return mutableListOf(chartService.findByDescriptionAndDate(chartDTO))
//        }
//        return chartService.findByDescription(chartDTO).toMutableList()
//    }

    @RequestMapping("/{id}/{userId}", method = [RequestMethod.DELETE])
    fun delete(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): Any = chartService.delete(id, userId)
}