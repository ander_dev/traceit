package thinksoft.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import thinksoft.entities.Account
import thinksoft.entities.dto.AccountDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.service.AccountService
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/traceit/account"], produces = [MediaType.APPLICATION_JSON])
class AccountController {

    @Autowired
    lateinit var accountService: AccountService

    @RequestMapping( method = [RequestMethod.POST])
    fun save(@RequestBody accountDTO: AccountDTO): ResponseDTO = accountService.save(accountDTO)

    @GetMapping("/{id}/{userId}")
    fun findById(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = accountService.findById(id, userId)

    @GetMapping("/by-user/{userId}")
    fun findByUser(@PathVariable("userId") userId: Long):  MutableList<Account> = accountService.findByUser(userId)

    @RequestMapping("/{id}/{userId}", method = [RequestMethod.DELETE])
    fun delete(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = accountService.delete(id, userId)
}