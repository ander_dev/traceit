package thinksoft.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import thinksoft.entities.TypeClosure
import thinksoft.service.TypeClosureService
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = arrayOf("/traceit/type-closure"), produces = arrayOf(MediaType.APPLICATION_JSON))
class TypeClosureController {

    @Autowired
    lateinit var typeClosureService: TypeClosureService

    @GetMapping("/by-locale/{locale}")
    fun findAllByLocale(@PathVariable("locale") locale: String): MutableList<TypeClosure> = typeClosureService.findByLocale(locale)

    @GetMapping("/{id}")
    fun findById(@PathVariable("id") id: Long): TypeClosure = typeClosureService.findById(id)

    @GetMapping("/by-description/{description}")
    fun findAllByDescription(@PathVariable("description") description: String):  TypeClosure = typeClosureService.findByDescription(description)
}