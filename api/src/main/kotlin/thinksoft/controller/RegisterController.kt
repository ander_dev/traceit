package thinksoft.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import thinksoft.entities.Register
import thinksoft.entities.dto.RegisterDTO
import thinksoft.entities.dto.ResponseDTO
import thinksoft.service.RegisterService
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/traceit/register"], produces = [MediaType.APPLICATION_JSON])
class RegisterController {

    @Autowired
    lateinit var registerService: RegisterService

    @RequestMapping( method = [RequestMethod.POST])
    fun save(@RequestBody registerDTO: RegisterDTO): ResponseDTO = registerService.save(registerDTO)

    @GetMapping("/{id}/{userId}")
    fun findById(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = registerService.findById(id, userId)

    @GetMapping("/by-user/{userId}")
    fun findByUser(@PathVariable("userId") userId: Long):  MutableList<Register> = registerService.findByUser(userId)

    @GetMapping("/by-instalment/{userId}/{instalmentId}")
    fun findByUserAndInstalment(@PathVariable("userId") userId: Long, @PathVariable("instalmentId") instalmentId: Long):  MutableList<Register> = registerService.findByUserAndInstalment(userId,instalmentId)

    @RequestMapping("/{id}/{userId}/", method = [RequestMethod.DELETE])
    fun delete(@PathVariable("id") id: Long, @PathVariable("userId") userId: Long): ResponseDTO = registerService.delete(id, userId)
}