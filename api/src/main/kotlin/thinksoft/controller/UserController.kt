package controller

import io.jsonwebtoken.ExpiredJwtException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.web.bind.annotation.*
import thinksoft.entities.dto.ResponseDTO
import thinksoft.entities.dto.UserDTO
import thinksoft.service.UserService
import java.sql.SQLException
import javax.ws.rs.FormParam
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/traceit/user"], produces = [(MediaType.APPLICATION_JSON)])
class UserController {

    @Autowired
    lateinit var userService: UserService

    @GetMapping()
    fun getUsers() = userService.findAll()

    @RequestMapping("/{id}", method = [(RequestMethod.GET)])
    fun findUsersById(@PathVariable("id") id: Long) = userService.findAllById(id)

    @RequestMapping("/{id}", method = [(RequestMethod.DELETE)])
    fun delete (@PathVariable("id") id: Long): Any = userService.delete(id)

    @RequestMapping( method = [(RequestMethod.POST)])
    fun save (@RequestBody userDTO: UserDTO): Any = userService.save(userDTO)

    @GetMapping("/by-email/{email:.+}")
    fun findByEmail(@PathVariable("email") email: String): UserDTO = userService.findByEmail(email)

    @GetMapping("/by-id/{id}")
    fun findById(@PathVariable("id") id: Long): UserDTO = userService.findById(id)

    @RequestMapping("/login", method = [(RequestMethod.POST)])
    fun login(@FormParam("email") email: String, @FormParam("password") password: String): ResponseDTO? {
        return userService.login(email, password)
    }

    @RequestMapping("/login/by-token", method = [(RequestMethod.POST)])
    fun loginByToken(@FormParam("token") token: String) : ResponseDTO? {
        try {
            return userService.loginByToken(token)
        } catch (e: ExpiredJwtException) {
            throw CredentialsExpiredException(e.message)
        }
    }

    @RequestMapping("/forget", method = [(RequestMethod.POST)])
    fun forgetPassword(@FormParam("email") email: String, @FormParam("redirectTo") redirectTo: String) : ResponseDTO {
        return userService.forgetPassword(email, redirectTo)
    }

    @RequestMapping("/newpass", method = [(RequestMethod.POST)])
    fun newPassword(@RequestBody userDTO: UserDTO): ResponseDTO? = userService.newPassword(userDTO)

    /** Cause an error to occur */
    @RequestMapping("/raiseError")
    fun raiseError() {
        throw IllegalArgumentException("This shouldn't have happened")
    }

    /** Handle the error */
    @ExceptionHandler(SQLException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleError(e: SQLException) = e.message
}