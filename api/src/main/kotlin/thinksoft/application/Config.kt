package thinksoft.application

import controller.UserController
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.transaction.annotation.EnableTransactionManagement
import thinksoft.controller.*


@Configuration
@EntityScan("thinksoft.entities")
@EnableTransactionManagement
internal class Config {

    @Bean
    fun userController() = UserController()

    @Bean
    fun descriptionController() = DescriptionController()

    @Bean
    fun typeAccountController() = TypeAccountController()

    @Bean
    fun typeClosureController() = TypeClosureController()

    @Bean
    fun currencyController() = CurrencyController()

    @Bean
    fun accountController() = AccountController()
}