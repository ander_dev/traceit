import axios from 'axios'

export const userService = {
    login,
    loginByToken,
    logout,
    register,
    getAll,
    forgotPassword,
    newPassword,
    getById,
    update,
    delete: _delete
}

let axiosConfig = {
    baseURL: 'http://localhost:8080/traceit/user',
    headers: {
        'Content-Type': 'application/json;charset=UTF-8',
    }, auth: {
        username: 'Thinksoft',
        password: '@Th1nks0ft'
    }
}

let axiosLoginConfig = {
    baseURL: 'http://localhost:8080/traceit/user',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    }, auth: {
        username: 'Thinksoft',
        password: '@Th1nks0ft'
    }
}

function login(user) {
    let params = new URLSearchParams()
    params.append('email', user.email)
    params.append('password', user.password)

    return axios.post('/login', params, axiosLoginConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}

function loginByToken(token) {
    let params = new URLSearchParams()
    params.append('token', token)

    return axios.post('/login/bytoken', params, axiosLoginConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user')
}

function getAll() {

    return axios.get('', axiosConfig)
        .then(function (response) {
            console.log(response)
        })
        .catch(function (error) {
            console.log(error)
        })
}

function getById(id) {
    return axios.get('', axiosConfig)
        .then(function (response) {
            console.log(response)
        })
        .catch(function (error) {
            console.log(error)
        })
}

function register(user) {
    return axios.post('', user, axiosConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}

function forgotPassword(user) {
    let params = new URLSearchParams()
    params.append('email', user.email)
    params.append('redirectTo', user.redirectTo)

    return axios.post('/forget', params, axiosLoginConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}

function newPassword(user) {

    return axios.post('/newpass', user, axiosConfig)
        .then(function (response) {
            return response.data
        })
        .catch(function (error) {
            return error.response
        })
}

function update(user) {
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
}
