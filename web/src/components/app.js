import React , { Component } from 'react'
import {TraceitStyles} from '../resources/themes/traceit-styles'
import Login from './public/login'
import ForgotPassword from './public/forgot.password'
import { history } from '../resources/history'
import {Route, Router} from 'react-router-dom'
import {PrivateRoute} from './private.route'
import Register from './public/register'
import Alert from 'react-s-alert'
import 'react-s-alert/dist/s-alert-default.css'
import {translate} from "react-i18next"
import './app.css'
import NewPassword from './public/new.password'
import Home from './restricted/home'
import * as cookie from "react-cookies"
import {store} from '../resources/store'
import {userActions} from "../user/user.actions"


class App extends Component {

    componentWillMount() {
        localStorage.removeItem('user');
        let token = cookie.load('traceit')
        console.log('token: '+token)
        if (token != null) {
            store.dispatch(userActions.loginByToken(token))
        }
    }

    render() {
        const { classes, t } = this.props

        return (
            <div className={classes.root}>
                <Alert stack={true} timeout={15000} />

                <Router history={history}>
                    <div>
                        <PrivateRoute exact path="/" component={Home} />
                        <Route path="/login" render={ (props) => <Login {...props} classes={classes} t={t}/>} />
                        <Route path="/register" render={ (props) => <Register {...props} classes={classes} t={t}/>} />
                        <Route path="/forgot-password" render={ (props) => <ForgotPassword {...props} classes={classes} t={t}/>} />
                        <Route path="/new-password" render={ (props) => <NewPassword {...props} classes={classes} t={t}/>} />
                    </div>
                </Router>
            </div>
        )
    }
}

export default translate("translations")(TraceitStyles(App))
