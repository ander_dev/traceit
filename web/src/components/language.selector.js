import React, { Component } from 'react'
import brFlag from '../resources/images/brazil_25.png'
import ukFlag from '../resources/images/united_kingdom_25.png'
import { translate } from 'react-i18next'

class LanguageSelector extends  Component {

    render() {

        const { classes , i18n } = this.props;

        const changeLanguage = lng => {
            i18n.changeLanguage(lng)
        }

        return (
            <div>
                <div className={classes.flag}>
                    <img src={brFlag} onClick={() => changeLanguage("pt")} alt="PT"/>
                </div>
                <div className={classes.flag}>
                    <img src={ukFlag} onClick={() => changeLanguage("en")} alt="EN"/>
                </div>
            </div>
        )
    }
}

export default translate("translations")(LanguageSelector);