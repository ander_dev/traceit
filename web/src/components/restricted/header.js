import React, {Component} from 'react'
import PropTypes from 'prop-types'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import {AccountCircle, Cancel} from '@material-ui/icons'
import MenuItem from '@material-ui/core/MenuItem'
import Menu from '@material-ui/core/Menu'
import traceitLogo from "../../resources/images/logo.png"
import {store} from "../../resources/store"
import {userActions} from "../../user/user.actions"
import {Link} from "react-router-dom"

class Header extends Component {
    state = {
        anchorEl: null,
    }

    handleMenu = event => {
        this.setState({anchorEl: event.currentTarget})
    }

    handleClose = () => {
        this.setState({anchorEl: null})
    }

    handleLogout() {
        store.dispatch(userActions.logout())
    }

    render() {
        const {classes, t, handleToggleMenu} = this.props
        const {anchorEl} = this.state
        const open = Boolean(anchorEl)

        return (
            <header className="header">
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu" onClick={handleToggleMenu}>
                            <MenuIcon/>
                        </IconButton>

                        <Typography variant="title" color="inherit" className={classes.flex}>
                            <div>
                                <img src={traceitLogo} alt={t("traceitTitle")} className={classes.logoSize}/>
                                <div className={classes.titleStyle}>
                                    <span className="softblue">{t("trace")}</span>
                                    <span className="hardblue">{t("it")}</span>
                                </div>
                            </div>
                        </Typography>

                        <div>
                            <IconButton
                                aria-owns={open ? 'menu-appbar' : null}
                                aria-haspopup="true"
                                onClick={this.handleMenu}
                                color="inherit"
                            >
                                <AccountCircle/>
                            </IconButton>
                            <IconButton onClick={this.handleLogout} component={Link} to="/">
                                <Cancel/>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={open}
                                onClose={this.handleClose}
                            >
                                <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                                <MenuItem onClick={this.handleClose}>My account</MenuItem>
                            </Menu>
                        </div>
                    </Toolbar>
                </AppBar>
            </header>
        )
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default Header