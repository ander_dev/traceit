import React , { Component } from 'react'
import { connect } from 'react-redux'
import {translate} from 'react-i18next'
import {TraceitStyles} from "../../resources/themes/traceit-styles"
import Header from "./header"
import Menu from "./menu"
import Dashboard from "./dashboard"
import Config from "./config"
import ContactForm from "./contact-form"

class Home extends Component {

    componentDidMount() {
        console.log(window.innerWidth)
        let screenWidth = window.innerWidth

        if (screenWidth >= 768) {
            let isBigScreen = true
            this.setState({ isBigScreen })
            this.handleToggleMenu()
        }
    }

    constructor(props) {
        super(props)

        this.state = {
            currentContent: '',
            isMenuHidden: true,
            isBigScreen: false
        }

        this.handleChangeContent = this.handleChangeContent.bind(this)
        this.handleToggleMenu = this.handleToggleMenu.bind(this)
    }

    handleChangeContent(value) {
        let currentContent = value
        this.setState({ currentContent })
        let isBigScreen = this.state.isBigScreen
        if (!isBigScreen) {
            this.handleToggleMenu()
        }
    }

    handleToggleMenu = () => {
        let isMenuHidden = !this.state.isMenuHidden
        this.setState({ isMenuHidden })
    }

    render() {
        const { classes, t } = this.props
        const { currentContent } = this.state
        let content
        let contentStyle = this.state.isMenuHidden ? 'contentMenuOff' : 'contentMenuOn'
        let style = this.state.isBigScreen ? 'menu height100' : 'menu'


        switch(currentContent) {
            case 'config':
                content = <Config classes={classes} t={t}/>
                break
            case 'contact':
                content = <ContactForm  classes={classes} t={t}/>
                break
            default:
                content = <Dashboard classes={classes} t={t}/>
        }

        return (
            <div className={classes.root}>

                <Header classes={classes} t={t} handleToggleMenu={this.handleToggleMenu} />

                {!this.state.isMenuHidden && <Menu classes={classes} t={t} handleChangeContent={this.handleChangeContent} style={style}/>}

                <div className={contentStyle}>
                    {content}
                </div>

            </div>
        )
    }
}

function mapStateToProps(state) {
    const { authentication } = state
    const { user } = authentication
    return {
        user
    }
}

export default connect(mapStateToProps)(translate("translations")(TraceitStyles(Home)))