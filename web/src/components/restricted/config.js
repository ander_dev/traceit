import React, { Component } from 'react'
import connect from "react-redux/es/connect/connect"
import createMuiTheme from "@material-ui/core/styles/createMuiTheme"
import purpleThem from "../../resources/themes/purpleTheme"
import fontThem from "../../resources/themes/fontTheme"
import ContactForm from "./contact-form"
import {AppBar, Tab, Tabs} from "@material-ui/core"
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider"
import Dashboard from "./dashboard"
import {Link} from "react-router-dom"

const defaultTheme = createMuiTheme()

const purpleTheme = createMuiTheme(purpleThem)

const fontTheme = createMuiTheme(fontThem)

const themes = [defaultTheme, purpleTheme, fontTheme]

function TabContainer(props) {
    console.log(props.children)

    switch (props.children) {
        case 'Default' :
            return <ContactForm classes={props.classes}/>
        case 'Purple':
            return <ContactForm classes={props.classes}/>
        default:
            return <Dashboard classes={props.classes}/>
    }
}

class Config extends  Component {

    constructor(props) {
        super(props)

        this.state = {
            value: 0,
            theme: defaultTheme
        }
    }

    handleChange = (event, value) => {
        this.setState({
            value, theme:themes[value]
        })
    }

    render() {

        const { classes, t } = this.props
        const { value, theme } = this.state

        return (
                <MuiThemeProvider theme={theme} className='content'>
                    <AppBar position="static">
                        <Tabs value={value} onChange={this.handleChange}>
                            <Tab label={t("defaultTheme")} href="#default-theme"/>
                            <Tab label={t("purpleTheme")} href="#purple-theme"/>
                            <Tab label={t("fontTheme")} href="#font-theme" />
                        </Tabs>
                    </AppBar>

                    {value === 0 && <TabContainer classes={classes} children={'Default'}/>}
                    {value === 1 && <TabContainer classes={classes} children={'Purple'}/>}
                    {value === 2 && <TabContainer classes={classes} children={'Font'}/>}
                </MuiThemeProvider>
        )
    }
}

function mapStateToProps(state) {
    const { authentication } = state
    const { user } = authentication
    return {
        user
    }
}
export default connect(mapStateToProps)(Config)
