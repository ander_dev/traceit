import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography/Typography'
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator'
import Save from '@material-ui/icons/Save'
import Cancel from '@material-ui/core/internal/svg-icons/Cancel'
import {Link} from "react-router-dom"
import {userActions} from "../../user/user.actions"
import {connect} from "react-redux"
import PublicHeader from './public.header'

class NewPassword extends  Component {

    constructor(props) {
        super(props)

        this.state = {
            user: {
                email: '',
                oldPassword: '',
                password: '',
                confirmPassword: '',
                token: ''
            }
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleChange(event) {
        const { user } = this.state
        user[event.target.name] = event.target.value
        this.setState({ user })
    }


    handleSubmit(event) {
        event.preventDefault()

        const { user } = this.state
        const { dispatch } = this.props

        user.token = this.props.location.pathname.split("/")[2]

        console.log(user)

        dispatch(userActions.newPassword(user))
    }

    componentWillMount() {
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            if (value !== this.state.user.password) {
                return false;
            }
            return true;
        })
    }

    render() {
        const { classes, t } = this.props
        const { user } = this.state

        let requiredField = t('requiredField')
        let invalidEmail = t('invalidEmail')
        let passwordMismatch = t('passwordMismatch')

        return (
            <div className="bgImage">
                <PublicHeader classes={classes} t={t}/>

                <div className={classes.homeContainer}>

                    <Paper className={classes.paper}>
                        <Typography variant="title" color="inherit" className={classes.registerLoginTitle}>
                            {t("newPassword")}
                        </Typography>

                        <ValidatorForm ref="form" onSubmit={this.handleSubmit} >
                            <TextValidator
                                id="user.email"
                                name="email"
                                label={t("email")}
                                placeholder={t("email")}
                                fullWidth={true}
                                onChange={this.handleChange}
                                value={user.email}
                                validators={['required', 'isEmail']}
                                errorMessages={[requiredField, invalidEmail]}
                            />
                            <TextValidator
                                id="user.oldPassword"
                                name="oldPassword"
                                label={t("currentPassword")}
                                placeholder={t("currentPassword")}
                                fullWidth={true}
                                onChange={this.handleChange}
                                value={user.oldPassword}
                                validators={['required']}
                                errorMessages={[requiredField]}
                            />
                            <TextValidator
                                id="user.password"
                                name="password"
                                label={t("password")}
                                placeholder={t("password")}
                                fullWidth={true}
                                type="password"
                                onChange={this.handleChange}
                                value={user.password}
                                validators={['required']}
                                errorMessages={[requiredField]}
                            />
                            <TextValidator
                                id="user.confirm.password"
                                name="confirmPassword"
                                label={t("confirmPassword")}
                                placeholder={t("confirmPassword")}
                                fullWidth={true}
                                type="password"
                                onChange={this.handleChange}
                                value={user.confirmPassword}
                                validators={['isPasswordMatch', 'required']}
                                errorMessages={[passwordMismatch, requiredField]}
                            />

                            <Button variant="contained"  color="primary" className={classes.saveBtn} type="submit">
                                <Save className={classes.leftIcon} />
                                {t("save")}
                            </Button>

                            <Button variant="contained" color="primary" className={classes.backBtn} component={Link} to="/">
                                <Cancel className={classes.leftIcon} />
                                {t("back")}
                            </Button>
                        </ValidatorForm>
                    </Paper>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    }
}

export default connect(mapStateToProps)(NewPassword)