import React, {Component} from 'react'
import LanguageSelector from '../language.selector'
import traceitLogo from '../../resources/images/logo.png'

class PublicHeader extends Component {

    render() {
        const { classes, t } = this.props

        return (
            <div>
                <LanguageSelector classes={classes}/>

                <div className={classes.titleBox}>
                    <img src={traceitLogo} alt={t("traceitTitle")} className={classes.logoSize}/>
                    <div className={classes.titleStyle}>
                        <span className="fontSize40 softblue">{t("trace")}</span>
                        <span className="fontSize40 hardblue">{t("it")}</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default PublicHeader