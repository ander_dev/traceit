import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography/Typography'
import {Link} from "react-router-dom"
import {TextValidator, ValidatorForm} from 'react-material-ui-form-validator'
import Save from '@material-ui/icons/Save'
import Cancel from '@material-ui/core/internal/svg-icons/Cancel'
import {userActions} from "../../user/user.actions"
import {store} from '../../resources/store'
import PublicHeader from './public.header'

class ForgotPassword extends  Component {

    constructor(props) {
        super(props)

        this.state = {
            user: {
                email: '',
                redirectTo: ''
            }
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleChange(event) {
        const { user } = this.state
        user[event.target.name] = event.target.value
        this.setState({ user })
    }


    handleSubmit(event) {
        event.preventDefault()

        const { user } = this.state;

        user.redirectTo = 'http://localhost:3000/new-password'

        store.dispatch(userActions.forgotPassword(user))
    }

    render() {
        const { classes, t } = this.props
        const { user } = this.state

        let requiredField = t('requiredField')
        let invalidEmail = t('invalidEmail')

        return (
            <div className="bgImage">

                <PublicHeader classes={classes} t={t}/>

                <div className={classes.homeContainer}>
                    <Paper className={classes.paper}>
                        <Typography variant="title" color="inherit" className={classes.registerLoginTitle}>
                            {t("forgotEmail")}
                        </Typography>

                        <ValidatorForm ref="form" onSubmit={this.handleSubmit} >
                            <TextValidator
                                id="user.email"
                                name="email"
                                label={t("email")}
                                placeholder={t("email")}
                                fullWidth={true}
                                onChange={this.handleChange}
                                value={user.email}
                                validators={['required', 'isEmail']}
                                errorMessages={[requiredField, invalidEmail]}
                            />
                            <Button variant="contained"  color="primary" className={classes.saveBtn} type="submit">
                                <Save className={classes.leftIcon} />
                                {t("save")}
                            </Button>

                            <Button variant="contained" color="primary" className={classes.backBtn} component={Link} to="/">
                                <Cancel className={classes.leftIcon} />
                                {t("back")}
                            </Button>
                        </ValidatorForm>
                    </Paper>

                </div>
            </div>
        )
    }
}

export default ForgotPassword