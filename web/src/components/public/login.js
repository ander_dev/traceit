import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Checkbox from "@material-ui/core/Checkbox/Checkbox"
import PersonAdd from "@material-ui/icons/PersonAdd"
import Help from "@material-ui/icons/Help"
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel"
import {Link} from "react-router-dom"
import PublicHeader from './public.header'
import {TextValidator, ValidatorForm} from "react-material-ui-form-validator"
import {userActions} from "../../user/user.actions"
import connect from "react-redux/es/connect/connect"

class Login extends  Component {

    constructor(props) {
        super(props)

        this.state = {
            user: {
                email: '',
                password: '',
                rememberMe: null,
            },
            rememberMe: true,
            showPassword: false,
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(event) {
        const { user } = this.state
        user[event.target.name] = event.target.value
        this.setState({ user })
    }


    handleSubmit(event) {
        event.preventDefault()

        const { user } = this.state
        const { dispatch } = this.props

        user.rememberMe = this.state.rememberMe

        dispatch(userActions.login(user))
    }


    handleRememberMe = name => event => {
        this.setState({ [name]: event.target.checked })
    }

    render() {
        const { classes, t } = this.props

        const { user, rememberMe, showPassword } = this.state

        let requiredField = t('requiredField')
        let invalidEmail = t('invalidEmail')

        return (
            <div className="bgImage">
                <PublicHeader classes={classes} t={t}/>

                <div className={classes.homeContainer}>
                    <Paper className={classes.paper}>
                        <ValidatorForm ref="form" onSubmit={this.handleSubmit} >
                            <TextValidator
                                id="user.email"
                                name="email"
                                label={t("email")}
                                placeholder={t("email")}
                                fullWidth={true}
                                onChange={this.handleChange}
                                value={user.email}
                                validators={['required', 'isEmail']}
                                errorMessages={[requiredField, invalidEmail]}
                            />
                            <TextValidator
                                id="user.password"
                                name="password"
                                label={t("password")}
                                placeholder={t("password")}
                                fullWidth={true}
                                type={showPassword ? 'text' : 'password'}
                                onChange={this.handleChange}
                                value={user.password}
                                validators={['required']}
                                errorMessages={[requiredField]}
                            />

                            <div>
                                <FormControlLabel className={classes.rememberMeBtn}
                                      control={
                                          <Checkbox
                                              id="loginRememberMe"
                                              checked={rememberMe}
                                              onChange={this.handleRememberMe('rememberMe')}
                                              value="rememberMe"
                                              color="secondary"
                                          />
                                      }
                                      label={t("rememberMe")}
                                />

                                <Button variant="contained" color="primary" className={classes.saveBtn} type="submit">
                                    {t("login")}
                                </Button>
                            </div>

                        </ValidatorForm>
                    </Paper>

                    <div className={classes.buttonsDiv}>
                        <Link to="/register">
                            <Button className={classes.flatButton}>
                                <PersonAdd className={classes.leftIcon} />
                                {t("register")}
                            </Button>
                        </Link>

                        <Link to="/forgot-password">
                            <Button className={classes.flatButton} >
                                <Help className={classes.leftIcon} />
                                {t("forgotPassword")}
                            </Button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    }
}

export default connect(mapStateToProps)(Login)