import {green, blue} from "@material-ui/core/colors";


export default {
    palette: {
        primary: blue,
        secondary: green
    }
};