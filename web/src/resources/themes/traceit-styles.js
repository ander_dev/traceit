import withStyles from "@material-ui/core/styles/withStyles"
import {grey500, white} from '@material-ui/core/colors/index'

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        height: '100vh',
        border: '0px dotted black'
    },
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    homeContainer: {
        minWidth: 300,
        maxWidth: 500,
        height: 'auto',
        position: 'absolute',
        top: '35%',
        left: 0,
        right: 0,
        margin: 'auto',
    },
    flag : {
        width: '30px',
        height: '30px',
        cursor: 'pointer',
        border: '0px solid black',
        padding: '0px',
        float: 'right',
        margin: '10px 5px 0px 0px',
        marginTop: '20px',
    },
    button: {
        margin: theme.spacing.unit,
    },
    leftIcon: {
        marginRight: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
    iconSmall: {
        fontSize: 20,
    },
    titleBox: {
        minWidth: 140,
        maxWidth: 220,
        height: 'auto',
        position: 'absolute',
        top: '25%',
        left: 0,
        right: 0,
        margin: 'auto',
        backgroundColor: 'white',
        border: '0px dotted black',
        borderRadius: '10px',
    },
    logoSize: {
        position: 'absolute',
        top: '10px',
        width: '40px',
        border: '0px dotted black',
        padding: '5px 5px 5px 5px',
    },
    titleStyle: {
        border: '0px dotted black',
        marginLeft: '40px',
        fontStyle: 'italic'
    },
    paper: {
        padding: 20,
        overflow: 'auto',
        minHeight: '210px'
    },
    buttonsDiv: {
        textAlign: 'center',
        padding: 10,
    },
    flatButton: {
        color: grey500
    },
    saveBtn: {
        float: 'right',
        marginTop: '25px',
        marginRight: '30px'
    },
    backBtn: {
        float: 'left',
        marginTop: '25px',
        marginLeft: '30px'
    },
    rememberMeBtn: {
        float: 'left',
        marginTop: '22px',
        marginLeft: '15px'
    },
    btn: {
        background: '#4f81e9',
        color: white,
        padding: 7,
        borderRadius: 2,
        margin: 2,
        fontSize: 13
    },
    btnSpan: {
        marginLeft: 5
    },
    registerLoginTitle: {
        marginLeft: 5,
        marginBottom: 25
    },
    menuItem: {
        '&:focus': {
            backgroundColor: theme.palette.primary.main,
            '& $primary, & $icon': {
                color: theme.palette.common.white,
            },
        },
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },









    mainContainer: {
        textAlign: 'center',
        paddingTop: 100,
        width: '100%',
        border: '1px solid blue',
    },
    centerContainer: {
        margin: 'auto',
        width: '80%',
        border: '3px solid green',
        padding: '10px'
    },
    search_bar: {
        margin: '20px',
        textAlign: 'center'
    },
    search_bar_input: {
        width: '75%'
    },
    list_group: {
        width: '34%',
        float: 'right',
        position: 'relative',
        minHeight: '1px',
        paddingRight: '15px',
        paddingLeft: '15px'
    },
    list_group_item: {
        cursor: 'pointer',
        padding: '10px 10px 10px 10px'
    },
    list_group_item_hover: {
        backgroundColor: '#eee'
    },
    toggle: {
        marginBottom: 50,
    },
    checkbox: {
        marginBottom: 16,
    }
})

export const TraceitStyles = withStyles(styles)