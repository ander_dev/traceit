import blue from "@material-ui/core/colors/blue";
import blueGrey from "@material-ui/core/colors/blueGrey";

export default {
    palette: {
        secondary: blue,
        primary: blueGrey,
    },
    typography: {
        fontFamily: ['Courier', 'Helvetica'],
    }
};