import {userConstants} from '../resources/constants/user.constants'
import {userService} from '../services/user.service'
import {history} from '../resources/history'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'

export const userActions = {
    login,
    loginByToken,
    logout,
    register,
    forgotPassword,
    newPassword,
    getAll,
    delete: _delete
}

function login(user) {
    return dispatch => {
        dispatch(request(user))
        userService.login(user)
            .then(
                data => {
                    if(data.statusCode !== 200){
                        Alert.error(data.message, {
                            position: 'top-right'
                        })
                    }else {
                        dispatch(success(data.obj))
                        localStorage.setItem('user', JSON.stringify(data.obj))
                        if (user.rememberMe){
                            cookie.save('traceit', data.obj.token)
                        }
                        history.push('/')
                        Alert.success(data.message, {
                            position: 'top-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    // dispatch(alertActions.error(error))
                }
            )
    }

    function request(user) {
        return {type: userConstants.LOGIN_REQUEST, user}
    }

    function success(user) {
        return {type: userConstants.LOGIN_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.LOGIN_FAILURE, error}
    }
}

function loginByToken(token) {
    return dispatch => {
        dispatch(request(token))

        userService.loginByToken(token)
            .then(
                data => {
                    console.log(data)
                    if(data.statusCode !== 200){
                        Alert.error(data.message, {
                            position: 'top-right'
                        })
                    }else {
                        dispatch(success(data.obj))
                        localStorage.setItem('user', JSON.stringify(data.obj))
                        history.push('/')
                        Alert.success(data.message, {
                            position: 'top-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    // dispatch(alertActions.error(error))
                }
            )
    }

    function request(token) {
        return {type: userConstants.LOGIN_REQUEST, token}
    }

    function success(user) {
        return {type: userConstants.LOGIN_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.LOGIN_FAILURE, error}
    }
}

function logout() {
    userService.logout()
    cookie.remove('traceit')
    return {type: userConstants.LOGOUT}
}

function register(user) {
    return dispatch => {
        dispatch(request(user))

        userService.register(user)
            .then(
                user => {
                    if(user.statusCode !== 200){
                        Alert.error(user.message, {
                            position: 'top-right'
                        })
                    }else {
                        dispatch(success())
                        history.push('/')
                        Alert.success(user.message, {
                            position: 'top-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    Alert.error(error, {
                        position: 'top-right'
                    })
                }
            )
    }

    function request(user) {
        return {type: userConstants.REGISTER_REQUEST, user}
    }

    function success(user) {
        return {type: userConstants.REGISTER_SUCCESS, user}
    }

    function failure(error) {
        return {type: userConstants.REGISTER_FAILURE, error}
    }
}

function forgotPassword(user) {
    return dispatch => {
        dispatch(request(user))

        userService.forgotPassword(user)
            .then(
                user => {
                    if(user.statusCode !== 200){
                        Alert.error(user.message, {
                            position: 'top-right'
                        })
                    }else {
                        dispatch(success())
                        history.push('/')
                        Alert.success(user.message, {
                            position: 'top-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    // dispatch(alertActions.error(error))
                }
            )
    }

    function request(user) {
        return {type: userConstants.FORGOT_PASSWORD_REQUEST, user}
    }

    function success(user) {
        return {type: userConstants.FORGOT_PASSWORD_SUCCESS, user}
    }

    function failure(user) {
        return {type: userConstants.FORGOT_PASSWORD_FAILURE, user}
    }
}

function newPassword(user) {
    return dispatch => {
        dispatch(request(user))

        userService.newPassword(user)
            .then(
                data => {
                    if(data.statusCode !== 200){
                        Alert.error(data.message, {
                            position: 'top-right'
                        })
                    }else {
                        dispatch(success(data.obj))
                        localStorage.setItem('user', JSON.stringify(data.obj))
                        history.push('/')
                        Alert.success(data.message, {
                            position: 'top-right'
                        })
                    }
                },
                error => {
                    dispatch(failure(error))
                    // dispatch(alertActions.error(error))
                }
            )
    }

    function request(user) {
        return {type: userConstants.LOGIN_REQUEST, user}
    }

    function success(user) {
        return {type: userConstants.LOGIN_SUCCESS, user}
    }

    function failure(user) {
        return {type: userConstants.LOGIN_FAILURE, user}
    }
}

function getAll() {
    return dispatch => {
        dispatch(request())

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error))
            )
    }

    function request() {
        return {type: userConstants.GETALL_REQUEST}
    }

    function success(users) {
        return {type: userConstants.GETALL_SUCCESS, users}
    }

    function failure(error) {
        return {type: userConstants.GETALL_FAILURE, error}
    }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id))

        userService.delete(id)
            .then(
                user => {
                    dispatch(success(id))
                },
                error => {
                    dispatch(failure(id, error))
                }
            )
    }

    function request(id) {
        return {type: userConstants.DELETE_REQUEST, id}
    }

    function success(id) {
        return {type: userConstants.DELETE_SUCCESS, id}
    }

    function failure(id, error) {
        return {type: userConstants.DELETE_FAILURE, id, error}
    }
}