package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.TypeAccount;

public interface TypeAccountDAO extends GenericDAO<TypeAccount> {

}
