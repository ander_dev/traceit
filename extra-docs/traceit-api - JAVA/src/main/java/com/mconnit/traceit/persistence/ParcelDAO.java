package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Parcel;

public interface ParcelDAO extends GenericDAO<Parcel> {

}
