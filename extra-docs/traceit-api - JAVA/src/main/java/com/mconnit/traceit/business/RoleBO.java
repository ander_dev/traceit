package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Role;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("roleBO")
public interface RoleBO extends GenericBO<Role> {

	List<Role> list() throws Exception;

	MessageReturn save(final Role role) throws Exception;

	MessageReturn delete(Long id);

	Role getById(Long id);
}

