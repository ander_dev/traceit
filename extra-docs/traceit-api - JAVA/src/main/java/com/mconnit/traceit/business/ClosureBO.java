package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Closure;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service("closureBO")
public interface ClosureBO extends GenericBO<Closure> {

	List<Closure> list(User user) throws Exception;

	List<Closure> listByParameter(Closure closure) throws Exception;

	MessageReturn save(final Closure closure) throws Exception;

	MessageReturn delete(Long id);

	Closure getById(Long id);

	void closeValuesByUserAndDate(User user, Calendar date) throws Exception;
}
