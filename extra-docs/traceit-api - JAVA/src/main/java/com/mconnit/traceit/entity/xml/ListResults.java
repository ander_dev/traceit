package com.mconnit.traceit.entity.xml;

import com.mconnit.traceit.entity.Account;
import com.mconnit.traceit.entity.CreditCard;
import com.mconnit.traceit.entity.Description;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "ListResults")
public class ListResults {

	private List<Description> creditDescriptionList;

	private List<Description> debitDescriptionList;

	private List<Description> creditDebitDescriptionList;

	private List<Description> groupList;

	private List<Description> superGroupList;

	private List<Account> accountList;

	private List<CreditCard> creditCardList;

	public List<Description> getCreditDescriptionList() {
		return creditDescriptionList;
	}

	public void setCreditDescriptionList(List<Description> creditDescriptionList) {
		this.creditDescriptionList = creditDescriptionList;
	}

	public List<Description> getDebitDescriptionList() {
		return debitDescriptionList;
	}

	public void setDebitDescriptionList(List<Description> debitDescriptionList) {
		this.debitDescriptionList = debitDescriptionList;
	}

	public List<Description> getCreditDebitDescriptionList() {
		return creditDebitDescriptionList;
	}

	public void setCreditDebitDescriptionList(List<Description> creditDebitDescriptionList) {
		this.creditDebitDescriptionList = creditDebitDescriptionList;
	}

	public List<Description> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<Description> groupList) {
		this.groupList = groupList;
	}

	public List<Description> getSuperGroupList() {
		return superGroupList;
	}

	public void setSuperGroupList(List<Description> superGroupList) {
		this.superGroupList = superGroupList;
	}

	public List<Account> getAccountList() {
		return accountList;
	}

	public void setAccountList(List<Account> accountList) {
		this.accountList = accountList;
	}

	public List<CreditCard> getCreditCardList() {
		return creditCardList;
	}

	public void setCreditCardList(List<CreditCard> creditCardList) {
		this.creditCardList = creditCardList;
	}
}
