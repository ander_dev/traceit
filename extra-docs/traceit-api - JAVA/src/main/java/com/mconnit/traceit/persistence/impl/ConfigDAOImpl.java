package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Config;
import com.mconnit.traceit.persistence.ConfigDAO;
import org.springframework.stereotype.Repository;

@Repository("configDAO")
public class ConfigDAOImpl extends GenericDAOImpl<Config> implements ConfigDAO {


}
