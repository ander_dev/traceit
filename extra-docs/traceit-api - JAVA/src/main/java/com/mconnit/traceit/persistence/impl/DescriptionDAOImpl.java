package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.persistence.DescriptionDAO;
import org.springframework.stereotype.Repository;

@Repository("descriptionDAO")
public class DescriptionDAOImpl extends GenericDAOImpl<Description> implements DescriptionDAO {

}
