package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.CurrencyBO;
import com.mconnit.traceit.entity.Currency;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.utils.MessageFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CurrencyBOImpl extends GenericBOImpl<Currency> implements CurrencyBO {

	@Override
	@Transactional
	public MessageReturn save(Currency currency) {
		MessageReturn libReturn = new MessageReturn();
		try {
			saveGeneric(currency);
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setCurrency(currency);
			libReturn.setMessage(e.getMessage());
		}
		if (libReturn.getMessage() == null && currency.getId() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_currency_saved", currency.getLocale()));
			libReturn.setCurrency(currency);
		} else if (libReturn.getMessage() == null && currency.getId() != null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_currency_updated", currency.getLocale()));
			libReturn.setCurrency(currency);
		}
		return libReturn;
	}

	public List<Currency> listByLocale(final String locale) throws Exception {
		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.locale = ", "'" + locale + "'");
		return list(Currency.class, queryParams, "acronym asc");
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		Currency currency = null;
		try {
			currency = findById(Currency.class, id);
			if (currency == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_currency_not_found", "en"));
			} else {
				String locale = currency.getLocale();
				remove(currency);
				libReturn.setMessage(MessageFactory.getMessage("lb_currency_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public Currency getById(Long id) {
		try {
			return findById(Currency.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Currency> list() throws Exception {
		return list(Currency.class, null, "acronym asc");
	}

}
