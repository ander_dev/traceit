package com.mconnit.traceit.scheduler;

import com.mconnit.traceit.business.ClosureBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class CloseAccountJob{

    @Autowired
    private UserBO userBO;

    @Autowired
    private ClosureBO closureBO;

    @Scheduled(cron = "0 0/1 * 1/1 * ?")
    public void closeAccounts(){
        try {
            List<User> userList = userBO.list();
            for (User user : userList) {
                if(user.getGmt() != null){
                    Calendar currentDate = Calendar.getInstance();
                    currentDate.setTimeZone(TimeZone.getTimeZone(user.getGmt()));
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    if (currentDate.get(Calendar.DAY_OF_MONTH) == currentDate.getActualMaximum(Calendar.DAY_OF_MONTH) && sdf.format(currentDate.getTime()).equals("23:59")){
                        closureBO.closeValuesByUserAndDate(user, currentDate);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}