package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.CurrencyBO;
import com.mconnit.traceit.entity.Currency;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/currency")
@CrossOrigin(origins = "http://localhost:3000")
public class CurrencyService {

	@Autowired
	private CurrencyBO currencyBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Currency> list() {

		List<Currency> list = new ArrayList<>();
		try {
			list = currencyBO.list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{locale}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Currency> listByLocale(@PathVariable String locale) {

		List<Currency> list = new ArrayList<>();
		try {
			list = currencyBO.listByLocale(locale);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn save(@RequestBody Currency currency) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = currencyBO.save(currency);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@RequestMapping(method = RequestMethod.DELETE, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn delete(@RequestBody Currency currency) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = currencyBO.delete(currency.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
