package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.PlanningItem;

public interface PlanningItemDAO extends GenericDAO<PlanningItem> {

}
