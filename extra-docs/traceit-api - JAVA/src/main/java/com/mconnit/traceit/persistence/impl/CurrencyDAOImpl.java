package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Currency;
import com.mconnit.traceit.persistence.CurrencyDAO;
import org.springframework.stereotype.Repository;

@Repository("currencyDAO")
public class CurrencyDAOImpl extends GenericDAOImpl<Currency> implements CurrencyDAO {


}
