package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Config;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("configBO")
public interface ConfigBO extends GenericBO<Config> {

	List<Config> list() throws Exception;

	MessageReturn save(final Config config) throws Exception;

	MessageReturn delete(Long id);

	Config getById(Long id);
}

