package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.GenericBO;
import com.mconnit.traceit.persistence.impl.GenericDAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public class GenericBOImpl<T> implements GenericBO<T> {

	@Autowired
	private GenericDAOImpl<T> genericDAO;

	@SuppressWarnings("hiding")
	public <T> T findById(final Class<T> type, Long id) throws Exception {
		return genericDAO.findById(type, id);
	}

	@SuppressWarnings("hiding")
	public <T> T findByParameter(final Class<T> type, Map<String, String> queryParams) throws Exception {
		return genericDAO.findByParameter(type, queryParams);
	}

	public List<T> listPaginated(final Class<T> type, int startRow, int pageSize, Map<String, String> queryParams, String orderByField) throws Exception {
		return genericDAO.listPaginated(type, startRow, pageSize, queryParams, orderByField);
	}

	public List<T> list(final Class<T> type, Map<String, String> queryParams, String orderByField) throws Exception {
		return genericDAO.list(type, queryParams, orderByField);
	}

	public Integer count(Class<T> type, Map<String, String> queryParams) throws Exception {
		return genericDAO.count(type, queryParams);
	}

	@Transactional
	public T saveGeneric(T obj) throws Exception {
		return genericDAO.save(obj);
	}

	@Transactional
	public Boolean remove(T obj) throws Exception {
		return genericDAO.remove(obj);
	}
}
