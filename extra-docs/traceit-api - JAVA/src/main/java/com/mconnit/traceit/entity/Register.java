package com.mconnit.traceit.entity;

import com.mconnit.traceit.entity.pagination.PaginationValues;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;

@Entity
@Table(name = "register")
@XmlRootElement
public class Register extends Audit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	private Long id;

	private Calendar date;

	private Calendar currentDate;

	@Transient
	private Calendar strDate;

	@Column(length = 13, precision = 13, scale = 2)
	private BigDecimal amount;

	private Integer numberParcel;

	@ManyToOne(cascade = {CascadeType.PERSIST}, targetEntity = Description.class)
	@JoinColumn(name = "description_id", foreignKey = @ForeignKey(name = "FK_REGISTER_DESCRIPTION"))
	private Description description;

	@ManyToOne(cascade = {CascadeType.PERSIST}, targetEntity = Description.class)
	@JoinColumn(name = "group_id", foreignKey = @ForeignKey(name = "FK_REGISTER_GROUP"))
	private Description group;

	@ManyToOne(cascade = {CascadeType.PERSIST}, targetEntity = Description.class)
	@JoinColumn(name = "supergroup_id", foreignKey = @ForeignKey(name = "FK_REGISTER_SGROUP"))
	private Description superGroup;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = Account.class)
	@JoinColumn(name = "account_id", foreignKey = @ForeignKey(name = "FK_REGISTER_ACCOUNT"))
	private Account account;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = TypeAccount.class)
	@JoinColumn(name = "typeaccount_id", foreignKey = @ForeignKey(name = "FK_REGISTER_TYPEACCOUNT"))
	private TypeAccount typeAccount;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = Parcel.class)
	@JoinColumn(name = "parcel_id", foreignKey = @ForeignKey(name = "FK_REGISTER_PARCEL"))
	private Parcel parcel;

	@Transient
	private Boolean search;

	@Column(name = "closed", nullable = false, columnDefinition = "boolean default false")
	private Boolean closed;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = CreditCard.class)
	@JoinColumn(name = "creditcard_id", foreignKey = @ForeignKey(name = "FK_REGISTER_CREDIT_CARD"))
	private CreditCard creditCard;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = User.class)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_REGISTER_USER"))
	private User user;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = PlanningItem.class)
	@JoinColumn(name = "planning_item_id", foreignKey = @ForeignKey(name = "FK_REGISTER_PITEM"))
	private PlanningItem planningItem;

	@Transient
	private Boolean multipleParcel;

	@Transient
	private String stringDate;

	@Transient
	private PaginationValues paginationValues;

	public PlanningItem getPlanningItem() {
		return planningItem;
	}

	public void setPlanningItem(PlanningItem planningItem) {
		this.planningItem = planningItem;
	}

	public PaginationValues getPaginationValues() {
		return paginationValues;
	}

	public void setPaginationValues(PaginationValues paginationValues) {
		this.paginationValues = paginationValues;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public Calendar getStrDate() {
		return strDate;
	}

	public void setStrDate(Calendar strDate) {
		this.strDate = strDate;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public Description getGroup() {
		return group;
	}

	public void setGroup(Description group) {
		this.group = group;
	}

	public Description getSuperGroup() {
		return superGroup;
	}

	public void setSuperGroup(Description superGroup) {
		this.superGroup = superGroup;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Parcel getParcel() {
		return parcel;
	}

	public void setParcel(Parcel parcel) {
		this.parcel = parcel;
	}

	public Boolean getClosed() {
		return closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getNumberParcel() {
		return numberParcel;
	}

	public void setNumberParcel(Integer numberParcel) {
		this.numberParcel = numberParcel;
	}

	public Calendar getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(Calendar currentDate) {
		this.currentDate = currentDate;
	}

	public TypeAccount getTypeAccount() {
		return typeAccount;
	}

	public void setTypeAccount(TypeAccount typeAccount) {
		this.typeAccount = typeAccount;
	}

	public Boolean getSearch() {
		return search;
	}

	public void setSearch(Boolean search) {
		this.search = search;
	}

	public Boolean getMultipleParcel() {
		return multipleParcel;
	}

	public void setMultipleParcel(Boolean multipleParcel) {
		this.multipleParcel = multipleParcel;
	}

	public String getStringDate() {
		return stringDate;
	}

	public void setStringDate(String stringDate) {
		this.stringDate = stringDate;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}
