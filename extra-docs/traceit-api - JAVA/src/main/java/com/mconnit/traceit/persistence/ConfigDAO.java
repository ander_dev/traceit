package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Config;

public interface ConfigDAO extends GenericDAO<Config> {

}
