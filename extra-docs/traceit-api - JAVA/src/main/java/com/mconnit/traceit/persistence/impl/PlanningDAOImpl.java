package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Planning;
import com.mconnit.traceit.persistence.PlanningDAO;
import org.springframework.stereotype.Repository;

@Repository("planningDAO")
public class PlanningDAOImpl extends GenericDAOImpl<Planning> implements PlanningDAO {


}
