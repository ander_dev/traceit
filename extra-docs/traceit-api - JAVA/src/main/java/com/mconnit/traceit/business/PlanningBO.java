package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Planning;
import com.mconnit.traceit.entity.PlanningGroup;
import com.mconnit.traceit.entity.PlanningItem;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("planningBO")
public interface PlanningBO extends GenericBO<Planning> {

	List<Planning> list(final Planning plannig) throws Exception;

	MessageReturn save(final Planning plannig) throws Exception;

	MessageReturn saveGroup(final PlanningGroup planningGroup) throws Exception;

	MessageReturn saveItem(final PlanningItem planningItem) throws Exception;

	MessageReturn deletePlanningGroup(final PlanningGroup planningGroup);

	MessageReturn deletePlanning(final Planning planning);

	Planning getById(Long id);

	Planning getSelectedPlanning(final User user) throws Exception;
}

