package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.PlanningGroup;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("planningGroupBO")
public interface PlanningGroupBO extends GenericBO<PlanningGroup> {

	List<PlanningGroup> list() throws Exception;

	MessageReturn save(final PlanningGroup plannnigGroup) throws Exception;

	MessageReturn delete(Long id);

	PlanningGroup getById(Long id);
}

