package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Chart;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("chartBO")
public interface ChartBO extends GenericBO<Chart> {

	List<Chart> listByUser(Long userId, Boolean visible, String orderby) throws Exception;

	List<Chart> listByParameter(Chart chart) throws Exception;

	List<Chart> listByParameter(Map<String, String> params, String orderBy) throws Exception;

	MessageReturn save(final Chart chart) throws Exception;

	MessageReturn delete(Long id);

	Chart getById(Long id);

}

