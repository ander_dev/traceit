package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.TypeClosure;
import com.mconnit.traceit.persistence.TypeClosureDAO;
import org.springframework.stereotype.Repository;

@Repository("typeClosureDAO")
public class TypeClosureDAOImpl extends GenericDAOImpl<TypeClosure> implements TypeClosureDAO {


}
