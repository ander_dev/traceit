package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.PlanningGroup;

public interface PlanningGroupDAO extends GenericDAO<PlanningGroup> {

}
