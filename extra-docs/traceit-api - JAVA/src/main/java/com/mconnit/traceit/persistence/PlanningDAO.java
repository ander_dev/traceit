package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Planning;

public interface PlanningDAO extends GenericDAO<Planning> {

}
