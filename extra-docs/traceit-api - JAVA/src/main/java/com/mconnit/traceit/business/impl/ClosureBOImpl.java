package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.AccountBO;
import com.mconnit.traceit.business.ChartBO;
import com.mconnit.traceit.business.ClosureBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.*;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.persistence.AccountDAO;
import com.mconnit.traceit.persistence.ClosureDAO;
import com.mconnit.traceit.persistence.TypeAccountDAO;
import com.mconnit.traceit.utils.Constants;
import com.mconnit.traceit.utils.MessageFactory;
import com.mconnit.traceit.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

public class ClosureBOImpl extends GenericBOImpl<Closure> implements ClosureBO {

	@Autowired
	private UserBO userBO;

	@Autowired
	private AccountBO accountBO;

	@Autowired
	private ChartBO chartBO;

	@Autowired
	private AccountDAO accountDAO;

	@Autowired
	private ClosureDAO closureDAO;

	@Autowired
	private TypeAccountDAO typeAccountDAO;

	private User getSuperUser(Closure closure) {
		return userBO.getSuperUser(closure.getAccount().getUser());
	}

	@Override
	@Transactional
	public MessageReturn save(Closure closure) {
		MessageReturn libReturn = new MessageReturn();

		closure.getAccount().setUser(getSuperUser(closure));
		if (closure.getAccount().getUser() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_user_is_null", "en"));
		} else if (closure.getAccount() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_account_is_null", closure.getAccount().getUser().getLanguage()));
		} else if (closure.getDate() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_date_is_null", closure.getAccount().getUser().getLanguage()));
		} else {
			try {

				Account account = findById(Account.class, closure.getAccount().getId());
				closure.setAccount(account);

				closure = closeValues(closure);
				saveGeneric(closure);

				BigDecimal totalDebit = account.getTotalDebit().doubleValue() > 0 ? account.getTotalDebit().subtract(closure.getTotalDebit()) : BigDecimal.ZERO;
				BigDecimal totalCredit = account.getTotalCredit().doubleValue() > 0 ? account.getTotalCredit().subtract(closure.getTotalCredit()) : BigDecimal.ZERO;

				account.setTotalCredit(totalCredit);
				account.setTotalDebit(totalDebit);

				accountDAO.save(account);
				closure.setAccount(account);
			} catch (Exception e) {
				e.printStackTrace();
				libReturn.setClosure(closure);
				libReturn.setMessage(e.getMessage());
			}
			if (libReturn.getMessage() == null && closure.getId() == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_closure_saved", closure.getAccount().getUser().getLanguage()));
				libReturn.setClosure(closure);
				libReturn.setAccount(closure.getAccount());
			} else if (libReturn.getMessage() == null && closure.getId() != null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_closure_updated", closure.getAccount().getUser().getLanguage()));
				libReturn.setClosure(closure);
				libReturn.setAccount(closure.getAccount());
			}
		}

		return libReturn;
	}

	@Transactional
	private Closure closeValues(Closure closure) {
		try {
			Account acc = closure.getAccount();
			Closure closureTemp = null;
			Date forcedDate = null;
			for (Closure clo : acc.getClosureList()) {
				if (clo.getId() != null) {
					if (closureTemp == null) {
						closureTemp = clo;
					} else if (closureTemp.getId() < clo.getId()) {
						closureTemp = clo;
					}
				}
			}

			if (closureTemp != null) {
				forcedDate = closureTemp.getEndDate().getTime();
			}

			Calendar date = closure.getDate();

			TypeClosure typeClosure = findById(TypeClosure.class, closure.getAccount().getTypeClosure().getId());

			HashMap<String, String> ret = new HashMap<String, String>();

			if (Constants.MENSAL.equalsIgnoreCase(typeClosure.getType().toLowerCase()) || Constants.MONTHLY.equalsIgnoreCase(typeClosure.getType().toLowerCase())) {
				ret = Utils.loadDates(date.getTime(), Calendar.DAY_OF_MONTH, -Utils.getLastDayOfMonth(date.getTime()), forcedDate);
			} else if (Constants.QUINZENAL.equalsIgnoreCase(typeClosure.getType().toLowerCase()) || Constants.FORTNIGHTLY.equalsIgnoreCase(typeClosure.getType().toLowerCase())) {
				ret = Utils.loadDates(date.getTime(), Calendar.DAY_OF_MONTH, -14, forcedDate);
			} else if (Constants.SEMANAL.equalsIgnoreCase(typeClosure.getType().toLowerCase()) || Constants.WEEKLY.equalsIgnoreCase(typeClosure.getType().toLowerCase())) {
				ret = Utils.loadDates(date.getTime(), Calendar.DAY_OF_MONTH, -6, forcedDate);
			} else if (Constants.DIARIO.equalsIgnoreCase(typeClosure.getType().toLowerCase()) || Constants.DAILY.equalsIgnoreCase(typeClosure.getType().toLowerCase())) {
				ret = Utils.loadDates(date.getTime(), Calendar.DAY_OF_MONTH, 0, forcedDate);
			}

			System.out.println("Account: "+acc.getDescription()+" | User: "+acc.getUser().getName());
			System.out.println("CLosure Type: " + typeClosure.getType());
			System.out.println("Start: " + ret.get(Constants.DATE_START) + " | End: " + ret.get(Constants.DATE_END));

			closure = getClosureValues(closure, ret.get(Constants.DATE_START), ret.get(Constants.DATE_END));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return closure;
	}

	private Closure getClosureValues(Closure closure, String startDate, String endDate) {
		BigDecimal sumCredit = new BigDecimal(0.00);
		BigDecimal sumDebit = new BigDecimal(0.00);

		Collection<Register> collectionCredit = closureDAO.getRegisters(closure.getAccount(), startDate, endDate, true, getTypeAccountByDescription(MessageFactory.getMessage("lb_credit", closure.getAccount().getUser().getLanguage())).getId());

		Collection<Register> collectionDebit = closureDAO.getRegisters(closure.getAccount(), startDate, endDate, true, getTypeAccountByDescription(MessageFactory.getMessage("lb_debit", closure.getAccount().getUser().getLanguage())).getId());

		for (Register debit : collectionDebit) {
			sumDebit = sumDebit.add(debit.getAmount());
		}

		for (Register credit : collectionCredit) {
			sumCredit = sumCredit.add(credit.getAmount());
		}

		BigDecimal total = sumCredit.subtract(sumDebit);
		closure.setTotalCredit(sumCredit);
		closure.setTotalDebit(sumDebit);
		closure.setTotalGeneral(total);
		closure.setStartDate(Utils.stringToDate(startDate, true));
		closure.setEndDate(Utils.stringToDate(endDate, true));
		closure.setUpdatedBy(closure.getAccount().getUser().getUsername());
		closure.setUpdatedWhen(closure.getDate().getTime());

		return closure;
	}

	private TypeAccount getTypeAccountByDescription(String description) {
		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.description = ", "'" + description + "'");
		TypeAccount typeAccount = getTypeAccountByParameter(queryParams);
		return typeAccount;
	}

	private TypeAccount getTypeAccountByParameter(Map<String, String> queryParams) {
		try {
			return typeAccountDAO.findByParameter(TypeAccount.class, queryParams);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Closure> list(User user) throws Exception {
		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.user.id = ", user.getId() + "");
		List<Closure> result = list(Closure.class, queryParams, null);
		return result;
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {

		MessageReturn libReturn = new MessageReturn();
		Closure closure = null;
		try {
			closure = findById(Closure.class, id);
			if (closure == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_closure_not_found", "en"));
			} else {
				String locale = closure.getAccount().getUser().getLanguage();
				remove(closure);
				libReturn.setMessage(MessageFactory.getMessage("lb_closure_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public Closure getById(Long id) {
		try {
			return findById(Closure.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Closure> listByParameter(Closure closure) throws Exception {
		User user = closure.getAccount().getUser();

		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where ", " 1=1 ");
		queryParams.put(" and x.user = ", user.getId() + "");

		return list(Closure.class, queryParams, " x.date desc");
	}

	@Override
	@Transactional
	public void closeValuesByUserAndDate(User user, Calendar date) throws Exception{
		if(user != null && date != null){
			Account account = new Account();
			account.setUser(user);

			List<Account> accountList = accountBO.listByParameter(account);

			for (Account acc: accountList) {
				Closure theClosure = null;
				//get last closure for current account
				for (Closure tempClosure: acc.getClosureList()) {
					if(theClosure == null){
						theClosure = tempClosure;
					} else if (tempClosure.getId() > theClosure.getId()){
						theClosure = tempClosure;
					}
				}

				if (Constants.MENSAL.equalsIgnoreCase(acc.getTypeClosure().getType().toLowerCase()) || Constants.MONTHLY.equalsIgnoreCase(acc.getTypeClosure().getType().toLowerCase())) {
					//if theClosure == null then test current date last day must be 30 or higher then previous month
					if(theClosure == null && Utils.isEndOfMonth(date.getTime())){
						createNewClosure(acc, date);
					} else if (theClosure != null && Utils.areDatesAMonthApart(theClosure.getDate(), date)){
						createNewClosure(acc, date);
					}
				} else if (Constants.QUINZENAL.equalsIgnoreCase(acc.getTypeClosure().getType().toLowerCase()) || Constants.FORTNIGHTLY.equalsIgnoreCase(acc.getTypeClosure().getType().toLowerCase())) {
					if(theClosure == null){
						createNewClosure(acc, date);
					} else if (theClosure != null && Utils.areDatesFortnightlyApart(theClosure.getDate(), date)){
						createNewClosure(acc, date);
					}
				} else if (Constants.SEMANAL.equalsIgnoreCase(acc.getTypeClosure().getType().toLowerCase()) || Constants.WEEKLY.equalsIgnoreCase(acc.getTypeClosure().getType().toLowerCase())) {
					if(theClosure == null){
						createNewClosure(acc, date);
					} else if (theClosure != null && Utils.areDatesWeekApart(theClosure.getDate(), date)){
						createNewClosure(acc, date);
					}
				} else if (Constants.DIARIO.equalsIgnoreCase(acc.getTypeClosure().getType().toLowerCase()) || Constants.DAILY.equalsIgnoreCase(acc.getTypeClosure().getType().toLowerCase())) {
					if(theClosure == null){
						createNewClosure(acc, date);
					} else if (theClosure != null && Utils.areDatesDayApart(theClosure.getDate(), date)){
						createNewClosure(acc, date);
					}
				}
			}
		}
	}

	@Transactional
	private void createNewClosure(Account acc, Calendar date) throws Exception {
		Closure closure = new Closure();
		closure.setDate(date);
		closure.setAccount(acc);

		save(closure);

		if(Utils.isEndOfMonth(date.getTime())){
			hideCharts(acc.getUser(), date);
		}
	}

	private void hideCharts(User user, Calendar date) throws Exception {
		//list charts by user and visible to hide then out
		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.visible = ", Utils.setBooleanValue(true)+"");
		queryParams.put(" and x.user = ", user.getId() + "");
		List<Chart> availableCharts = chartBO.listByParameter(queryParams, null);

		for (Chart chart : availableCharts) {
			chart.setVisible(false);
			chartBO.saveGeneric(chart);
		}

		//get next month chart and make then visible
		Calendar calendar = Calendar.getInstance(date.getTimeZone());
		calendar.add(Calendar.MONTH, 1);

		queryParams.clear();
		queryParams.put(" where date_format(x.date, '%m/%Y') = ", "'"+Utils.dateToStringMonthYear(calendar.getTime())+"'");
		queryParams.put(" and x.user = ", user.getId() + "");
		availableCharts = chartBO.listByParameter(queryParams, null);

		for (Chart chart : availableCharts) {
			chart.setVisible(true);
			chartBO.saveGeneric(chart);
		}
	}

}
