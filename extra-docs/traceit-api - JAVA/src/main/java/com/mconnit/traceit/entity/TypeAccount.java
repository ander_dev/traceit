package com.mconnit.traceit.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@Entity
@Table(name = "typeaccount")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"id", "description", "locale", "showType"})
public class TypeAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	@XmlAttribute
	private Long id;

	@XmlAttribute
	private String description;

	@XmlAttribute
	private String locale;

	@XmlAttribute
	@Column(name = "showtype")
	private Boolean showType;

	@XmlAttribute
	@Transient
	private String showTypeString;

	@XmlAttribute
	@Transient
	private Boolean selected;

	public String getShowTypeString() {
		return showTypeString;
	}

	public void setShowTypeString(String showTypeString) {
		this.showTypeString = showTypeString;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Boolean getShowType() {
		return showType;
	}

	public void setShowType(Boolean showType) {
		this.showType = showType;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}
}
