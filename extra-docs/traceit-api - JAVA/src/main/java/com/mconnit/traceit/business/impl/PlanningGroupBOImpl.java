package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.ChartBO;
import com.mconnit.traceit.business.PlanningGroupBO;
import com.mconnit.traceit.business.PlanningItemBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.Chart;
import com.mconnit.traceit.entity.PlanningGroup;
import com.mconnit.traceit.entity.PlanningItem;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.utils.MessageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class PlanningGroupBOImpl extends GenericBOImpl<PlanningGroup> implements PlanningGroupBO {

	@Autowired
	private UserBO userBO;

	@Autowired
	private ChartBO chartBO;

	@Autowired
	private PlanningItemBO planningItemBO;

	private User getSuperUser(PlanningGroup planningGroup) {
		return userBO.getSuperUser(planningGroup.getUser());
	}

	@Override
	@Transactional
	public MessageReturn save(PlanningGroup planningGroup) {
		MessageReturn libReturn = new MessageReturn();
		try {
			if (planningGroup.getId() != null) {
				planningGroup.setUpdatedBy(planningGroup.getUser().getUsername());
			}
			planningGroup.setUser(getSuperUser(planningGroup));
			saveGeneric(planningGroup);
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setPlanningGroup(planningGroup);
			libReturn.setMessage(e.getMessage());
		}
		if (libReturn.getMessage() == null && planningGroup.getId() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_saved", planningGroup.getUser().getLanguage()));
			libReturn.setPlanningGroup(planningGroup);
		} else if (libReturn.getMessage() == null && planningGroup.getId() != null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_updated", planningGroup.getUser().getLanguage()));
			libReturn.setPlanningGroup(planningGroup);
		}

		return libReturn;
	}

	public List<PlanningGroup> list() throws Exception {
		return list(PlanningGroup.class, null, null);
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		PlanningGroup planningGroup = null;
		try {
			planningGroup = findById(PlanningGroup.class, id);
			if (planningGroup == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_planning_not_found", "en"));
			} else {
				for (PlanningItem planningItem : planningGroup.getPlannigItemList()) {
					Chart chart = planningItem.getChart();
					planningItem.setChart(null);
					planningItemBO.save(planningItem);

					if (chart != null) {
						chartBO.remove(chart);
					}

					planningItemBO.remove(planningItem);
				}
				String locale = planningGroup.getUser().getLanguage();
				remove(planningGroup);
				libReturn.setMessage(MessageFactory.getMessage("lb_planning_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public PlanningGroup getById(Long id) {
		try {
			return findById(PlanningGroup.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
