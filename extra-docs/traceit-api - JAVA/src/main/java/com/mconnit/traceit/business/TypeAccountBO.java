package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.entity.TypeAccount;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("typeAccountBO")
public interface TypeAccountBO extends GenericBO<TypeAccount> {

	List<TypeAccount> list(final User user, final Boolean showType) throws Exception;

	MessageReturn save(final TypeAccount typeAccount) throws Exception;

	MessageReturn delete(Long id);

	TypeAccount getById(Long id);

	TypeAccount getByDescription(Description description);

	Map<String, Object> getTypeAccountDescriptionByUserAndDescription(User user, String description, Boolean withTypeAccount);

}

