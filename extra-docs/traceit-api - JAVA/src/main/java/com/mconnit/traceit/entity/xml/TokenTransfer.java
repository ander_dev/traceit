package com.mconnit.traceit.entity.xml;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
public class TokenTransfer implements Serializable {

	private static final long serialVersionUID = -6970616561454320306L;

	private String token;

	public TokenTransfer() {
		super();
	}

	public TokenTransfer(String token) {
		this.token = token;
	}

	public String getToken() {
		return this.token;
	}

}