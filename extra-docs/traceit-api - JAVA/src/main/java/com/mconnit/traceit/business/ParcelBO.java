package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Parcel;
import org.springframework.stereotype.Service;

@Service("parcelBO")
public interface ParcelBO extends GenericBO<Parcel> {

}

