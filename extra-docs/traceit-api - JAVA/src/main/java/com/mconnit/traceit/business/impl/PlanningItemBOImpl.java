package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.PlanningItemBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.PlanningItem;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.persistence.PlanningItemDAO;
import com.mconnit.traceit.utils.MessageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PlanningItemBOImpl extends GenericBOImpl<PlanningItem> implements PlanningItemBO {

	@Autowired
	private PlanningItemDAO planningItemDAO;

	@Autowired
	private UserBO userBO;

	private User getSuperUser(PlanningItem planningItem) {
		return userBO.getSuperUser(planningItem.getUser());
	}

	@Override
	@Transactional
	public MessageReturn save(PlanningItem planningItem) {
		MessageReturn libReturn = new MessageReturn();
		try {
			if (planningItem.getId() != null) {
				planningItem.setUpdatedBy(planningItem.getUser().getUsername());
				planningItem.setUpdatedWhen(new Date());
				planningItem.setDate(Calendar.getInstance());
			}

			planningItem.setUser(getSuperUser(planningItem));
			saveGeneric(planningItem);
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setPlanningItem(planningItem);
			libReturn.setMessage(e.getMessage());
		}
		if (libReturn.getMessage() == null && planningItem.getId() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_saved", planningItem.getUser().getLanguage()));
			libReturn.setPlanningItem(planningItem);
		} else if (libReturn.getMessage() == null && planningItem.getId() != null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_updated", planningItem.getUser().getLanguage()));
			libReturn.setPlanningItem(planningItem);
		}

		return libReturn;
	}

	public List<PlanningItem> list() throws Exception {
		return list(PlanningItem.class, null, null);
	}

	@Override
	public PlanningItem getById(Long id) {
		try {
			return findById(PlanningItem.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
