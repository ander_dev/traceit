package com.mconnit.traceit.entity.xml;

import com.mconnit.traceit.entity.Register;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "PaginationReturn")
public class PaginationReturn {

	private List<Register> registerList = new ArrayList<>();

	private int rowCount;

	private PaginationResults paginationResults;

	public List<Register> getRegisterList() {
		if (registerList == null) {
			registerList = new ArrayList<>();
		}
		return registerList;
	}

	public void setRegisterList(List<Register> registerList) {
		this.registerList = registerList;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public PaginationResults getPaginationResults() {
		return paginationResults;
	}

	public void setPaginationResults(PaginationResults paginationResults) {
		this.paginationResults = paginationResults;
	}
}
