package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.AccountBO;
import com.mconnit.traceit.entity.Account;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.entity.xml.TransferBtAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/account")
@CrossOrigin(origins = "http://localhost:3000")
public class AccountService {

	@Autowired
	private AccountBO accountBO;

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/getall", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public List<Account> listByParameter(@RequestBody Account account) {

		List<Account> list = new ArrayList<>();
		try {
			list = accountBO.listByParameter(account);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public Account getAccountById(@PathVariable Long id) {
		Account ret = new Account();
		try {
			ret = accountBO.getById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	@ResponseBody
	public MessageReturn saveAccount(@RequestBody Account account) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = accountBO.save(account);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public MessageReturn delete(@RequestBody ArrayList<Long> ids) {
		MessageReturn ret = new MessageReturn();
		try {
			for (Long id : ids) {
				ret = accountBO.delete(id);
			}
		} catch (Exception e) {
			ret.setMessage(e.getMessage());
			ret.setException(Boolean.TRUE);
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/transfer", method = RequestMethod.PUT, headers = "Accept=application/json")
	@ResponseBody
	public MessageReturn transferBtAccounts(@RequestBody TransferBtAccounts transferBtAccounts) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = accountBO.transferBtAccounts(transferBtAccounts);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
