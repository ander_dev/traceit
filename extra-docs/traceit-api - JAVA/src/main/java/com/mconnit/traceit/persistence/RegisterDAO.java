package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.entity.Register;
import com.mconnit.traceit.entity.TypeAccount;
import com.mconnit.traceit.entity.User;

public interface RegisterDAO extends GenericDAO<Register> {

	Register getByDescription(Description description, User user, TypeAccount typeAccount) throws Exception;

}
