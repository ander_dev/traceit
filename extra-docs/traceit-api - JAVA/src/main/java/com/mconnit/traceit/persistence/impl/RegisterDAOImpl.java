package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.entity.Register;
import com.mconnit.traceit.entity.TypeAccount;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.persistence.RegisterDAO;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;

@Repository("registerDAO")
public class RegisterDAOImpl extends GenericDAOImpl<Register> implements RegisterDAO {

	@Override
	public Register getByDescription(Description description, User user, TypeAccount typeAccount) throws Exception {

		StringBuilder sql = new StringBuilder();
		sql.append(" select d from Register d ");
		sql.append(" where d.id = ");
		sql.append(" ( select max(id) from Register ");
		sql.append(" where typeAccount = ").append(typeAccount.getId());
		sql.append(" and user = ").append(user.getId());
		sql.append(" and description = ").append(description.getId()).append(" )");

		System.out.println("Query: " + sql.toString());

		Query query = em.createQuery(sql.toString());
		Register res;
		try {
			res = (Register) query.getSingleResult();
		} catch (NoResultException nre) {
			res = null;
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
		return res;
	}


}
