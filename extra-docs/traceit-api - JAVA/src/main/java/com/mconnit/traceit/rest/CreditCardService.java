package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.CreditCardBO;
import com.mconnit.traceit.entity.CreditCard;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/creditcard")
@CrossOrigin(origins = "http://localhost:3000")
public class CreditCardService {

	@Autowired
	private CreditCardBO creditCardBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{userId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<CreditCard> listByParameter(@PathVariable String userId) {
		List<CreditCard> list = new ArrayList<>();
		try {
			list = creditCardBO.listByParameter(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn save(@RequestBody CreditCard creditCard) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = creditCardBO.save(creditCard);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn delete(@RequestBody ArrayList<Long> ids) {
		MessageReturn ret = new MessageReturn();
		try {
			for (Long id : ids) {
				ret = creditCardBO.delete(Long.valueOf(id));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
