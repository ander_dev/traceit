package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Chart;

public interface ChartDAO extends GenericDAO<Chart> {

}