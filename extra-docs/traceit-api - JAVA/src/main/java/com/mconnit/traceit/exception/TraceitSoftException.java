package com.mconnit.traceit.exception;

/**
 * Created by anderson on 17/04/16.
 */
public class TraceitSoftException extends Exception {

	private static final long serialVersionUID = 1L;

	private String mensagem;

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public TraceitSoftException(String msg) {
		super(msg);
		setMensagem(msg);
	}
}
