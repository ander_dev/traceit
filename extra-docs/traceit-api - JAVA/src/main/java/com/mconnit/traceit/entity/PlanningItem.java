package com.mconnit.traceit.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "planningitem")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PlanningItem extends Audit implements Serializable {

	private static final long serialVersionUID = -4253573156625710123L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	private Long id;

	@Column(length = 13, precision = 13, scale = 2)
	private BigDecimal amount;

	@Column(length = 13, precision = 13, scale = 2)
	private BigDecimal currentAmount;

	private Calendar date;

	private Integer month;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = PlanningGroup.class)
	@JoinColumn(name = "planninggroup_id", foreignKey = @ForeignKey(name = "FK_PLITEM_PLGROUP"))
	@JsonIgnore
	private PlanningGroup planningGroup;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = User.class)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_PLITEM_USER"))
	private User user;

	@OneToOne(cascade = {CascadeType.REFRESH}, targetEntity = Chart.class)
	@JoinColumn(name = "chart_id", foreignKey = @ForeignKey(name = "FK_PLITEM_CHART"))
	private Chart chart;

	@OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "planning_item_id", foreignKey = @ForeignKey(name = "FK_PLITEM_REGISTER"))
	@Fetch(value = FetchMode.SUBSELECT)
	@JsonIgnore
	private List<Register> registerList;

	@Transient
	private Integer instalment;

	public List<Register> getRegisterList() {
		return registerList;
	}

	public void setRegisterList(List<Register> registerList) {
		if (this.registerList == null) {
			this.registerList = new ArrayList<>();
		}
		this.registerList = registerList;
	}

	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(BigDecimal currentAmount) {
		this.currentAmount = currentAmount;
	}

	public Integer getInstalment() {
		return instalment;
	}

	public void setInstalment(Integer instalment) {
		this.instalment = instalment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public PlanningGroup getPlanningGroup() {
		return planningGroup;
	}

	public void setPlanningGroup(PlanningGroup planningGroup) {
		this.planningGroup = planningGroup;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public Chart getChart() {
		return chart;
	}

	public void setChart(Chart chart) {
		this.chart = chart;
	}
}
