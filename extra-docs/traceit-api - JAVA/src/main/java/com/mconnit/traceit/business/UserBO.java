package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userBO")
public interface UserBO extends GenericBO<User> {

	List<User> list() throws Exception;

	List<User> listByUser(final Long id) throws Exception;

	MessageReturn save(final User user) throws Exception;

	MessageReturn delete(Long id);

	User getById(Long id);

	MessageReturn login(String username, String password);

	User getSuperUser(User user);

	MessageReturn validateToken(String token) throws Exception;

	MessageReturn validateNewUser(String token);

	MessageReturn resetPassword(String email, String locale, String authenticationUrl);

	MessageReturn registerNewPassword(User user);

	void validateEmail(String email) throws Exception;
}

