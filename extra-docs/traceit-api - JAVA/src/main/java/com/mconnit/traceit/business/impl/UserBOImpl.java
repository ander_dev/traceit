package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.business.impl.security.TokenUtils;
import com.mconnit.traceit.entity.Config;
import com.mconnit.traceit.entity.Role;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.entity.xml.TokenTransfer;
import com.mconnit.traceit.persistence.ConfigDAO;
import com.mconnit.traceit.persistence.UserDAO;
import com.mconnit.traceit.thread.EmailThread;
import com.mconnit.traceit.utils.Constants;
import com.mconnit.traceit.utils.Crypt;
import com.mconnit.traceit.utils.MessageFactory;
import com.mconnit.traceit.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.RollbackException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserBOImpl extends GenericBOImpl<User> implements UserBO {

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private ConfigDAO configDAO;

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authManager;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	@Transactional
	public MessageReturn save(User user) {
		MessageReturn libReturn = new MessageReturn();
		Boolean newUser = false;
		if (user.getLanguage() != null) {
			try {
				if (user.getId() == null) {

					validateEmail(user.getEmail());

					if (user.getSuperUser() == null) {
						if (!user.getPassword().equals(user.getConfirmPassword())) {
							throw new Exception(MessageFactory.getMessage("lb_password_not_match", user.getLanguage()));
						}
					}

					MessageReturn ret = userDAO.getByUsername(user.getUsername());
					if (ret.getUser() != null) {
						if (ret.getUser().getUsername().equals(user.getUsername()) && !ret.getUser().getEmail().equals(user.getEmail())) {
							throw new Exception(MessageFactory.getMessage("lb_username_in_use", user.getLanguage()));
						} else {
							throw new Exception(MessageFactory.getMessage("lb_user_already_registered", user.getLanguage()));
						}
					}
					if (user.getBirth() != null && !user.getBirth().isEmpty()) {
						user.setBirthDate(Utils.stringToDate(user.getBirth(), false).getTime());
					}
					user.setRegister(new Date());
					user.setExcluded(false);

					if (user.getDefaultPassword() != null && user.getDefaultPassword()) {
						user.setPassword(Crypt.encrypt(user.getPassword()));
						user.setOldPassword(user.getPassword());
						user.setConfirmPassword(user.getPassword());
						user.setDefaultPassword(true);
						user.setEmailVerified(true);
					} else {
						user.setPassword(passwordEncoder.encode(user.getPassword()));
						user.setConfirmPassword(user.getPassword());
						user.setDefaultPassword(false);
						user.setEmailVerified(false);
					}
				} else {
					if (user.getDefaultPassword()) {
						if (user.getPassword() == null || !user.getPassword().equals(user.getConfirmPassword())) {
							throw new Exception(MessageFactory.getMessage("lb_password_not_match", user.getLanguage()));
						} else {
							MessageReturn ret = userDAO.getByUsername(user.getUsername());
							if (ret.getUser().getOldPassword().equals(Crypt.encrypt(user.getOldPassword()))) {
								user.setPassword(passwordEncoder.encode(user.getPassword()));
								user.setDefaultPassword(false);
							} else {
								throw new Exception(MessageFactory.getMessage("lb_wrong_password_review_email", ret.getUser().getLanguage()));
							}
						}
					}
					user.setUpdatedBy(user.getUsername());
					user.setUpdatedWhen(new Date());
				}
				Role role;
				Map<String, String> queryParams = new LinkedHashMap<>();
				if (user.getRole() == null || (user.getRole() != null && user.getRole().getId() == null) && user.getSuperUser() != null) {
					queryParams.clear();
					queryParams.put(" where x.role = '", Constants.ROLE_USER + "'");
					role = findByParameter(Role.class, queryParams);
				} else if (user.getRole() == null || (user.getRole() != null && user.getRole().getId() == null) && user.getToken() == null) {
					queryParams.clear();
					queryParams.put(" where x.role = '", Constants.ROLE_SUPER_USER + "'");
					role = findByParameter(Role.class, queryParams);
				} else {
					role = findById(Role.class, user.getRole().getId());
				}

				if (user.getId() != null) {
					queryParams.clear();
					queryParams.put(" where x.user = ", getSuperUser(user).getId() + "");
					Config config = configDAO.findByParameter(Config.class, queryParams);
					libReturn.setConfig(config);
				}

				if (role != null) {
					user.setRole(role);
					if (user.getId() == null) {
						newUser = true;
					}
					String tokenURL = user.getAuthenticationUrl();
					user = saveGeneric(user);
					user.setAuthenticationUrl(tokenURL);
				}
				libReturn.setStatusCode(200);
				libReturn.setException(false);
			} catch (Exception e) {
				// TODO fix exceptions to do not print if not on DEBUG
//				e.printStackTrace();
				libReturn.setStatusCode(412);
				libReturn.setException(true);
				libReturn.setUser(user);
				libReturn.setMessage(e.getMessage());
			}

			if (libReturn.getMessage() == null && newUser) {
				libReturn.setUser(user);
				if (user.getSuperUser() != null) {
					libReturn.setMessage(MessageFactory.getMessage("lb_user_saved_user", user.getSuperUser().getLanguage()));
					EmailThread email = new EmailThread(user, "USER");
					email.start();
				} else {
					libReturn.setMessage(MessageFactory.getMessage("lb_user_saved_super_user", user.getLanguage()));
					EmailThread email = new EmailThread(user, "SUPER-USER");
					email.start();
				}
			} else if (libReturn.getMessage() == null && !newUser) {
				libReturn.setMessage(MessageFactory.getMessage("lb_user_updated", user.getLanguage()));
				libReturn.setUser(user);
			}
		} else {
			// TODO this error message has to be refactored
			libReturn.setMessage(MessageFactory.getMessage("lb_city_not_found", user.getLanguage()));
		}

		return libReturn;
	}

	@Override
	public List<User> list() throws Exception {
		return list(User.class, null, null);
	}

	@Override
	public List<User> listByUser(final Long id) throws Exception {
		Map<String, String> queryParams = new LinkedHashMap<String, String>();
		queryParams.put(" where x.superUser", "= " + id);
		queryParams.put(" or x.id", "= " + id);
		return list(User.class, queryParams, "x.name asc");
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		User user = null;
		try {
			user = findById(User.class, id);
			if (user == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_user_not_found", "en"));
			} else {
				String locale = user.getLanguage();
				remove(user);
				libReturn.setMessage(MessageFactory.getMessage("lb_user_deleted", locale));
			}
		} catch (RollbackException r) {
			r.printStackTrace();
			libReturn.setMessage(r.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public User getById(Long id) {
		try {
			return findById(User.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public MessageReturn login(String username, String password) {
		MessageReturn messageReturn;
		try {
			messageReturn = getByUsername(username);
			if (messageReturn.getUser() == null) {
				messageReturn.setMessage(MessageFactory.getMessage("lb_user_not_found", "en"));
				messageReturn.setStatusCode(400);
			} else if (!messageReturn.getUser().getEmailVerified()) {
				messageReturn.setMessage(MessageFactory.getMessage("lb_user_email_not_verified", messageReturn.getUser().getLanguage()));
				messageReturn.setUser(null);
				messageReturn.setStatusCode(400);
			} else if (messageReturn.getUser().getDefaultPassword()) {
				if (!messageReturn.getUser().getPassword().equals(Crypt.encrypt(password))) {
					messageReturn.setUser(null);
					messageReturn.setMessage(MessageFactory.getMessage("lb_wrong_password_review_email", messageReturn.getUser().getLanguage()));
					messageReturn.setStatusCode(400);
				} else {
					messageReturn.setMessage(MessageFactory.getMessage("lb_user_confirm_password", messageReturn.getUser().getLanguage()));
					messageReturn.setStatusCode(400);
				}
			} else {

				messageReturn.setTokenTransfer(getTokenTransfer(username, password));

				if ("ADMIN".equals(messageReturn.getUser().getRole().getRole())) {
					messageReturn.getUser().setAdmin(true);
				}

				Map<String, String> queryParams = new LinkedHashMap<>();
				queryParams.put(" where x.user = ", getSuperUser(messageReturn.getUser()).getId() + "");
				Config config = configDAO.findByParameter(Config.class, queryParams);
				messageReturn.setConfig(config);

				messageReturn.setMessage(MessageFactory.getMessage("lb_login_success", messageReturn.getUser().getLanguage()));
				messageReturn.setStatusCode(200);
			}
		} catch (Exception e) {
			e.printStackTrace();
			messageReturn = new MessageReturn();
			messageReturn.setStatusCode(400);
			if (e.getMessage().equals("Bad credentials")) {
				messageReturn.setMessage(MessageFactory.getMessage("lb_user_incorrect_password", "en"));
			} else {
				messageReturn.setMessage(e.getMessage());
			}
		}
		return messageReturn;
	}

	private TokenTransfer getTokenTransfer(String username, String password) {
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
		Authentication authentication = this.authManager.authenticate(authenticationToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		/*
		 * Reload user as password of authentication principal will be null after authorization and password is needed for token generation
		 */
		UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
		return new TokenTransfer(TokenUtils.createToken(userDetails));
	}

	@Override
	public User getSuperUser(User user) {
		User userRet = getById(user.getId());
		if (userRet.getSuperUser() == null) {
			return userRet;
		} else {
			return userRet.getSuperUser();
		}
	}

	private MessageReturn getByUsername(String username) throws Exception {
		MessageReturn ret = userDAO.getByUsername(username);
		if (ret.getUser() != null) {
			Map<String, String> queryParams = new LinkedHashMap<>();
			queryParams.put(" where x.user = ", getSuperUser(ret.getUser()).getId() + "");
			Config config = configDAO.findByParameter(Config.class, queryParams);
			ret.setConfig(config);
		}
		return ret;
	}

	@Override
	public MessageReturn validateToken(String token) throws Exception {
		MessageReturn messageReturn = new MessageReturn();
		UserDetails userDetails = this.userDetailsService.loadUserByUsername(token.split(":")[0]);
		if (userDetails != null) {
			Boolean valid = TokenUtils.validateToken(token, userDetails);
			if (valid) {
				messageReturn = getByUsername(userDetails.getUsername());
				Map<String, String> queryParams = new LinkedHashMap<>();
				queryParams.put(" where x.user = ", getSuperUser(messageReturn.getUser()).getId() + "");
				Config config = configDAO.findByParameter(Config.class, queryParams);
				messageReturn.setConfig(config);
				messageReturn.setStatusCode(200);
			} else {
				messageReturn.setMessage(MessageFactory.getMessage("lb_expired_token", "en"));
				messageReturn.setStatusCode(400);
			}
		}
        return messageReturn;
	}

	@Override
	@Transactional
	public MessageReturn validateNewUser(String token) {
		MessageReturn ret = new MessageReturn();
		try {
			String[] splitedToken = Crypt.decrypt(token).split("#");
			Calendar registerDate = Utils.stringToDate(splitedToken[1], false);
			Calendar finalDate = Utils.addDaysToADate(registerDate, 2);
			User user = getById(new Long(splitedToken[0]));
			if (!registerDate.after(finalDate)) {
				user.setEmailVerified(true);
				saveGeneric(user);
				ret.setMessage(MessageFactory.getMessage("lb_user_autorized", user.getLanguage()));
				ret.setUser(user);
			} else {
				delete(user.getId());
				ret.setMessage(MessageFactory.getMessage("lb_user_autorization_expired", user.getLanguage()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Override
	@Transactional
	public MessageReturn resetPassword(String email, String locale, String authenticationUrl) {
		MessageReturn ret = new MessageReturn();
		try {
			Map<String, String> queryParams = new LinkedHashMap<>();
			queryParams.put("where x.email = '", email + "'");
			User user = userDAO.findByParameter(User.class, queryParams);
			if (user != null) {
				user.setPassword(Crypt.encrypt("NeedToChangeIt"));
				user.setOldPassword(user.getPassword());
				user.setConfirmPassword(user.getPassword());
				user.setDefaultPassword(true);
				user.setEmailVerified(true);
				user = userDAO.save(user);
				user.setAuthenticationUrl(authenticationUrl);
				ret.setMessage(MessageFactory.getMessage("lb_user_reset_password", user.getLanguage()));
				ret.setStatusCode(200);
				EmailThread emailThread = new EmailThread(user, "RESET-PASSWORD");
				emailThread.start();
			} else {
				ret.setMessage(MessageFactory.getMessage("error_email_not_found", locale));
                ret.setStatusCode(400);
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setStatusCode(400);
		}
		return ret;
	}

	@Override
	@Transactional
	public MessageReturn registerNewPassword(User user) {
		MessageReturn ret = new MessageReturn();
		try {
			String decryptedToken = Crypt.decrypt(user.getToken());
			String[] tokenArray = decryptedToken.split("#");
			User dbUser = userDAO.findById(User.class, Long.valueOf(tokenArray[0]));
			if(dbUser != null){
				Calendar tokenDate = Utils.stringToDate(tokenArray[1],false);

				Calendar current = new GregorianCalendar();
				current.setTime(new Date());
				current.add(Calendar.DAY_OF_MONTH, 2);
				if(tokenDate.before(current) || current.equals(tokenDate)){
					if(dbUser.getDefaultPassword() && Crypt.encrypt(user.getOldPassword()).equals(dbUser.getOldPassword())){
						if(user.getPassword().equals(user.getConfirmPassword())){
							dbUser.setPassword(passwordEncoder.encode(user.getPassword()));
							dbUser.setDefaultPassword(false);

							dbUser = userDAO.save(dbUser);
							ret = login(dbUser.getUsername(), user.getPassword());
						}else {
							ret.setMessage(MessageFactory.getMessage("lb_password_not_match", dbUser.getLanguage()));
							ret.setStatusCode(400);
						}
					} else {
						ret.setMessage(MessageFactory.getMessage("lb_wrong_password_review_email", dbUser.getLanguage()));
						ret.setStatusCode(400);
					}
				} else {
					ret.setMessage(MessageFactory.getMessage("lb_expired_temporary_password", dbUser.getLanguage()));
					ret.setStatusCode(400);
				}
			} else {
				new Exception(MessageFactory.getMessage("lb_expired_temporary_password", dbUser.getLanguage()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setStatusCode(400);
		}
		return ret;
	}

	@Override
	public void validateEmail(String email) throws Exception{
		Map<String, String> queryParams = new LinkedHashMap<String, String>();
		queryParams.put(" where x.email", "= '" + email + "'");

		User user = findByParameter(User.class, queryParams);

		if (user != null){
			throw new Exception(MessageFactory.getMessage("lb_email_already_been_used", "en"));
		}

		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Matcher m = p.matcher(email);
		boolean matchFound = m.matches();

		if (!matchFound) {
			throw new Exception(MessageFactory.getMessage("error_invalid_email", "en"));
		}

	}

}
