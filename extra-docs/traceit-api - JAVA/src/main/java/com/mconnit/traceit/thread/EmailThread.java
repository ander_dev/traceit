package com.mconnit.traceit.thread;

import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.utils.Crypt;
import com.mconnit.traceit.utils.MessageFactory;
import com.mconnit.traceit.utils.Utils;

import java.net.URI;
import java.util.Calendar;

public class EmailThread extends Thread {

	User user;

	String emailType;

	public EmailThread(User user, String emailType) {
		this.user = user;
		this.emailType = emailType;
	}

	public void run() {
		try {
			sendEmail(user, emailType);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sendEmail(User user, String userType) throws Exception {
		String emailTo = user.getEmail();
		String emailFrom = "contact@traceit.nz";
		String nameFrom = "Traceit";
		String emailSubject = MessageFactory.getMessage("lb_email_subject", user.getLanguage());

		StringBuilder body = new StringBuilder();

		body.append(MessageFactory.getMessage("lb_email_hello", user.getLanguage())).append(" <b>").append(user.getName()).append("</b>,<br/><br/>");
		if (userType.equals("USER")) {
			nameFrom = user.getSuperUser().getName();
			if (user.getDefaultPassword() != null && user.getDefaultPassword()) {

				body.append("<b>").append(user.getSuperUser().getName()).append("</b>").append(MessageFactory.getMessage("lb_email_first_line", user.getLanguage())).append("<br/><br/>");
				body.append(MessageFactory.getMessage("lb_email_second_line", user.getLanguage())).append(" <b>").append("</b><br/><br/>");

				body.append(MessageFactory.getMessage("lb_email_first_login", user.getLanguage())).append("<br/><br/>");
				body.append(MessageFactory.getMessage("lb_email_user", user.getLanguage())).append(" <b>").append(user.getUsername()).append("</b><br/>");
				body.append(MessageFactory.getMessage("lb_email_password", user.getLanguage())).append(" <b>").append(Crypt.decrypt(user.getPassword())).append("</b><br/><br/>");
				body.append(MessageFactory.getMessage("lb_email_link", user.getLanguage())).append(" <b>").append("<a href='"+user.getAuthenticationUrl()+"' target'_blank'>www.traceit.nz</a>").append("</b><br/><br/>");
				body.append(MessageFactory.getMessage("lb_email_next_page", user.getLanguage())).append(" <b>").append("</b><br/><br/><br/>");
			}
		} else if (userType.equals("SUPER-USER")) {
			body.append(MessageFactory.getMessage("lb_email_confirmation", user.getLanguage())).append("<br/><br/>");
			Calendar registerDate = Calendar.getInstance();
			registerDate.setTime(user.getRegister());
			String date = Utils.dateToString(registerDate);
			String token = Crypt.encrypt(user.getId() + "#" + date);
			String tokenURL = user.getAuthenticationUrl() + "/" + token;
			body.append(MessageFactory.getMessage("lb_email_link", user.getLanguage())).append(" <b>").append("<a href=" + tokenURL + ">" + tokenURL + "</a>").append("</b><br/><br/>");
		} else if (userType.equals("RESET-PASSWORD")) {
			Calendar registerDate = Calendar.getInstance();
			registerDate.setTime(user.getRegister());
			String date = Utils.dateToString(registerDate);
			String token = Crypt.encrypt(user.getId() + "#" + date);
			String[] names = user.getName().split(" ");
			URI confirmURL = URI.create(user.getAuthenticationUrl()+"/"+names[0]+"/"+token);
			emailSubject = MessageFactory.getMessage("lb_email_subject_reset_password", user.getLanguage());

			body.append(MessageFactory.getMessage("lb_email_reset_password_first_line", user.getLanguage())).append("<br/><br/>");
			body.append(MessageFactory.getMessage("lb_email_reset_password_second_line", user.getLanguage())).append(" <b>").append("</b><br/><br/>");

			body.append(MessageFactory.getMessage("lb_email_user", user.getLanguage())).append(" <b>").append(user.getUsername()).append("</b><br/>");
			body.append(MessageFactory.getMessage("lb_email_password", user.getLanguage())).append(" <b>").append(Crypt.decrypt(user.getPassword())).append("</b><br/><br/>");
			body.append(MessageFactory.getMessage("lb_email_link", user.getLanguage())).append(" <b>").append("<a href='"+confirmURL+"' target'_blank'>"+confirmURL.getHost()+"</a>").append("</b><br/><br/>");
			body.append(MessageFactory.getMessage("lb_email_next_page", user.getLanguage())).append(" <b>").append("</b><br/><br/><br/>");
		}

		body.append(MessageFactory.getMessage("lb_email_thanks", user.getLanguage())).append("<br/><br/>");
		body.append("<b>").append(MessageFactory.getMessage("lb_email_signature", user.getLanguage())).append("</b>");

		Utils.sendGridEmail(emailTo, emailFrom, nameFrom, emailSubject, body.toString());
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

}
