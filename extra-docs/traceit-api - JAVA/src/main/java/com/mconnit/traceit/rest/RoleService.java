package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.RoleBO;
import com.mconnit.traceit.entity.Role;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/role")
public class RoleService {

	@Autowired
	private RoleBO roleBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Role> listRole() {

		List<Role> list = new ArrayList<>();
		try {
			list = roleBO.list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn saveRole(@RequestBody Role role) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = roleBO.save(role);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.DELETE, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn deleteRole(@RequestBody Role role) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = roleBO.delete(role.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
