package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.ClosureBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.Closure;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@RestController
@RequestMapping(value = "/rest/closure")
@CrossOrigin(origins = "http://localhost:3000")
public class ClosureService {

	@Autowired
	private ClosureBO closureBO;

	@Autowired
	private UserBO userBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{userId}/{timeInMillis}", method = RequestMethod.GET, headers = "Accept=application/json")
	@ResponseBody
	public MessageReturn closeAccount(@PathVariable Long userId, @PathVariable Long timeInMillis ) {
		MessageReturn ret = new MessageReturn();
		try {
			User user = userBO.getById(userId);
			Calendar date = Utils.timemillisToDate(timeInMillis);
			date.setTimeZone(TimeZone.getTimeZone(user.getGmt()));
			closureBO.closeValuesByUserAndDate(user, date);
			ret.setMessage("Successfully Executed!");
		} catch (Exception e) {
			e.printStackTrace();
			ret.setMessage(e.getMessage());
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/all", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Closure> listClosure(@RequestBody Closure closure) {

		List<Closure> list = new ArrayList<>();
		try {
			list = closureBO.list(closure.getAccount().getUser());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/list", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Closure> listClosureByParameter(@RequestBody Closure closure) {

		List<Closure> list = new ArrayList<>();
		try {
			list = closureBO.listByParameter(closure);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	// @Produces({ MediaType.APPLICATION_JSON })
	// @Consumes({ MediaType.APPLICATION_JSON })
	// @RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
	// public @ResponseBody MessageReturn executeClosure(@RequestBody Closure closure) {
	// MessageReturn ret = new MessageReturn();
	// try {
	// ret = closureBO.getValuesToClose(closure);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return ret;
	// }

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn saveClosure(@RequestBody Closure closure) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = closureBO.save(closure);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.DELETE, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn deleteClosure(@RequestBody Closure closure) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = closureBO.delete(closure.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
