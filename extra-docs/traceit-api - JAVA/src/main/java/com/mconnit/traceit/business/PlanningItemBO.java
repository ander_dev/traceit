package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.PlanningItem;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("planningItemBO")
public interface PlanningItemBO extends GenericBO<PlanningItem> {

	List<PlanningItem> list() throws Exception;

	MessageReturn save(final PlanningItem plannnigItem) throws Exception;

	PlanningItem getById(Long id);
}

