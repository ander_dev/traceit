package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.TypeAccount;
import com.mconnit.traceit.persistence.TypeAccountDAO;
import org.springframework.stereotype.Repository;

@Repository("typeAccountDAO")
public class TypeAccountDAOImpl extends GenericDAOImpl<TypeAccount> implements TypeAccountDAO {


}
