package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.PlanningGroup;
import com.mconnit.traceit.persistence.PlanningGroupDAO;
import org.springframework.stereotype.Repository;

@Repository("planningGroupDAO")
public class PlanningGroupDAOImpl extends GenericDAOImpl<PlanningGroup> implements PlanningGroupDAO {


}
