package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/rest/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserService {

	@Autowired
	private UserBO userBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<User> list() {

		List<User> list = new ArrayList<>();
		try {
			list = userBO.list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<User> listByUser(@PathVariable Long id) {

		List<User> list = new ArrayList<>();
		try {
			list = userBO.listByUser(id);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/validatenewuser/{token}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	ResponseEntity<?> validateNewUser(@PathVariable String token) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = userBO.validateNewUser(token);
			ret.setStatusCode(200);
		} catch (Exception e) {
			ret.setStatusCode(412);
			ret.setException(true);
		}

		return new ResponseEntity<>(ret, HttpStatus.valueOf(ret.getStatusCode()));
	}

	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	ResponseEntity<?> resetPassword(@FormParam("email") String email, @FormParam("locale") String locale,  @FormParam("authenticationUrl") String authenticationUrl) {
		MessageReturn ret = new MessageReturn();
		try {
			if (locale.equals("pt_BR") || locale.equals("pt")) {
				locale = "pt_br";
			} else {
				locale = "en";
			}
			ret = userBO.resetPassword(email, locale, authenticationUrl);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>(ret, HttpStatus.valueOf(ret.getStatusCode()));
	}

	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/newpassword", method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	ResponseEntity<?> registerNewPassword(@RequestBody User user) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = userBO.registerNewPassword(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ResponseEntity<>(ret, HttpStatus.valueOf(ret.getStatusCode()));
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	ResponseEntity<?> save(@RequestBody User user) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = userBO.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(ret, HttpStatus.valueOf(ret.getStatusCode()));
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn delete(@RequestBody ArrayList<Long> ids) {
		MessageReturn ret = new MessageReturn();
		try {
			for (Long id : ids) {
				ret = userBO.delete(id);
			}
		} catch (Exception e) {
			ret.setMessage(e.getMessage());
		}
		return ret;
	}

	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/login", method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	ResponseEntity<?> login(@FormParam("username") String username, @FormParam("password") String password) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = userBO.login(username, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(ret, HttpStatus.valueOf(ret.getStatusCode()));
	}

	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/validate-email", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	ResponseEntity<?> validateEmail(@PathVariable String email) {
		MessageReturn ret = new MessageReturn();
		try {
			userBO.validateEmail(email);
			ret.setStatusCode(200);
		} catch (Exception e) {
			ret.setStatusCode(412);
			ret.setException(true);
		}
		return new ResponseEntity<>(ret, HttpStatus.valueOf(ret.getStatusCode()));
	}

	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/username-check", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn usernameCheck(@RequestBody User user) {
		MessageReturn ret = new MessageReturn();
		try {
			Map<String, String> queryParams = new LinkedHashMap<String, String>();
			queryParams.put(" where x.username", "= '" + user.getUsername() + "'");

			ret.setUser(userBO.findByParameter(User.class, queryParams));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/validatetoken", method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
    ResponseEntity<?>  loginByToken(@FormParam("token") String token) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = userBO.validateToken(token);
		} catch (Exception e) {
			e.printStackTrace();
            ret.setMessage(e.getMessage());
            ret.setStatusCode(400);
		}
        return new ResponseEntity<>(ret, HttpStatus.valueOf(ret.getStatusCode()));
	}
}
