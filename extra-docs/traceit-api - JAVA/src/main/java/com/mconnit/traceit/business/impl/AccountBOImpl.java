package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.AccountBO;
import com.mconnit.traceit.business.ClosureBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.Account;
import com.mconnit.traceit.entity.Config;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.entity.xml.TransferBtAccounts;
import com.mconnit.traceit.persistence.ConfigDAO;
import com.mconnit.traceit.utils.MessageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class AccountBOImpl extends GenericBOImpl<Account> implements AccountBO {

	@Autowired
	private UserBO userBO;

	@Autowired
	private ClosureBO closureBO;

	@Autowired
	private ConfigDAO configDAO;

	private User getUser(Account account) {
		return userBO.getSuperUser(account.getUser());
	}

	@Override
	@Transactional
	public MessageReturn save(Account account) {
		MessageReturn libReturn = new MessageReturn();
		User user = getUser(account);
		if (user != null) {
			try {
				account.setUser(getUser(account));

				if (account.getId() == null) {
					account.setStartDate(new Date());
				} else if (account.getTotalGeneral().equals(BigDecimal.ZERO)) {
					account.setTotalCredit(BigDecimal.ZERO);
					account.setTotalDebit(BigDecimal.ZERO);
				}

				account.setUpdatedBy(account.getUser().getUsername());
				account = saveGeneric(account);

				if (account.getDefaultAccount() != null && account.getDefaultAccount()) {
					Map<String, String> queryParams = new LinkedHashMap<>();
					queryParams.put(" where x.user = ", account.getUser().getId() + "");
					Config config = configDAO.findByParameter(Config.class, queryParams);

					if (config == null) {
						config = new Config();
						config.setUser(account.getUser());
					}

					config.setAccount(account);
					config.setUpdatedBy(account.getUser().getUsername());
					configDAO.save(config);
				}

			} catch (Exception e) {
				e.printStackTrace();
				libReturn.setMessage(e.getMessage());
			}
			if (libReturn.getMessage() == null && account.getId() == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_account_saved", user.getLanguage()));
				libReturn.setAccount(account);
			} else if (libReturn.getMessage() == null && account.getId() != null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_account_updated", user.getLanguage()));
				libReturn.setAccount(account);
			}
		} else {
			libReturn.setMessage(MessageFactory.getMessage("lb_account_not_found", "en"));
		}

		return libReturn;
	}

	public List<Account> list() throws Exception {
		return list(Account.class, null, null);
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		Account account;
		try {
			account = findById(Account.class, id);
			if (account == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_account_not_found", "en"));
			} else {
				String locale = account.getUser().getLanguage();
				remove(account);
				libReturn.setMessage(MessageFactory.getMessage("lb_account_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO add error message more specific
			libReturn.setException(Boolean.TRUE);
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public Account getById(Long id) {
		try {
			return findById(Account.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Transactional
	public MessageReturn transferBtAccounts(TransferBtAccounts transferBtAccounts) throws Exception {
		MessageReturn ret = new MessageReturn();
		Account from = getById(transferBtAccounts.getAccountFrom().getId());
		Account to = getById(transferBtAccounts.getAccountTo().getId());
		BigDecimal amount = transferBtAccounts.getAmmount();

		try {
			if (from.getTotalGeneral().floatValue() > amount.floatValue()) {
				from.setTotalGeneral(from.getTotalGeneral().subtract(amount));
				save(from);

				to.setTotalGeneral(to.getTotalGeneral().add(amount));
				save(to);
				ret.setAccount(from);
				ret.setMessage(MessageFactory.getMessage("lb_account_transfer_successful", from.getUser().getLanguage()));
			} else {
				ret.setMessage(MessageFactory.getMessage("lb_account_not_enough_funds", from.getUser().getLanguage()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			ret.setMessage(e.getMessage());
		}
		return ret;
	}

	@Override
	public List<Account> listByParameter(Account account) throws Exception {
		User user = getUser(account);
		// TODO add error message to the bundle
		// in case of user not informed

		account.setUser(user);

		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where ", " 1=1 ");
		queryParams.put(" and x.user = ", account.getUser().getId() + "");

		return list(Account.class, queryParams, "x.startDate");
	}

}
