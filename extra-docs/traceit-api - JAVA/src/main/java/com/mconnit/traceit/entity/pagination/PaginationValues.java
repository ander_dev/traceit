package com.mconnit.traceit.entity.pagination;

import java.util.Map;

/**
 * Created by anderson on 3/12/15.
 */
public class PaginationValues {

	private int startingAt;
	private int maxPerPage;
	private int rowCount;
	private String sortField;
	private String sortOrder;
	private Map<String, Object> filters;

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public int getStartingAt() {
		return startingAt;
	}

	public void setStartingAt(int startingAt) {
		this.startingAt = startingAt;
	}

	public int getMaxPerPage() {
		return maxPerPage;
	}

	public void setMaxPerPage(int maxPerPage) {
		this.maxPerPage = maxPerPage;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Map<String, Object> getFilters() {
		return filters;
	}

	public void setFilters(Map<String, Object> filters) {
		this.filters = filters;
	}
}
