package com.mconnit.traceit.entity.xml;

import java.math.BigDecimal;

/**
 * Created by anderson on 10/12/15.
 */
public class PaginationResults {
	private BigDecimal totalCredit;
	private BigDecimal totalDebit;
	private BigDecimal totalPagedCredit;
	private BigDecimal totalPagedDebit;
	private BigDecimal total;

	public BigDecimal getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	public BigDecimal getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}

	public BigDecimal getTotalPagedCredit() {
		return totalPagedCredit;
	}

	public void setTotalPagedCredit(BigDecimal totalPagedCredit) {
		this.totalPagedCredit = totalPagedCredit;
	}

	public BigDecimal getTotalPagedDebit() {
		return totalPagedDebit;
	}

	public void setTotalPagedDebit(BigDecimal totalPagedDebit) {
		this.totalPagedDebit = totalPagedDebit;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}
