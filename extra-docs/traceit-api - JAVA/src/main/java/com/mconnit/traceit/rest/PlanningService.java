package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.PlanningBO;
import com.mconnit.traceit.business.PlanningGroupBO;
import com.mconnit.traceit.entity.Planning;
import com.mconnit.traceit.entity.PlanningGroup;
import com.mconnit.traceit.entity.PlanningItem;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class PlanningService {

	@Autowired
	private PlanningBO planningBO;

	@Autowired
	private PlanningGroupBO planningGroupBO;

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/rest/planning/list", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Planning> listPlanning(@RequestBody Planning planning) {
		List<Planning> list = new ArrayList<>();
		try {
			list = planningBO.list(planning);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/rest/planning/selected", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	Planning getSelected(@RequestBody Planning planning) {
		try {
			planning = planningBO.getSelectedPlanning(planning.getUser());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return planning;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/rest/planning", method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn savePlanning(@RequestBody Planning planning) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = planningBO.save(planning);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/rest/planninggroup", method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn savePlanningGroup(@RequestBody PlanningGroup planningGroup) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = planningBO.saveGroup(planningGroup);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/rest/planningitem", method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn savePlanningItem(@RequestBody PlanningItem planningItem) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = planningBO.saveItem(planningItem);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/rest/planning/deletegroup", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn deletePlanningGroup(@RequestBody PlanningGroup planningGroup) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = planningGroupBO.delete(planningGroup.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/rest/planning", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn deletePlanning(@RequestBody Planning planning) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = planningBO.deletePlanning(planning);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

}
