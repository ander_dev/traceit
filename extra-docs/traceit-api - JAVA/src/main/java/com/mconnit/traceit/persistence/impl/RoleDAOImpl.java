package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Role;
import com.mconnit.traceit.persistence.RoleDAO;
import org.springframework.stereotype.Repository;

@Repository("roleDAO")
public class RoleDAOImpl extends GenericDAOImpl<Role> implements RoleDAO {


}
