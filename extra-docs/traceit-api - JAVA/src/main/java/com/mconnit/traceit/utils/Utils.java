package com.mconnit.traceit.utils;

import com.sendgrid.SendGrid;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public abstract class Utils {

	public static int setBooleanValue(Boolean value) {
		if (value) {
			return 1;
		} else {
			return 0;
		}
	}

	public static Boolean isCurrentMonth(Date date) {
		SimpleDateFormat sdfMonth = new SimpleDateFormat("MM/YYYY");
		String chartMonth = sdfMonth.format(date);
		String currentMont = sdfMonth.format(new Date());

		return chartMonth.equals(currentMont);
	}

	public static Boolean areDatesAMonthApart(Calendar lastDate, Calendar followingDate){
		int ret = lastDate.compareTo(followingDate);
		if(ret < 0){
			int daysOfCurrentMonth = followingDate.getActualMaximum((Calendar.DAY_OF_MONTH));

			return isFuturedDate(lastDate, followingDate, daysOfCurrentMonth);
		}
		return false;
	}

	public static  Boolean areDatesFortnightlyApart(Calendar lastDate, Calendar followingDate){
		int ret = lastDate.compareTo(followingDate);
		if(ret < 0){
			return isFuturedDate(lastDate, followingDate, 14);
		}
		return false;
	}

	public static  Boolean areDatesWeekApart(Calendar lastDate, Calendar followingDate){
		int ret = lastDate.compareTo(followingDate);
		if(ret < 0){
			return isFuturedDate(lastDate, followingDate, 7);
		}
		return false;
	}

	public static  Boolean areDatesDayApart(Calendar lastDate, Calendar followingDate){
		int ret = lastDate.compareTo(followingDate);
		if(ret < 0){
			return isFuturedDate(lastDate, followingDate, 1);
		}
		return false;
	}

	private static boolean isFuturedDate(Calendar lastDate, Calendar followingDate, int value) {
		long timeApart = followingDate.getTime().getTime() - lastDate.getTime().getTime();
		long daysApart = TimeUnit.DAYS.convert(timeApart, TimeUnit.MILLISECONDS);

		if(value == daysApart || daysApart > value){
			return true;
		}
		return false;
	}

	public static Boolean isEndOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);


		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

		SimpleDateFormat currentDay = new SimpleDateFormat("dd");
		String strCurrentDay = currentDay.format(calendar.getTime());

		if(maxDay == Integer.parseInt(strCurrentDay)){
			return true;
		}
		return false;
	}

	public static Boolean setBooleanFromString(String value) {
		return value != null && "true".equals(value);
	}

	public static Calendar timemillisToDate(Long timemillis){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(timemillis);
		return calendar;
	}

	public static Calendar stringToDate(String data, Boolean mostraHora) {

		String[] date2 = data.split("/");
		Calendar calendar = new GregorianCalendar();

		if (!mostraHora) {
			calendar.set(Integer.valueOf(date2[2]), Integer.valueOf(date2[1]) - 1, Integer.valueOf(date2[0]), 00, 00, 00);
		} else {
			calendar.set(Integer.valueOf(date2[2]), Integer.valueOf(date2[1]) - 1, Integer.valueOf(date2[0]));
		}
		return calendar;
	}

	public static String dateToStringMySQL(Calendar dtData) {

		Calendar calendar = dtData;
		SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
		String ret = sdf.format(calendar.getTime());

		return ret;
	}

	public static String dateToString(Calendar dtData) {

		Calendar calendar = dtData;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String ret = sdf.format(calendar.getTime());

		return ret;
	}

	public static String dateToStringMonthYear(Date dtData) {
		SimpleDateFormat DtFormat = new SimpleDateFormat("MM/yyyy");
		return (dtData == null || dtData.equals("")) ? "" : DtFormat.format(dtData);
	}

	public static Calendar getCreditCardExpiredDate(String jsonString) {
		String strDate = "01/" + jsonString;

		String[] tmpDate = strDate.split("/");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Integer.valueOf(tmpDate[2]), Integer.valueOf(tmpDate[1]) - 1, Integer.valueOf(tmpDate[0]), 00, 00, 00);

		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);

		return calendar;
	}

	public static Integer getLastDayOfMonth(Date date) {
		Calendar testCalendar = Calendar.getInstance();
		testCalendar.setTime(date);
		testCalendar.add(Calendar.DAY_OF_MONTH, 1);

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		if (testCalendar.get(Calendar.DAY_OF_MONTH) == 1) {
			calendar.add(Calendar.MONTH, 1);
		} else {
			calendar.add(Calendar.MONTH, 0);
		}

		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);

		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	public static HashMap<String, String> loadDates(Date date, Integer type, Integer days, Date forceStartDate) {
		HashMap<String, String> map = new HashMap<String, String>();

		Calendar calendar = Calendar.getInstance();
		if (forceStartDate != null) {
			calendar.setTime(forceStartDate);
			calendar.add(type, 1);
		} else {
			calendar.setTime(date);
			calendar.add(type, days + 1);
		}

		SimpleDateFormat dataFormatada = new SimpleDateFormat("dd/MM/yyyy");
		String startDate = dataFormatada.format(calendar.getTime());
		calendar.setTime(date);
		String endDate = dataFormatada.format(calendar.getTime());

		map.put(Constants.DATE_START, startDate);
		map.put(Constants.DATE_END, endDate);
		return map;
	}

	public static void sendEmail(final String emailTo, final String emailFrom, final String nameFrom, final String subject, final String body) throws UnsupportedEncodingException, MessagingException {
		try {
			Properties props = System.getProperties();
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");

			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("ander.dev@gmail.com", "errlqakquvklgeuf");
				}
			};

			Session session = Session.getInstance(props, auth);
			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(emailFrom, "Traceit"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
			StringBuilder sb = new StringBuilder();

			sb.append("Email sent by: ").append(nameFrom + " &lt;" + emailFrom + "&gt;");
			sb.append("<br/><br/> " + body);

			message.setSubject(subject, "UTF-8");
			message.setContent(sb.toString(), "text/HTML; charset=UTF-8");


			Transport.send(message);
		} catch (Exception e) {
			throw new UnsupportedEncodingException(e.getMessage());
		}
	}

	public static void sendGridEmail(final String emailTo, final String emailFrom, final String nameFrom, final String subject, final String body) {
		SendGrid sendgrid = new SendGrid("traceitmail", "@Tracepass79");

		SendGrid.Email email = new SendGrid.Email();
		email.addTo(emailTo);
		email.setFrom(emailFrom);
		email.setFromName("Traceit");
		email.setSubject(subject);
		email.setHtml(body);

		try {
			SendGrid.Response response = sendgrid.send(email);
			System.out.println(response.getMessage());
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public static Calendar addDaysToADate(Calendar baseDate, int daysToAdd) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(baseDate.getTime());
		calendar.add(Calendar.DAY_OF_YEAR, daysToAdd);
		return calendar;
	}

	public static String trimBeginAndEnd(final String value) {
		final StringBuilder sb = new StringBuilder(value);
		while (sb.length() > 0 && Character.isWhitespace(sb.charAt(0)))
			sb.deleteCharAt(0); // delete from the beginning
		while (sb.length() > 0 && Character.isWhitespace(sb.charAt(sb.length() - 1)))
			sb.deleteCharAt(sb.length() - 1); // delete from the end
		return sb.toString();
	}
}
