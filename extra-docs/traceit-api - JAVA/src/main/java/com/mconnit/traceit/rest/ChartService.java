package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.ChartBO;
import com.mconnit.traceit.entity.Chart;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/chart")
@CrossOrigin(origins = "http://localhost:3000")
public class ChartService {

	@Autowired
	private ChartBO chartBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/list/{userId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Chart> listByUser(@PathVariable("userId") Long userId) {

		List<Chart> list = new ArrayList<>();
		try {
			list = chartBO.listByUser(userId,null,null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/list/visible/{userId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Chart> listVisibleChartsByUser(@PathVariable("userId") Long userId) {

		List<Chart> list = new ArrayList<>();
		try {
			String orderBy = " x.paid asc, x.description.description";
			list = chartBO.listByUser(userId, Boolean.TRUE,orderBy);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	Chart getById(@PathVariable Long id) {
		Chart ret = new Chart();
		try {
			ret = chartBO.getById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn save(@RequestBody Chart chart) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = chartBO.save(chart);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn delete(@RequestBody ArrayList<Long> ids) {
		MessageReturn ret = new MessageReturn();
		try {
			for (Long id : ids) {
				ret = chartBO.delete(id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
