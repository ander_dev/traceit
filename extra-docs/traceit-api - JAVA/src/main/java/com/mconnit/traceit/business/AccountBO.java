package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Account;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.entity.xml.TransferBtAccounts;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("accountBO")
public interface AccountBO extends GenericBO<Account> {

	List<Account> list() throws Exception;

	List<Account> listByParameter(Account account) throws Exception;

	MessageReturn save(final Account account) throws Exception;

	MessageReturn delete(Long id);

	Account getById(Long id);

	MessageReturn transferBtAccounts(final TransferBtAccounts transferBtAccounts) throws Exception;
}

