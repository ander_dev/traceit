package com.mconnit.traceit.entity.xml;

import com.mconnit.traceit.entity.*;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "MessageReturn")
public class MessageReturn {

	private String message;

	private Integer statusCode;

	private Boolean exception;

	private User user;

	private Currency currency;

	private Account account;

	private TypeAccount typeAccount;

	private TypeClosure typeClosure;

	private Description description;

	private CreditCard creditCard;

	private Config config;

	private Register register;

	private Closure closure;

	private Planning planning;

	private PlanningGroup planningGroup;

	private PlanningItem planningItem;

	private Role role;

	private TokenTransfer tokenTransfer;

	private Chart chart;

	private ListResults listResults;

	public void setException(Boolean exception) {
		this.exception = exception;
	}

	public Boolean getException() {
		return exception;
	}

	public Chart getChart() {
		return chart;
	}

	public void setChart(Chart chart) {
		this.chart = chart;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public TypeAccount getTypeAccount() {
		return typeAccount;
	}

	public void setTypeAccount(TypeAccount typeAccount) {
		this.typeAccount = typeAccount;
	}

	public TypeClosure getTypeClosure() {
		return typeClosure;
	}

	public void setTypeClosure(TypeClosure typeClosure) {
		this.typeClosure = typeClosure;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public Config getConfig() {
		return config;
	}

	public void setConfig(Config config) {
		this.config = config;
	}

	public Register getRegister() {
		return register;
	}

	public void setRegister(Register register) {
		this.register = register;
	}

	public Closure getClosure() {
		return closure;
	}

	public void setClosure(Closure closure) {
		this.closure = closure;
	}

	public Planning getPlanning() {
		return planning;
	}

	public void setPlanning(Planning planning) {
		this.planning = planning;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public PlanningItem getPlanningItem() {
		return planningItem;
	}

	public void setPlanningItem(PlanningItem planningItem) {
		this.planningItem = planningItem;
	}

	public PlanningGroup getPlanningGroup() {
		return planningGroup;
	}

	public void setPlanningGroup(PlanningGroup planningGroup) {
		this.planningGroup = planningGroup;
	}

	public TokenTransfer getTokenTransfer() {
		return tokenTransfer;
	}

	public void setTokenTransfer(TokenTransfer tokenTransfer) {
		this.tokenTransfer = tokenTransfer;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public ListResults getListResults() {
		return listResults;
	}

	public void setListResults(ListResults listResults) {
		this.listResults = listResults;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
}
