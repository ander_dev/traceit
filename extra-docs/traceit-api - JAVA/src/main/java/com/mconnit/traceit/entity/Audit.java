package com.mconnit.traceit.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public class Audit {

	@Column(name = "updatedby")
	private String updatedBy;

	@Column(name = "updatedwhen")
	private Date updatedWhen;

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedWhen() {
		return updatedWhen;
	}

	public void setUpdatedWhen(Date updatedWhen) {
		this.updatedWhen = updatedWhen;
	}

}
