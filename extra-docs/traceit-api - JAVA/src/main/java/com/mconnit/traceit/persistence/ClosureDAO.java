package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Account;
import com.mconnit.traceit.entity.Closure;
import com.mconnit.traceit.entity.Register;

import java.util.Collection;

public interface ClosureDAO extends GenericDAO<Closure> {

	Collection<Register> getRegisters(Account account, String startDate, String endDate, boolean closed, Long typeAccountId);

}
