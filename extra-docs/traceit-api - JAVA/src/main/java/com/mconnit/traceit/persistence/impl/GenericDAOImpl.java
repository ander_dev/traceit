package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.exception.TraceitSoftException;
import com.mconnit.traceit.persistence.GenericDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Repository("genericDAO")
public class GenericDAOImpl<T> implements GenericDAO<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenericDAOImpl.class);

	@PersistenceContext(unitName = "traceit_PU")
	protected EntityManager em;

	@SuppressWarnings("hiding")
	public <T> T findById(final Class<T> type, final Long id) throws Exception {
		T obj = null;
		try {
			obj = em.find(type, id);
		} catch (NoResultException nre) {
			obj = null;
		} catch (RuntimeException e) {
			throw new Exception(e.getMessage(), e);
		}
		return obj;
	}

	@SuppressWarnings({"unchecked", "hiding"})
	public <T> T findByParameter(final Class<T> type, Map<String, String> queryParams) throws Exception {
		T obj = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append(" select x from ");
			sql.append(type.getSimpleName());
			sql.append(" x ");

			Iterator<Entry<String, String>> iterator = queryParams.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, String> element = iterator.next();
				sql.append(element.getKey()).append(element.getValue());
			}

			LOGGER.debug("Query: {}", sql.toString());

			System.out.println("Query: "+sql.toString());
			Query query = em.createQuery(sql.toString());
			obj = (T) query.getSingleResult();
		} catch (NoResultException nre) {
			obj = null;
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
		return obj;
	}

	public T save(T obj) throws Exception {
		try {
			obj = em.merge(obj);
			em.flush();

		} catch (Throwable e) {
			throw new Exception(e.getMessage(), e);
		}
		return obj;
	}

	public Boolean remove(T obj) throws Exception {
		try {
			Field field = obj.getClass().getDeclaredField("id");
			field.setAccessible(true);
			Object value = field.get(obj);
			em.remove(em.find(obj.getClass(), value));
			em.flush();
			return true;
		} catch (PersistenceException cv) {
			throw new TraceitSoftException(cv.getMessage());
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}

	public T merge(T obj) {
		return em.merge(obj);
	}

	/**
	 * Lista objetos da classe informada no primeiro parametro, retornando um lista paginada, caso queira esta lista ordenada, informe o segundo parametro como true e preencha os outros parametros de acordo com a ordenação desejada, caso contrario deixe o orderBy como NULL.
	 *
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> listPaginated(final Class<T> type, int startRow, int pageSize, Map<String, String> queryParams, String orderByField) throws Exception {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" select x from ");
			sql.append(type.getSimpleName());
			sql.append(" x ");

			if (queryParams != null) {
				sql.append(buildWhereClause(queryParams));
			}

			if (orderByField != null && !orderByField.isEmpty()) {
				sql.append(" order by ").append(orderByField);
			}

			LOGGER.debug("Query: %s ", sql.toString());

			Query query = em.createQuery(sql.toString());
			query.setFirstResult(startRow);
			query.setMaxResults(pageSize);

			return query.getResultList();
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
	}

	private String buildWhereClause(Map<String, String> queryParams) {
		StringBuilder sql = new StringBuilder();
		Iterator<Entry<String, String>> iterator = queryParams.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, String> element = iterator.next();
			sql.append(element.getKey()).append(" " + element.getValue());
		}
		return sql.toString();
	}

	/**
	 * Lista objetos da classe informada no primeiro parametro, retornando um lista n�o paginada, caso queira esta lista ordenada, informe o segundo parametro como true e preencha os outros parametros de acordo com a ordena��o desejada, caso contrario deixe o 2� e outros parametros como null.
	 *
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> list(final Class<T> type, Map<String, String> queryParams, String orderByField) throws Exception {
		try {

			StringBuilder sql = new StringBuilder();
			sql.append(" select x from ");
			sql.append(type.getSimpleName());
			sql.append(" x ");

			if (queryParams != null) {
				sql.append(buildWhereClause(queryParams));
			}

			if (orderByField != null && !orderByField.isEmpty()) {
				sql.append(" order by ").append(orderByField);
			}

			LOGGER.debug("Query: %s ", sql.toString());

			Query query = em.createQuery(sql.toString());
			return query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage(), e);
		}
	}

	public Integer count(final Class<T> type, Map<String, String> queryParams) throws Exception {
		Long ret = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append(" select count(x) from ");
			sql.append(type.getSimpleName());
			sql.append(" x ");

			String where = buildWhereClause(queryParams);
			sql.append(where);

			LOGGER.debug("Query: %s ", sql.toString());

			Query query = em.createQuery(sql.toString());
			ret = (Long) query.getSingleResult();
		} catch (Exception e) {
			throw new Exception(e.getMessage(), e);
		}
		return ret.intValue();
	}
}
