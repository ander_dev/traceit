package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.TypeClosure;

public interface TypeClosureDAO extends GenericDAO<TypeClosure> {

}
