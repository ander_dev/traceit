package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.*;
import com.mconnit.traceit.entity.*;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.persistence.PlanningDAO;
import com.mconnit.traceit.utils.MessageFactory;
import com.mconnit.traceit.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

public class PlanningBOImpl extends GenericBOImpl<Planning> implements PlanningBO {

	@Autowired
	private PlanningGroupBO planningGroupBO;

	@Autowired
	private TypeAccountBO typeAccountBO;

	@Autowired
	private PlanningItemBO planningItemBO;

	@Autowired
	private PlanningDAO planningDAO;

	@Autowired
	private UserBO userBO;

	@Autowired
	private ChartBO chartBO;

	@Autowired
	private RegisterBO registerBO;

	private User getSuperUser(User user) {
		return userBO.getSuperUser(user);
	}

	@Override
	@Transactional
	public MessageReturn save(Planning planning) {
		MessageReturn libReturn = new MessageReturn();
		if (planning.getId() != null) {
			planning.setUpdatedBy(planning.getUser().getUsername());
		}
		User user = getSuperUser(planning.getUser());
		try {
			Planning planTemp = getSelectedPlanning(user);
			if (planTemp != null) {
				planTemp.setSelected(false);
				planTemp.setUser(user);
				saveGeneric(planTemp);
			}
			saveGeneric(planning);
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setPlanning(planning);
			libReturn.setMessage(e.getMessage());
		}
		if (libReturn.getMessage() == null && planning.getId() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_saved", planning.getUser().getLanguage()));
			libReturn.setPlanning(planning);
		} else if (libReturn.getMessage() == null && planning.getId() != null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_updated", planning.getUser().getLanguage()));
			libReturn.setPlanning(planning);
		}

		return libReturn;
	}

	@Transactional
	public List<Planning> list(Planning plannig) throws Exception {
		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.user.id = ", getSuperUser(plannig.getUser()).getId() + "");
		List<Planning> result = list(Planning.class, queryParams, null);
		for (Planning planning : result) {
			for (PlanningGroup planningGroup : planning.getPlannigGroupList()) {
				if (planningGroup.getTypeAccount() != null && planningGroup.getTypeAccount().getId() != null) {
					if (planningGroup.getTypeAccount().getDescription().equals(MessageFactory.getMessage("lb_credit", planningGroup.getUser().getLanguage()))) {
						planningGroup.setIsCredit(true);
					} else {
						planningGroup.setIsCredit(false);
					}
				}
			}
		}
		return result;
	}

	@Override
	public MessageReturn deletePlanningGroup(final PlanningGroup planningGroup) {
		MessageReturn libReturn = new MessageReturn();
		try {
			String locale = planningGroup.getUser().getLanguage();
			deletePlanningItemsAndGroup(planningGroup);

			libReturn.setMessage(MessageFactory.getMessage("lb_planning_deleted", locale));
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public Planning getById(Long id) {
		try {
			return findById(Planning.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void deletePlanningItemsAndGroup(PlanningGroup planningGroup) throws Exception {
		for (PlanningItem planningItem : planningGroup.getPlannigItemList()) {
			Chart chart = planningItem.getChart();
			planningItem.setChart(null);
			planningItemBO.save(planningItem);

			if (chart != null) {
				chartBO.remove(chart);
			}

			planningItemBO.remove(planningItem);
		}
		planningGroupBO.delete(planningGroup.getId());
	}

	@Override
	@Transactional
	public MessageReturn deletePlanning(final Planning planning) {
		MessageReturn libReturn = new MessageReturn();
		try {
			String locale = planning.getUser().getLanguage();

			Map<String, String> queryParams = new LinkedHashMap<>();
			queryParams.put(" where x.planning = ", planning.getId() + "");

			List<PlanningGroup> pgList = planningGroupBO.list(PlanningGroup.class, queryParams, null);

			for (PlanningGroup planningGroup : pgList) {
				deletePlanningItemsAndGroup(planningGroup);
			}

			planningDAO.remove(planning);

			queryParams.clear();
			queryParams.put(" where x.user = ", planning.getUser().getId() + "");
			List<Planning> planList = planningDAO.list(Planning.class, queryParams, "x.id");
			for (Planning plan : planList) {
				if (!plan.getId().equals(planning.getId())) {
					plan.setSelected(true);
					Planning tempReturn = saveGeneric(plan);
					libReturn.setPlanning(tempReturn);
					break;
				}
			}

			libReturn.setMessage(MessageFactory.getMessage("lb_planning_deleted", locale));
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public Planning getSelectedPlanning(User user) throws Exception {

		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.user = ", user.getId() + "");
		queryParams.put(" and x.selected = ", " 1 ");
		return findByParameter(Planning.class, queryParams);
	}

	@Override
	@Transactional
	public MessageReturn saveGroup(PlanningGroup planningGroup) throws Exception {
		MessageReturn libReturn = new MessageReturn();
		try {
			if (planningGroup.getId() != null) {
				planningGroup.setUpdatedBy(planningGroup.getUser().getUsername());
			}
			User user = getSuperUser(planningGroup.getUser());
			planningGroup.setPlanning(getSelectedPlanning(user));
			List<PlanningItem> pItemList = createDefaultItems(user, planningGroup);

			planningGroup.getPlannigItemList().addAll(pItemList);
			TypeAccount typeAccount = null;

			if (planningGroup.getTypeAccount() != null && planningGroup.getTypeAccount().getId() != null) {
				typeAccount = findById(TypeAccount.class, planningGroup.getTypeAccount().getId());
			} else {
				List<TypeAccount> typeAccountList = typeAccountBO.list(planningGroup.getUser(), true);
				for (TypeAccount taTemp : typeAccountList) {
					if (planningGroup.getIsCredit()) {
						if (taTemp.getDescription().equals(MessageFactory.getMessage("lb_credit", planningGroup.getUser().getLanguage()))) {
							typeAccount = taTemp;
						}
					} else {
						if (taTemp.getDescription().equals(MessageFactory.getMessage("lb_debit", planningGroup.getUser().getLanguage()))) {
							typeAccount = taTemp;
						}
					}

				}
			}

			Description description = findById(Description.class, planningGroup.getDescription().getId());
			planningGroup.setUser(user);

			planningGroup.setDescription(description);
			planningGroup.setTypeAccount(typeAccount);

			libReturn = planningGroupBO.save(planningGroup);
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setPlanningGroup(planningGroup);
			libReturn.setMessage(e.getMessage());
		}
		if (libReturn.getMessage() == null && planningGroup.getId() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_saved", planningGroup.getUser().getLanguage()));
		} else if (libReturn.getMessage() == null && planningGroup.getId() != null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_updated", planningGroup.getUser().getLanguage()));
		}

		return libReturn;
	}

	@Override
	@Transactional
	public MessageReturn saveItem(PlanningItem planningItem) throws Exception {
		MessageReturn libReturn = new MessageReturn();
		try {
			PlanningItem planningItemTemp = planningItemBO.findById(PlanningItem.class, planningItem.getId());
			planningItem.setPlanningGroup(planningItemTemp.getPlanningGroup());

			Planning currentPlanning = planningItem.getPlanningGroup().getPlanning();

			User superUser = getSuperUser(planningItem.getUser());

			Map<String, String> params = new LinkedHashMap<>();

			if (planningItem.getInstalment() != null && planningItem.getInstalment().intValue() > 1) {
				PlanningGroup planningGroup = planningItem.getPlanningGroup();
				int currentMonth = planningItem.getMonth();
				for (int x = 1; x <= planningItem.getInstalment(); x++) {
					if (currentMonth > 12) {
						currentMonth = 1;
						Planning nextPlanning = null;
						/* verify how many plannings does this user has to find out nextPlanning */
						params.clear();
						params.put(" where x.user.id = ", superUser.getId() + "");
						params.put(" and x.id > ", currentPlanning.getId() + "");
						List<Planning> planningList = list(Planning.class, params, null);

						if (!planningList.isEmpty()) {
							Planning tempPlanning = null;
							for (Planning testPlanning : planningList) {
								if (tempPlanning == null || (testPlanning.getId() > planningItem.getPlanningGroup().getPlanning().getId() && testPlanning.getId() < tempPlanning.getId())) {
									tempPlanning = testPlanning;
								}
							}
							nextPlanning = tempPlanning;
						}

						if (nextPlanning == null) {
							nextPlanning = new Planning();
							int newYear = Integer.valueOf(currentPlanning.getDescription()) + 1;
							nextPlanning.setDescription(newYear + "");
							GregorianCalendar calendar = new GregorianCalendar();
							calendar.setTime(new Date());
							nextPlanning.setDate(calendar);
							nextPlanning.setSelected(Boolean.FALSE);
							nextPlanning.setUser(superUser);
							nextPlanning = saveGeneric(nextPlanning);

							planningGroup = createPlanningGroup(superUser, planningGroup, nextPlanning);
						} else {
							planningGroup = createPlanningGroup(superUser, planningGroup, nextPlanning);
						}

						currentPlanning = nextPlanning;

						params.clear();
						params.put(" where x.user.id = ", superUser.getId() + "");
						params.put(" and x.month = ", currentMonth + "");
						params.put(" and x.planningGroup.id = ", planningGroup.getId() + "");

						PlanningItem planningItemPersitence = findByParameter(PlanningItem.class, params);
						planningItemPersitence.setAmount(planningItem.getAmount());
						planningItemPersitence.setInstalment(planningItem.getInstalment());

						saveItem(planningItemPersitence, superUser);
					} else {

						params.clear();
						params.put(" where x.user.id = ", superUser.getId() + "");
						params.put(" and x.month = ", currentMonth + "");
						params.put(" and x.planningGroup.id = ", planningGroup.getId() + "");

						PlanningItem planningItemPersitence = findByParameter(PlanningItem.class, params);
						planningItemPersitence.setAmount(planningItem.getAmount());
						planningItemPersitence.setInstalment(planningItem.getInstalment());

						saveItem(planningItemPersitence, superUser);
					}
					currentMonth++;
				}
			} else {

				params.clear();
				params.put(" where x.planningItem = ", planningItem.getId() + "");
				params.put(" and x.closed = ", Utils.setBooleanValue(false)+"");
				List<Register> registers = registerBO.list(Register.class, params,null);

				MessageReturn ret;
				if(registers != null && !registers.isEmpty()){
					for (Register reg: registers) {
						ret = registerBO.save(reg);
						libReturn.setAccount(ret.getAccount());
					}
				}

				saveItem(planningItem, superUser);
			}

		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setPlanningItem(planningItem);
			libReturn.setMessage(e.getMessage());
		}
		if (libReturn.getMessage() == null && planningItem.getId() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_saved", planningItem.getUser().getLanguage()));
			libReturn.setPlanningItem(planningItem);
		} else if (libReturn.getMessage() == null && planningItem.getId() != null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_planning_updated", planningItem.getUser().getLanguage()));
			libReturn.setPlanningItem(planningItem);
		}

		return libReturn;
	}

	private PlanningGroup createPlanningGroup(User superUser, PlanningGroup planningGroup, Planning nextPlanning) throws Exception {
		PlanningGroup newPlanningGroup = new PlanningGroup();
		newPlanningGroup.setUser(superUser);
		newPlanningGroup.setPlanning(nextPlanning);
		newPlanningGroup.setDescription(planningGroup.getDescription());
		newPlanningGroup.setIsCredit(planningGroup.getIsCredit());
		newPlanningGroup.setTypeAccount(planningGroup.getTypeAccount());

		List<PlanningItem> pItemList = createDefaultItems(superUser, newPlanningGroup);

		newPlanningGroup.getPlannigItemList().addAll(pItemList);
		planningGroup = planningGroupBO.saveGeneric(newPlanningGroup);
		return planningGroup;
	}

	private List<PlanningItem> createDefaultItems(User superUser, PlanningGroup planningGroup) {
		List<PlanningItem> pItemList = new ArrayList<>();
		for (int z = 1; z < 13; z++) {
			PlanningItem pItem = new PlanningItem();
			pItem.setAmount(BigDecimal.ZERO);
			pItem.setMonth(z);
			pItem.setPlanningGroup(planningGroup);
			pItem.setUser(superUser);
			pItem.setDate(Utils.stringToDate("01/" + ((z > 9) ? z + "" : "0" + z) + "/" + planningGroup.getPlanning().getDescription(), Boolean.FALSE));
			pItemList.add(pItem);
		}
		return pItemList;
	}

	private void saveItem(PlanningItem planningItem, User superUser) throws Exception {
		if (planningItem.getId() != null) {
			planningItem.setUpdatedBy(planningItem.getUser().getUsername());
			planningItem.setUpdatedWhen(new Date());
		}

		if (planningItem.getCurrentAmount() == null) {
			planningItem.setCurrentAmount(BigDecimal.ZERO);
		}

		MessageReturn chartReturn;
		if (planningItem.getChart() == null) {
			Chart chart = new Chart();
			chart.setDescription(planningItem.getPlanningGroup().getDescription());
			chart.setUser(superUser);
			chart.setDate(planningItem.getDate().getTime());
			chart.setAmount(planningItem.getAmount());
			chart.setCurrentAmount(BigDecimal.ZERO);
			if (planningItem.getPlanningGroup().getTypeAccount().getDescription().equals(MessageFactory.getMessage("lb_credit", superUser.getLanguage()))) {
				chart.setIsCredit(true);
			} else {
				chart.setIsCredit(false);
			}
			chartReturn = chartBO.save(chart);
		} else {
			planningItem.getChart().setUser(superUser);
			planningItem.getChart().setAmount(planningItem.getAmount());
			chartReturn = chartBO.save(planningItem.getChart());
		}

		planningItem.setUser(superUser);
		planningItem.setChart(chartReturn.getChart());
		planningItemBO.save(planningItem);
	}

}
