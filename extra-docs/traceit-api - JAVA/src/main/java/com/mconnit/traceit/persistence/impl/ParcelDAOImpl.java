package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Parcel;
import com.mconnit.traceit.persistence.ParcelDAO;
import org.springframework.stereotype.Repository;

@Repository("parcelDAO")
public class ParcelDAOImpl extends GenericDAOImpl<Parcel> implements ParcelDAO {


}
