package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Chart;
import com.mconnit.traceit.persistence.ChartDAO;
import org.springframework.stereotype.Repository;

@Repository("chartDAO")
public class ChartDAOImpl extends GenericDAOImpl<Chart> implements ChartDAO {

}
