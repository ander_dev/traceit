package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.CreditCard;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("creditCardBO")
public interface CreditCardBO extends GenericBO<CreditCard> {

	List<CreditCard> list() throws Exception;

	MessageReturn save(final CreditCard creditCard) throws Exception;

	MessageReturn delete(Long id);

	CreditCard getById(Long id);

	List<CreditCard> listByParameter(String userId) throws Exception;
}

