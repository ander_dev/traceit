package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Currency;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("currencyBO")
public interface CurrencyBO extends GenericBO<Currency> {

	List<Currency> list() throws Exception;

	List<Currency> listByLocale(final String locale) throws Exception;

	MessageReturn save(final Currency currency) throws Exception;

	MessageReturn delete(Long id);

	Currency getById(Long id);
}

