package com.mconnit.traceit.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "application")
@XmlRootElement
public class Config extends Audit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	private Long id;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = Account.class)
	@JoinColumn(name = "account_id", foreignKey = @ForeignKey(name = "FK_CONFIG_ACCOUNT"))
	private Account account;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = User.class)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_CONFIG_USER"))
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}
