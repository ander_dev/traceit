package com.mconnit.traceit.entity;

import com.mconnit.traceit.utils.Crypt;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "description")
@XmlRootElement
public class Description implements Serializable {

	private static final long serialVersionUID = 7999831669893410987L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	private Long id;

	private String description;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = TypeAccount.class)
	@JoinColumn(name = "typeaccount_id", foreignKey = @ForeignKey(name = "FK_DESCRIPTION_TYPEACCOUNT"))
	private TypeAccount typeAccount;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = User.class)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_DESCRIPTION_USER"))
	private User user;

	@Transient
	@XmlAttribute
	private List<Long> typeAccountIdList;

	public TypeAccount getTypeAccount() {
		return typeAccount;
	}

	public void setTypeAccount(TypeAccount typeAccount) {
		this.typeAccount = typeAccount;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		try {
			if (description != null) {
				return Crypt.decrypt(description);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setDescription(String description) {
		try {
			if (description != null) {
				this.description = Crypt.encrypt(description);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Long> getTypeAccountIdList() {
		if (typeAccountIdList == null) {
			typeAccountIdList = new ArrayList<Long>();
		}
		return typeAccountIdList;
	}

	public void setTypeAccountIdList(List<Long> typeAccountIdList) {
		this.typeAccountIdList = typeAccountIdList;
	}

}
