package com.mconnit.traceit.utils;

public abstract class Constants {

	public static String DEFAULT_PASSWORD = "123456";

	public static Integer SINGLE_PARCEL = 1;

	public static String MENSAL = "mensal";

	public static String SEMANAL = "semanal";

	public static String QUINZENAL = "quinzenal";

	public static String DIARIO = "diario";

	public static String ANUAL = "anual";

	public static String MONTHLY = "monthly";

	public static String WEEKLY = "weekly";

	public static String FORTNIGHTLY = "fortnightly";

	public static String DAILY = "daily";

	public static String YEARLY = "yearly";

	public static String DATE_START = "startDate";

	public static String DATE_END = "endDate";

	public static String ROLE_ADMIN = "ADMIN";

	public static String ROLE_SUPER_USER = "SUPER_USER";

	public static String ROLE_USER = "USER";
}
