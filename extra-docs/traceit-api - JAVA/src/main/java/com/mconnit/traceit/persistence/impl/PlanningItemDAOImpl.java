package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.PlanningItem;
import com.mconnit.traceit.persistence.PlanningItemDAO;
import org.springframework.stereotype.Repository;

@Repository("planningItemDAO")
public class PlanningItemDAOImpl extends GenericDAOImpl<PlanningItem> implements PlanningItemDAO {

}
