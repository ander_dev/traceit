package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.TypeAccountBO;
import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.entity.TypeAccount;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/typeaccount")
@CrossOrigin(origins = "http://localhost:3000")
public class TypeAccountService {

	@Autowired
	private TypeAccountBO typeAccountBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	List<TypeAccount> listByUserLocale(@RequestBody User user) {

		List<TypeAccount> list = new ArrayList<>();
		try {
			list = typeAccountBO.list(user, null);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/getbylocaleshowtype", method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	List<TypeAccount> listAccountByLocaleAndShowType(@FormParam("user") User user, @FormParam("showType") Boolean showType) {

		List<TypeAccount> list = new ArrayList<>();
		try {
			list = typeAccountBO.list(user, showType);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/getbydescription", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn getByDescription(@RequestBody Description description) {
		MessageReturn ret = new MessageReturn();
		try {
			TypeAccount typeAccount = typeAccountBO.getByDescription(description);
			ret.setTypeAccount(typeAccount);
		} catch (Exception e) {
			ret.setMessage(e.getMessage());
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn save(@RequestBody TypeAccount account) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = typeAccountBO.save(account);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.DELETE, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn delete(@RequestBody TypeAccount account) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = typeAccountBO.delete(account.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
