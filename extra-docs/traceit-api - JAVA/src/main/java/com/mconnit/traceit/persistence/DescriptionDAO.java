package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Description;

public interface DescriptionDAO extends GenericDAO<Description> {

}
