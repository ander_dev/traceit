package com.mconnit.traceit.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.ws.rs.DefaultValue;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "account")
@XmlRootElement
public class Account extends Audit implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	private Long id;

	private String description;

	private Date startDate;

	private Calendar endDate;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = TypeClosure.class)
	@JoinColumn(name = "typeclosure_id", foreignKey = @ForeignKey(name = "FK_ACCOUNT_TYPECLOSURE"))
	private TypeClosure typeClosure;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = Currency.class)
	@JoinColumn(name = "currency_id", foreignKey = @ForeignKey(name = "FK_ACCOUNT_CURRENCY"))
	private Currency currency;

	@Column(length = 13, precision = 13, scale = 2)
	private BigDecimal totalCredit;

	@Column(length = 13, precision = 13, scale = 2)
	private BigDecimal totalDebit;

	@Column(length = 13, precision = 13, scale = 2)
	private BigDecimal totalGeneral;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = User.class)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_ACCOUNT_USER"))
	private User user;

	@DefaultValue(value = "false")
	private Boolean defaultAccount;

	@OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "account_id", foreignKey = @ForeignKey(name = "FK_ACCOUNT_CLOSURE"))
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Closure> closureList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public BigDecimal getTotalCredit() {
		return totalCredit;
	}

	public void setTotalCredit(BigDecimal totalCredit) {
		this.totalCredit = totalCredit;
	}

	public BigDecimal getTotalDebit() {
		return totalDebit;
	}

	public void setTotalDebit(BigDecimal totalDebit) {
		this.totalDebit = totalDebit;
	}

	public BigDecimal getTotalGeneral() {
		return totalGeneral;
	}

	public void setTotalGeneral(BigDecimal totalGeneral) {
		this.totalGeneral = totalGeneral;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public TypeClosure getTypeClosure() {
		return typeClosure;
	}

	public void setTypeClosure(TypeClosure typeClosure) {
		this.typeClosure = typeClosure;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getDefaultAccount() {
		return defaultAccount;
	}

	public void setDefaultAccount(Boolean defaultAccount) {
		this.defaultAccount = defaultAccount;
	}

	public List<Closure> getClosureList() {
		if (closureList == null) {
			closureList = new ArrayList<>();
		}
		return closureList;
	}

	public void setClosureList(List<Closure> closureList) {
		this.closureList = closureList;
	}
}
