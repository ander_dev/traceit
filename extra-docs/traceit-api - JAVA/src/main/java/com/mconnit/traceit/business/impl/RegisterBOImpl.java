package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.*;
import com.mconnit.traceit.entity.*;
import com.mconnit.traceit.entity.xml.ListResults;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.entity.xml.PaginationResults;
import com.mconnit.traceit.entity.xml.PaginationReturn;
import com.mconnit.traceit.persistence.*;
import com.mconnit.traceit.utils.Constants;
import com.mconnit.traceit.utils.Crypt;
import com.mconnit.traceit.utils.MessageFactory;
import com.mconnit.traceit.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

public class RegisterBOImpl extends GenericBOImpl<Register> implements RegisterBO {

	@Autowired
	private RegisterDAO registerDAO;

	@Autowired
	private ConfigDAO configDAO;

	@Autowired
	private DescriptionDAO descriptionDAO;

	@Autowired
	private DescriptionBO descriptionBO;

	@Autowired
	private TypeClosureDAO typeClosureDAO;

	@Autowired
	private ParcelDAO parcelDAO;

	@Autowired
	private CreditCardDAO creditCardDAO;

	@Autowired
	private CreditCardBO creditCardBO;

	@Autowired
	private UserBO userBO;

	@Autowired
	private TypeAccountBO typeAccountBO;

	@Autowired
	private AccountDAO accountDAO;

	@Autowired
	private AccountBO accountBO;

	@Autowired
	private ChartBO chartBO;

	@Autowired
	private PlanningItemBO planningItemBO;

	private User getSuperUser(Register register) {
		return userBO.getSuperUser(register.getUser());
	}

	private CreditCard getCreditCard(Register debit) {
		try {
			return creditCardDAO.findById(CreditCard.class, debit.getCreditCard().getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setCreditCardDate(Register register) {
		CreditCard creditCard = getCreditCard(register);
		if (register.getId() == null) {
			setupDate(register, creditCard);
		}
	}

	private void setupDate(Register register, CreditCard creditCard) {
		String[] temp = Utils.dateToString(register.getDate()).split("/");
		Integer dayOfBuy = Integer.parseInt(temp[0]);
		Integer monthOfBuy = Integer.parseInt(temp[1]);
		Integer yearOfBuy = Integer.parseInt(temp[2]);
		if ((dayOfBuy < creditCard.getPayday()) || (dayOfBuy > creditCard.getPayday() && dayOfBuy < creditCard.getLastDayToBuy())) {
			register.setDate(Utils.stringToDate(creditCard.getPayday().toString() + "/" + (monthOfBuy + 1) + "/" + yearOfBuy.toString(), false));
		} else {
			register.setDate(Utils.stringToDate(creditCard.getPayday().toString() + "/" + (monthOfBuy + 1) + "/" + yearOfBuy.toString(), false));
		}
	}

	private void setParcel(Register register, Date currentDate, Integer interval, Integer x) {
		Date newDate = currentDate;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(newDate);
		calendar.add(interval, x);
		register.setDate(calendar);
	}

	private Account summarizeAccount(Register register, Boolean deleting) {

		Account account = null;
		try {
			account = accountBO.getById(register.getAccount().getId());

			if (account == null) {
				Map<String, String> queryParams = new LinkedHashMap<>();
				queryParams.put(" where x.user.id = ", "" + register.getUser().getId());

				Config config = configDAO.findByParameter(Config.class, queryParams);
				account = config.getAccount();
			}

			BigDecimal balance = account.getTotalGeneral() != null ? account.getTotalGeneral() : BigDecimal.ZERO;
			BigDecimal debitBalance = account.getTotalDebit() != null ? account.getTotalDebit() : BigDecimal.ZERO;
			BigDecimal creditBalance = account.getTotalCredit() != null ? account.getTotalCredit() : BigDecimal.ZERO;

			if (register.getClosed() == null || !register.getClosed()) {
				if (register.getTypeAccount().getId().equals(1L) || register.getTypeAccount().getId().equals(5L)) {
					creditBalance = creditBalance.add(register.getAmount());
					balance = balance.add(register.getAmount());
				} else if (register.getTypeAccount().getId().equals(2L) || register.getTypeAccount().getId().equals(6L)) {
					debitBalance = debitBalance.add(register.getAmount());
					balance = balance.subtract(register.getAmount());
				}

			} else if (register.getClosed() != null && register.getClosed()) {
				Register regTemp = findById(Register.class, register.getId());
				if (!regTemp.getAmount().equals(register.getAmount())) {
					BigDecimal diff = register.getAmount().subtract(regTemp.getAmount());
					if (register.getTypeAccount().getId().equals(1L) || register.getTypeAccount().getId().equals(5L)) {
						creditBalance = creditBalance.add(diff);
						balance = balance.add(diff);
					} else if (register.getTypeAccount().getId().equals(2L) || register.getTypeAccount().getId().equals(6L)) {
						debitBalance = debitBalance.add(diff);
						balance = balance.subtract(diff);
					}
				} else if (deleting != null && deleting) {
					if (register.getTypeAccount().getId().equals(1L) || register.getTypeAccount().getId().equals(5L)) {
						creditBalance = creditBalance.subtract(register.getAmount());
						balance = balance.subtract(register.getAmount());
					} else if (register.getTypeAccount().getId().equals(2L) || register.getTypeAccount().getId().equals(6L)) {
						debitBalance = debitBalance.subtract(register.getAmount());
						balance = balance.add(register.getAmount());
					}
				}
			}
			account.setTotalCredit(creditBalance);
			account.setTotalDebit(debitBalance);
			account.setTotalGeneral(balance);
			account = accountDAO.save(account);

			register.setClosed(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return account;
	}

	@Override
	@Transactional
	public MessageReturn save(Register register) {
		MessageReturn libReturn = new MessageReturn();
		Map<String, String> queryParams;
		if (register.getId() != null) {
			register.setUpdatedBy(register.getUser().getUsername());
		}

		User user = getSuperUser(register);
		BigDecimal dbAmount = BigDecimal.ZERO;
		register.setUser(user);
		register = setCorrectTypeAccount(register, user);

		if (register.getUser() != null && register.getAccount() != null && register.getSuperGroup() != null) {
			try {

				if (register.getId() != null) {
					dbAmount = findById(Register.class, register.getId()).getAmount();
				}

				if (register.getCreditCard() != null && register.getId() == null) {
					queryParams = new LinkedHashMap<>();
					queryParams.put(" where x.type = ", "'" + MessageFactory.getMessage("lb_monthly", user.getLanguage()) + "'");
					TypeClosure typeClosure = typeClosureDAO.findByParameter(TypeClosure.class, queryParams);
					register.getAccount().setTypeClosure(typeClosure);
					register.setClosed(false);
					setCreditCardDate(register);
					libReturn.setAccount(register.getAccount());
				} else {
					//losing account because it is not summarizing
					libReturn.setAccount(summarizeAccount(register, null));
				}

				if (register.getStringDate() != null && !register.getStringDate().isEmpty()) {
					register.setDate(Utils.stringToDate(register.getStringDate(), true));
				}

				if (register.getMultipleParcel() != null && register.getMultipleParcel()) {
					if (register.getId() == null) {
						Parcel parcel = new Parcel();
						parcel.setParcels(register.getNumberParcel());
						parcel.setUser(user);
						register.setParcel(parcelDAO.save(parcel));
						register.getAccount().setTypeClosure(typeClosureDAO.findById(TypeClosure.class, register.getAccount().getTypeClosure().getId()));
						Date currentDate = register.getDate().getTime();

						for (int x = 0; x < register.getNumberParcel(); x++) {
							if (x > 0) {
								register.setClosed(false);
							}
							if (register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.ANUAL) || register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.YEARLY)) {
								setParcel(register, currentDate, Calendar.YEAR, x);
							} else if (register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.MENSAL) || register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.MONTHLY)) {
								setParcel(register, currentDate, Calendar.MONTH, x);
							} else if (register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.QUINZENAL) || register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.FORTNIGHTLY)) {
								setParcel(register, currentDate, Calendar.DAY_OF_WEEK, x * 14);
							} else if (register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.SEMANAL) || register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.WEEKLY)) {
								setParcel(register, currentDate, Calendar.WEEK_OF_MONTH, x);
							} else if (register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.DIARIO) || register.getAccount().getTypeClosure().getType().toLowerCase().equals(Constants.DAILY)) {
								setParcel(register, currentDate, Calendar.DAY_OF_WEEK, x);
							}
							// save description created on the register form
							register = saveDescription(register);

							register.setCurrentDate(Calendar.getInstance());
							register = saveGeneric(register);
							updateChart(register, dbAmount, false);
							register = newRegister(register);
						}
					} else {
						queryParams = new LinkedHashMap<>();
						queryParams.put(" where ", " 1=1 ");
						queryParams.put(" and x.user = ", user.getId() + "");
						queryParams.put(" and x.parcel = ", register.getParcel().getId() + "");

						List<Register> registerParcels = list(Register.class, queryParams, " x.date");

						for (Register reg : registerParcels) {
							Long id = reg.getId();
							Calendar date = reg.getDate();
							reg = register;
							reg.setId(id);
							reg.setDate(date);
							// save description created on the register form
							reg = saveDescription(register);
							reg.setCurrentDate(Calendar.getInstance());
							register = saveGeneric(reg);
							updateChart(register, dbAmount, false);
						}
					}
				} else {
					// save description created on the register form
					register = saveDescription(register);
					register.setCurrentDate(Calendar.getInstance());
					register = saveGeneric(register);
					updateChart(register, dbAmount, false);
				}

			} catch (Exception e) {
				e.printStackTrace();
				libReturn.setRegister(register);
				libReturn.setMessage(e.getMessage());
			}
			if (libReturn.getMessage() == null && register.getId() == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_register_saved", register.getUser().getLanguage()));
				libReturn.setRegister(register);
			} else if (libReturn.getMessage() == null && register.getId() != null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_register_updated", register.getUser().getLanguage()));
				libReturn.setRegister(register);
			}
		} else {
			libReturn.setMessage(MessageFactory.getMessage("lb_register_not_found", "en"));
		}

		return libReturn;
	}

	private Register newRegister(Register register) throws Exception {
		Register newRegister = new Register();
		newRegister.setAccount(register.getAccount());
		newRegister.setAmount(register.getAmount());
		newRegister.setClosed(register.getClosed());
		newRegister.setCreditCard(register.getCreditCard());
		newRegister.setCurrentDate(register.getCurrentDate());
		newRegister.setDate(register.getDate());
		newRegister.setDescription(register.getDescription());
		newRegister.setGroup(register.getGroup());
		newRegister.setMultipleParcel(register.getMultipleParcel());
		newRegister.setNumberParcel(register.getNumberParcel());
		newRegister.setSuperGroup(register.getSuperGroup());
		newRegister.setTypeAccount(register.getTypeAccount());
		newRegister.setUser(register.getUser());
		newRegister.setParcel(register.getParcel());
		newRegister.setPlanningItem(null);

		return newRegister;
	}

	private PlanningItem getPlanningItem(Register register, boolean nextMonth) throws Exception {

		//getting next month
		Calendar tempRegisterDate = Calendar.getInstance();
		tempRegisterDate.setTime(register.getDate().getTime());
		if(nextMonth){
			tempRegisterDate.add(Calendar.MONTH,1);
		}

		//get PlanningItem based on register date
		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" inner join x.planningGroup pg", " inner join x.chart ch ");
		queryParams.put(" where date_format(ch.date, '%m/%Y') = ", "'"+Utils.dateToStringMonthYear(tempRegisterDate.getTime())+"'");
		queryParams.put(" and pg.description = ", register.getSuperGroup().getId() + "");

		PlanningItem tempPlanningItem = planningItemBO.findByParameter(PlanningItem.class, queryParams);

		if (tempPlanningItem == null) {
			queryParams.clear();
			queryParams.put(" inner join x.planningGroup pg", " inner join x.chart ch ");
			queryParams.put(" where date_format(ch.date, '%m/%Y') = ", "'"+Utils.dateToStringMonthYear(tempRegisterDate.getTime())+"'");
			queryParams.put(" and pg.description = ", register.getGroup().getId() + "");
			tempPlanningItem = planningItemBO.findByParameter(PlanningItem.class, queryParams);
		}
		return tempPlanningItem;
	}

	@Transactional
	private void updateChart(Register register, BigDecimal dbAmount, final boolean isDeleting) throws Exception {

		PlanningItem planningItem = getPlanningItem(register, false);
		if(planningItem != null){
			Chart chart = planningItem.getChart();

			if (chart != null) {
				if (register.getPlanningItem() == null || isDeleting) {
					final boolean isRegisterCredit = register.getTypeAccount().getDescription().equals("Credit") || register.getTypeAccount().getDescription().equals("Crédito") ? true : false;
					final boolean isItemCredit = planningItem.getPlanningGroup().getTypeAccount().getDescription().equals("Credit") || planningItem.getPlanningGroup().getTypeAccount().getDescription().equals("Crédito") ? true : false;

					//testing itemType and registerType to add or subtract amount from item and chart
					if (isItemCredit && isRegisterCredit) {
						isDeletingSubtract(register, isDeleting, chart, planningItem);

					} else if (isItemCredit && !isRegisterCredit) {
						isDeletingAdd(register, isDeleting, chart, planningItem);
					} else if (!isItemCredit && !isRegisterCredit) {
						isDeletingSubtract(register, isDeleting, chart, planningItem);

					} else if (!isItemCredit && isRegisterCredit) {
						isDeletingAdd(register, isDeleting, chart, planningItem);

					}

					planningItem = planningItemBO.saveGeneric(planningItem);

					chartBO.saveGeneric(chart);

					register.setPlanningItem(planningItem);
					saveGeneric(register);
				} else {
					Boolean exists = false;
					for (Register tempRegister : planningItem.getRegisterList()) {
						if (tempRegister.getId().equals(register.getId())) {
							BigDecimal registerAmount = register.getAmount();
							BigDecimal planningItemAmount = planningItem.getCurrentAmount();

							BigDecimal dbRegisterAmount = dbAmount;
							BigDecimal deductedAmount = planningItemAmount.subtract(dbRegisterAmount);

							BigDecimal updatedAmount = deductedAmount.add(registerAmount);

							planningItem.setCurrentAmount(updatedAmount);
							planningItem = planningItemBO.saveGeneric(planningItem);
							tempRegister.setPlanningItem(planningItem);
							saveGeneric(tempRegister);

							chart.setCurrentAmount(updatedAmount);
							chartBO.saveGeneric(chart);

							exists = true;
							break;
						}
					}
					if (!exists) {
						BigDecimal currentAmount = planningItem.getCurrentAmount();
						BigDecimal correctAmount = currentAmount.add(register.getAmount());
						planningItem.setCurrentAmount(correctAmount);

						planningItem = planningItemBO.saveGeneric(planningItem);
						register.setPlanningItem(planningItem);
						saveGeneric(register);
					}
				}
			}
		}
	}

	private void isDeletingAdd(Register register, boolean isDeleteing, Chart chart, PlanningItem planningItem) {
		if (isDeleteing){
            planningItem.setCurrentAmount(planningItem.getCurrentAmount().add(register.getAmount()));
            chart.setCurrentAmount(chart.getCurrentAmount().add(register.getAmount()));
        } else {
            planningItem.setCurrentAmount(planningItem.getCurrentAmount().subtract(register.getAmount()));
            chart.setCurrentAmount(chart.getCurrentAmount().subtract(register.getAmount()));
        }
	}

	private void isDeletingSubtract(Register register, boolean isDeleteing, Chart chart, PlanningItem planningItem) {
		if(isDeleteing){
            planningItem.setCurrentAmount(planningItem.getCurrentAmount().subtract(register.getAmount()));
            chart.setCurrentAmount(chart.getCurrentAmount().subtract(register.getAmount()));
        } else {
            planningItem.setCurrentAmount(planningItem.getCurrentAmount().add(register.getAmount()));
            chart.setCurrentAmount(chart.getCurrentAmount().add(register.getAmount()));
        }
	}

	private TypeAccount getTypeAccountByDescription(String description) {
		TypeAccount typeAccount = null;
		try {
			Map<String, String> queryParams = new LinkedHashMap<>();
			queryParams.put(" where lower(x.description) = ", "'" + description.toLowerCase() + "'");
			typeAccount = findByParameter(TypeAccount.class, queryParams);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typeAccount;
	}

	private Description getDescriptionByDescription(User user, String description) throws Exception {
		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where ", " 1=1 ");
		queryParams.put(" and x.user = ", user.getId() + "");
		queryParams.put(" and x.description = '", Crypt.encrypt(description) + "'");

		return descriptionDAO.findByParameter(Description.class, queryParams);
	}

	private Register saveDescription(Register register) throws Exception {
		User currentUser = register.getUser();
		if (register.getDescription() != null && register.getDescription().getId() == null) {

			Description description = getDescriptionByDescription(currentUser, Utils.trimBeginAndEnd(register.getDescription().getDescription()));
			if (description != null) {
				register.setDescription(description);
			} else {
				register.getDescription().setUser(register.getUser());
				register.getDescription().setTypeAccount(register.getTypeAccount());
				register.setDescription(descriptionDAO.save(register.getDescription()));
			}
		}
		if (register.getGroup() != null && register.getGroup().getId() == null) {
			Description description = getDescriptionByDescription(currentUser, Utils.trimBeginAndEnd(register.getGroup().getDescription()));
			if (description != null) {
				register.setGroup(description);
			} else {
				register.getGroup().setUser(register.getUser());
				String typeDescription = (currentUser.getLanguage().equals("en") ? "Group" : "Grupo");
				TypeAccount typeAccount = getTypeAccountByDescription(typeDescription);
				register.getGroup().setTypeAccount(typeAccount);
				register.setGroup(descriptionDAO.save(register.getGroup()));
			}
		}
		if (register.getSuperGroup() != null && register.getSuperGroup().getId() == null) {
			Description description = getDescriptionByDescription(currentUser, Utils.trimBeginAndEnd(register.getSuperGroup().getDescription()));
			if (description != null) {
				register.setSuperGroup(description);
			} else {
				register.getSuperGroup().setUser(register.getUser());
				String typeDescription = (currentUser.getLanguage().equals("en") ? "Super Group" : "Super Grupo");
				TypeAccount typeAccount = getTypeAccountByDescription(typeDescription);
				register.getSuperGroup().setTypeAccount(typeAccount);
				register.setSuperGroup(descriptionDAO.save(register.getSuperGroup()));
			}
		}
		return register;
	}

	@Override
	public List<Register> list(Register register) throws Exception {
		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.user.id = ", getSuperUser(register).getId() + "");
		return list(Register.class, queryParams, " x.date ");
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		Register register = null;
		try {
			register = findById(Register.class, id);
			if (register == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_register_not_found", "en"));
			} else {
				String locale = register.getUser().getLanguage();

				if(register.getClosed()){
					libReturn.setAccount(summarizeAccount(register, true));
				}

				updateChart(register, null, true);

				remove(register);
				libReturn.setMessage(MessageFactory.getMessage("lb_register_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public MessageReturn getLists(User user) {
		MessageReturn ret = new MessageReturn();

		user = userBO.getSuperUser(user);

		try {
			Description description = new Description();
			description.setUser(user);

			TypeAccount typeAccount = new TypeAccount();
			//CREDIT
			typeAccount.setDescription(MessageFactory.getMessage("lb_credit", user.getLanguage()));
			description.setTypeAccount(typeAccount);
			List<Description> creditDescriptionList = descriptionBO.listByParameter(description);

			//DEBIT
			typeAccount.setDescription(MessageFactory.getMessage("lb_debit", user.getLanguage()));
			description.setTypeAccount(typeAccount);
			List<Description> debitDescriptionList = descriptionBO.listByParameter(description);

			//GROUP
			typeAccount.setDescription(MessageFactory.getMessage("lb_group", user.getLanguage()));
			description.setTypeAccount(typeAccount);
			List<Description> groupList = descriptionBO.listByParameter(description);

			//SUPER GROUP
			typeAccount.setDescription(MessageFactory.getMessage("lb_super_group", user.getLanguage()));
			description.setTypeAccount(typeAccount);
			List<Description> superGroupList = descriptionBO.listByParameter(description);

			//CREDIT_DEBIT
			typeAccount.setDescription("credit_debit");
			List<Description> creditDebitDescriptionList = descriptionBO.listByParameter(description);

			//ACCOUNT
			Account account = new Account();
			account.setUser(user);
			List<Account> accountList = accountBO.listByParameter(account);

			//CREDIT CARD
			List<CreditCard> creditCardList = creditCardBO.listByParameter(user.getId() + "");

			ListResults listResults = new ListResults();
			listResults.setAccountList(accountList);
			listResults.setCreditCardList(creditCardList);
			listResults.setCreditDebitDescriptionList(creditDebitDescriptionList);
			listResults.setCreditDescriptionList(creditDescriptionList);
			listResults.setDebitDescriptionList(debitDescriptionList);
			listResults.setGroupList(groupList);
			listResults.setSuperGroupList(superGroupList);

			ret.setListResults(listResults);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	private String addAndOr(Boolean isAnd) {
		String andOr = "";
		if (isAnd) {
			andOr = "and";
		} else {
			andOr = "or";
		}
		return andOr;
	}

	private Register setCorrectTypeAccount(Register register, User user) {
		Boolean getTypeAccount = false;
		// TODO need a better solution.
		if (register.getTypeAccount().getId() == 5 && user.getLanguage().equals("en")) {
			register.getTypeAccount().setId(1L);
			getTypeAccount = true;
		} else if (register.getTypeAccount().getId() == 6 && user.getLanguage().equals("en")) {
			register.getTypeAccount().setId(2L);
			getTypeAccount = true;
		} else if (register.getTypeAccount().getId() == 1 && user.getLanguage().equals("pt_BR")) {
			register.getTypeAccount().setId(5L);
			getTypeAccount = true;
		} else if (register.getTypeAccount().getId() == 2 && user.getLanguage().equals("pt_BR")) {
			register.getTypeAccount().setId(6L);
			getTypeAccount = true;
		}
		if (getTypeAccount) {
			TypeAccount typeAccount = typeAccountBO.getById(register.getTypeAccount().getId());
			register.setTypeAccount(typeAccount);
		}
		return register;
	}

	@Override
	public PaginationReturn listByParameter(Register register) throws Exception {

		User user = getSuperUser(register);

		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where ", " 1=1 ");
		queryParams.put(" and x.user = ", user.getId() + "");

		if (register.getTypeAccount() != null && register.getTypeAccount().getId() != null && register.getTypeAccount().getId() > 0) {
			register = setCorrectTypeAccount(register, user);
			queryParams.put(" and x.typeAccount = ", register.getTypeAccount().getId() + "");
		}

		Boolean isAnd = true;

		if (register.getSearch() != null && register.getSearch()) {
			if (register.getDescription() != null && register.getDescription().getId() != null && register.getDescription().getId() > 0) {
				queryParams.put(" " + addAndOr(isAnd) + "  x.description = ", register.getDescription().getId() + "");
				isAnd = false;
			}
			if (register.getGroup() != null && register.getGroup().getId() != null && register.getGroup().getId() > 0) {
				queryParams.put(" " + addAndOr(isAnd) + "   x.group = ", register.getGroup().getId() + "");
				isAnd = false;
			}
			if (register.getSuperGroup() != null && register.getSuperGroup().getId() != null && register.getSuperGroup().getId() > 0) {
				queryParams.put(" " + addAndOr(isAnd) + "  x.superGroup = ", register.getSuperGroup().getId() + "");
				isAnd = false;
			}

			if (register.getCreditCard() != null && register.getCreditCard().getId() != null && register.getCreditCard().getId() > 0) {
				queryParams.put(" " + addAndOr(isAnd) + "  x.creditCard = ", register.getCreditCard().getId() + "");
				isAnd = false;
			}
			if (register.getAccount() != null && (register.getAccount().getCurrency() != null && register.getAccount().getCurrency().getId() != null && register.getAccount().getCurrency().getId() > 0)) {
				queryParams.put(" " + addAndOr(isAnd) + "  x.account.currency = ", register.getAccount().getCurrency().getId() + "");
				isAnd = false;
			}

			if (register.getAccount() != null && (register.getAccount().getTypeClosure() != null && register.getAccount().getTypeClosure().getId() != null && register.getAccount().getTypeClosure().getId() > 0)) {
				queryParams.put(" " + addAndOr(isAnd) + "  x.account.typeClosure = ", register.getAccount().getTypeClosure().getId() + "");
				isAnd = false;
			}

			if (register.getStrDate() != null && register.getDate() == null) {
				queryParams.put(" and x.date = ", " '" + Utils.dateToStringMySQL(register.getStrDate()) + "'");
			}

			if (register.getStrDate() == null && register.getDate() != null) {
				queryParams.put(" and x.date = ", " '" + Utils.dateToStringMySQL(register.getDate()) + "'");
			}

			if (register.getStrDate() != null && register.getDate() != null) {
				queryParams.put(" and x.date between ", " '" + Utils.dateToStringMySQL(register.getStrDate()) + "' and '" + Utils.dateToStringMySQL(register.getDate()) + "'");
			}
		}

		List<Register> list = new ArrayList<>();

		if (register.getPaginationValues() != null) {
			list = listPaginated(Register.class, register.getPaginationValues().getStartingAt(), register.getPaginationValues().getMaxPerPage(), queryParams, " x.date desc");
		}

		List<Register> fullList = list(Register.class, queryParams, null);
		BigDecimal creditTotal = BigDecimal.ZERO;
		BigDecimal debitTotal = BigDecimal.ZERO;

		PaginationReturn listRet = new PaginationReturn();

		for (Register reg : fullList) {
			if (reg.getTypeAccount().getDescription().equals("Credito") || reg.getTypeAccount().getDescription().equals("Credit")) {
				creditTotal = creditTotal.add(reg.getAmount());
			} else if (reg.getTypeAccount().getDescription().equals("Debito") || reg.getTypeAccount().getDescription().equals("Debit")) {
				debitTotal = debitTotal.add(reg.getAmount());
			}
		}
		PaginationResults paginationResults = new PaginationResults();

		listRet.setPaginationResults(paginationResults);
		listRet.getPaginationResults().setTotalCredit(creditTotal);
		listRet.getPaginationResults().setTotalDebit(debitTotal);
		listRet.getPaginationResults().setTotal(creditTotal.subtract(debitTotal));

		listRet.getRegisterList().addAll(list);

		int count = count(Register.class, queryParams);

		if (count > list.size()) {
			creditTotal = BigDecimal.ZERO;
			debitTotal = BigDecimal.ZERO;
			for (Register reg : list) {
				if (reg.getTypeAccount().getDescription().equals("Credito") || reg.getTypeAccount().getDescription().equals("Credit")) {
					creditTotal = creditTotal.add(reg.getAmount());
				} else if (reg.getTypeAccount().getDescription().equals("Debito") || reg.getTypeAccount().getDescription().equals("Debit")) {
					debitTotal = debitTotal.add(reg.getAmount());
				}
			}
			listRet.getPaginationResults().setTotalPagedCredit(creditTotal);
			listRet.getPaginationResults().setTotalPagedDebit(debitTotal);
		}

		listRet.setRowCount(count);

		return listRet;
	}

	@Override
	public Register getByDescription(Map<String, String> request) {
		User user = userBO.getById(new Long(request.get("userId")));

		Register register = new Register();
		register.setUser(user);

		Long typeAccountId = new Long(request.get("typeAccountId"));
		Long descriptionId = new Long(request.get("descriptionId"));
		Description description;
		try {
			description = findById(Description.class, descriptionId);
			TypeAccount typeAccount = findById(TypeAccount.class, typeAccountId);
			register = registerDAO.getByDescription(description, getSuperUser(register), typeAccount);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return register;
	}

}
