package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.TypeClosure;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("typeClosureBO")
public interface TypeClosureBO extends GenericBO<TypeClosure> {

	List<TypeClosure> list() throws Exception;

	List<TypeClosure> listByLocale(final String locale) throws Exception;

	MessageReturn save(final TypeClosure typeClosure) throws Exception;

	MessageReturn delete(Long id);

	TypeClosure getById(Long id);
}

