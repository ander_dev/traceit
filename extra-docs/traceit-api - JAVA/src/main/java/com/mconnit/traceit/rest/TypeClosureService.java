package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.TypeClosureBO;
import com.mconnit.traceit.entity.TypeClosure;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/typeclosure")
@CrossOrigin(origins = "http://localhost:3000")
public class TypeClosureService {

	@Autowired
	private TypeClosureBO typeClosureBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<TypeClosure> list() {

		List<TypeClosure> list = new ArrayList<>();
		try {
			list = typeClosureBO.list();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{locale}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<TypeClosure> listByLocale(@PathVariable String locale) {

		List<TypeClosure> list = new ArrayList<>();
		try {
			list = typeClosureBO.listByLocale(locale);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn saveTypeClosure(@RequestBody TypeClosure typeClosure) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = typeClosureBO.save(typeClosure);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.DELETE, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn deleteTypeClosure(@RequestBody TypeClosure typeClosure) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = typeClosureBO.delete(typeClosure.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}