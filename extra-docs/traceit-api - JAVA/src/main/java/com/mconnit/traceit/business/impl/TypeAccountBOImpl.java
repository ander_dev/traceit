package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.TypeAccountBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.entity.TypeAccount;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.utils.MessageFactory;
import com.mconnit.traceit.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TypeAccountBOImpl extends GenericBOImpl<TypeAccount> implements TypeAccountBO {

	@Autowired
	private UserBO userBO;

	@Override
	@Transactional
	public MessageReturn save(TypeAccount typeAccount) {
		MessageReturn libReturn = new MessageReturn();
		TypeAccount c = null;
		try {
			String[] nameSplit = typeAccount.getDescription().split(";");
			String[] showTypeSplit = typeAccount.getShowTypeString().split(";");
			if (nameSplit.length > 1) {
				for (int x = 0; x < nameSplit.length; x++) {
					c = new TypeAccount();
					c.setId(typeAccount.getId());
					c.setDescription(nameSplit[x]);
					c.setLocale(typeAccount.getLocale());
					c.setShowType(Utils.setBooleanFromString(showTypeSplit[x]));
					saveGeneric(c);
				}
			} else {
				saveGeneric(typeAccount);
			}

		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setTypeAccount(typeAccount);
			libReturn.setMessage(e.getMessage());
		}
		if (libReturn.getMessage() == null && typeAccount.getId() == null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_typeaccount_saved", typeAccount.getLocale()));
			libReturn.setTypeAccount(typeAccount);
		} else if (libReturn.getMessage() == null && typeAccount.getId() != null) {
			libReturn.setMessage(MessageFactory.getMessage("lb_typeaccount_updated", typeAccount.getLocale()));
			libReturn.setTypeAccount(typeAccount);
		}
		return libReturn;
	}

	@Override
	public List<TypeAccount> list(final User user, final Boolean showType) throws Exception {
		User superUser = userBO.getSuperUser(user);

		String locale = user.getSuperUser() != null ? superUser.getLanguage() : user.getLanguage();

		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.locale = ", "'" + locale + "'");
		if (showType != null) {
			queryParams.put(" and x.showType = ", Utils.setBooleanValue(showType) + "");
		}
		List<TypeAccount> list = list(TypeAccount.class, queryParams, null);
		for (TypeAccount typeAccount : list) {
			if (superUser.getLanguage().equals("pt_BR")) {
				switch (typeAccount.getDescription().toLowerCase()) {
					case "credit":
						typeAccount.setDescription("Crédito");
						break;
					case "debit":
						typeAccount.setDescription("Débito");
						break;
					case "group":
						typeAccount.setDescription("Grupo");
						break;
					case "super group":
						typeAccount.setDescription("Super Grupo");
						break;
				}
			} else {
				switch (typeAccount.getDescription().toLowerCase()) {
					case "credito":
						typeAccount.setDescription("Credit");
						break;
					case "debito":
						typeAccount.setDescription("Debit");
						break;
					case "grupo":
						typeAccount.setDescription("Group");
						break;
					case "super grupo":
						typeAccount.setDescription("Super Group");
						break;
				}
			}
			if (typeAccount.getSelected() == null) {
				typeAccount.setSelected(false);
			}
		}

		return list;
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		TypeAccount typeAccount = null;
		try {
			typeAccount = findById(TypeAccount.class, id);
			if (typeAccount == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_typeaccount_not_found", "en"));
			} else {
				String locale = typeAccount.getLocale();
				remove(typeAccount);
				libReturn.setMessage(MessageFactory.getMessage("lb_typeaccount_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public TypeAccount getById(Long id) {
		try {
			return findById(TypeAccount.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public TypeAccount getByDescription(Description description) {
		User user = userBO.getSuperUser(description.getUser());
		TypeAccount typeAccount = null;

		if (description.getTypeAccount() != null) {
			if (user.getLanguage().equals("pt_BR")) {
				switch (description.getTypeAccount().getDescription().toLowerCase()) {
					case "credit":
						description.getTypeAccount().setDescription("Credito");
						break;
					case "debit":
						description.getTypeAccount().setDescription("Debito");
						break;
					case "group":
						description.getTypeAccount().setDescription("Grupo");
						break;
					case "super group":
						description.getTypeAccount().setDescription("Super Grupo");
						break;
				}
			} else {
				switch (description.getTypeAccount().getDescription().toLowerCase()) {
					case "credito":
						description.getTypeAccount().setDescription("Credit");
						break;
					case "debito":
						description.getTypeAccount().setDescription("Debit");
						break;
					case "grupo":
						description.getTypeAccount().setDescription("Group");
						break;
					case "super grupo":
						description.getTypeAccount().setDescription("Super Group");
						break;
				}
			}
			typeAccount = getByDescription(description.getTypeAccount().getDescription());
		}

		return typeAccount;
	}

	private TypeAccount getByDescription(final String strDescription) {
		TypeAccount retTypeAccount = null;
		try {
			Map<String, String> queryParams = new LinkedHashMap<>();
			queryParams.put(" where lower(x.description) = ", "'" + strDescription.toLowerCase() + "'");
			retTypeAccount = findByParameter(TypeAccount.class, queryParams);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retTypeAccount;
	}

	@Override
	public Map<String, Object> getTypeAccountDescriptionByUserAndDescription(User user, String description, Boolean withTypeAccount) {
		User tempUser = userBO.getSuperUser(user);
		Map<String, Object> map = new HashMap<String, Object>();
		if (description != null) {
			String strDescription = "";
			if (!tempUser.getId().equals(user.getId()) && !tempUser.getLanguage().equals(user.getLanguage())) {
				if (tempUser.getLanguage().equals("pt_BR")) {
					switch (description.toLowerCase()) {
						case "credit":
							strDescription = "Credito";
							break;
						case "debit":
							strDescription = "Debito";
							break;
						case "group":
							strDescription = "Grupo";
							break;
						case "super group":
							strDescription = "Super Grupo";
							break;
						default:
							strDescription = description;
							break;
					}
				} else {
					switch (description.toLowerCase()) {
						case "credito":
							strDescription = "Credit";
							break;
						case "debito":
							strDescription = "Debit";
							break;
						case "grupo":
							strDescription = "Group";
							break;
						case "super grupo":
							strDescription = "Super Group";
							break;
						default:
							strDescription = description;
							break;
					}
				}
			} else {
				strDescription = description;
			}

			map.put("description", strDescription);

			if (withTypeAccount) {
				map.put("typeAccount", getByDescription(strDescription));
			}
		}

		map.put("user", tempUser);

		return map;
	}

}
