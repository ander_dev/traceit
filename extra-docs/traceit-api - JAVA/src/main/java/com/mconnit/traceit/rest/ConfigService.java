package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.ConfigBO;
import com.mconnit.traceit.entity.Config;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/application")
@CrossOrigin(origins = "http://localhost:3000")
public class ConfigService {

	@Autowired
	private ConfigBO configBO;

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Config> list() {
		List<Config> list = new ArrayList<>();
		try {
			list = configBO.list();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn save(@RequestBody Config config) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = configBO.save(config);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn delete(@PathVariable Long id) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = configBO.delete(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
