package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.DescriptionBO;
import com.mconnit.traceit.business.TypeAccountBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.entity.TypeAccount;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.persistence.DescriptionDAO;
import com.mconnit.traceit.utils.Crypt;
import com.mconnit.traceit.utils.MessageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.Map.Entry;

public class DescriptionBOImpl extends GenericBOImpl<Description> implements DescriptionBO {

	@Autowired
	private UserBO userBO;

	@Autowired
	private DescriptionDAO descriptionDAO;

	@Autowired
	private TypeAccountBO typeAccountBO;

	private User getUser(Description description) {
		return userBO.getSuperUser(description.getUser());
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public MessageReturn save(Description description) {
		MessageReturn libReturn = new MessageReturn();
		User user = getUser(description);
		if (user != null) {
			try {
				Map<String, String> queryParams = new HashMap<>();

				if (user.getSuperUser() != null) {
					queryParams.put(" where x.locale = ", "'" + user.getSuperUser().getLanguage() + "'");
					description.setUser(user.getSuperUser());
				} else {
					queryParams.put(" where x.locale = ", "'" + user.getLanguage() + "'");
					description.setUser(user);
				}

				if (description.getTypeAccountIdList() == null || description.getTypeAccountIdList().isEmpty()) {
					// TODO get error message from bundle
					throw new Exception("Error - Type Account not specified!");
				}

				if (description.getId() == null) {
					for (Long typeAccountId : description.getTypeAccountIdList()) {
						TypeAccount typeAccount = typeAccountBO.getById(typeAccountId);
						description.setTypeAccount(typeAccount);
						description = saveGeneric(description);
					}
				} else {
					Description descTemp = getById(description.getId());
					List<Description> listOfExistentsDescription = listByParameter(descTemp);
					for (Description desc : listOfExistentsDescription) {
						for (Long typeAccountId : description.getTypeAccountIdList()) {
							if (desc.getTypeAccount().getId().equals(typeAccountId)) {
								desc.setDescription(description.getDescription());
								description.getTypeAccountIdList().remove(typeAccountId);
								saveGeneric(desc);
								break;
							}
						}
					}
					if (description.getTypeAccountIdList().size() > 0) {
						for (Long typeAccountId : description.getTypeAccountIdList()) {
							TypeAccount typeAccount = typeAccountBO.getById(typeAccountId);
							description.setTypeAccount(typeAccount);
							description.setId(null);
							saveGeneric(description);
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				libReturn.setMessage(e.getMessage());
			}
			if (libReturn.getMessage() == null && description.getId() == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_description_saved", user.getLanguage()));
				libReturn.setDescription(description);
			} else if (libReturn.getMessage() == null && description.getId() != null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_description_updated", user.getLanguage()));
				libReturn.setDescription(description);
			}
		} else {
			libReturn.setMessage(MessageFactory.getMessage("lb_description_not_found", "en"));
		}

		return libReturn;
	}

	@Override
	public List<Description> list() throws Exception {
		return list(Description.class, null, null);
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		Description description;
		try {
			description = findById(Description.class, id);
			if (description == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_description_not_found", "en"));
			} else {
				String locale = description.getUser().getLanguage();
				remove(description);
				libReturn.setMessage(MessageFactory.getMessage("lb_description_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public Description getById(Long id) {
		try {
			Description description = findById(Description.class, id);
			List<Description> list = listByParameter(description);
			for (Description descriptionTemp : list) {
				description.getTypeAccountIdList().add(descriptionTemp.getTypeAccount().getId());
			}
			return description;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Description> listByParameter(Description description) throws Exception {
		User user = getUser(description);
		// TODO add error message to the bundle
		// in case of user not informed

		description.setUser(user);

		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where ", " 1=1 ");
		queryParams.put(" and x.user = ", description.getUser().getId() + "");

		if (description.getDescription() != null && !description.getDescription().isEmpty()) {
			queryParams.put(" and x.description = ", "'" + Crypt.encrypt(description.getDescription()) + "'");
		} else if (description.getTypeAccount() != null && description.getTypeAccount().getId() != null) {
			queryParams.put(" and x.typeAccount = ", description.getTypeAccount().getId() + "");
		} else if (description.getTypeAccount() != null && description.getTypeAccount().getDescription() != null && description.getTypeAccount().getDescription().equals("credit_debit")) {
			queryParams.put(" and x.typeAccount.description in (", "'" + MessageFactory.getMessage("lb_credit", description.getUser().getLanguage()) + "','" + MessageFactory.getMessage("lb_debit", description.getUser().getLanguage()) + "')");
		} else if (description.getTypeAccount() != null && description.getTypeAccount().getDescription() != null && description.getTypeAccount().getDescription().equals("group_super_group")) {
			queryParams.put(" and x.typeAccount.description in (", "'" + MessageFactory.getMessage("lb_group", description.getUser().getLanguage()) + "','" + MessageFactory.getMessage("lb_super_group", description.getUser().getLanguage()) + "')");
		} else if (description.getTypeAccount() != null && description.getTypeAccount().getDescription() != null && !description.getTypeAccount().getDescription().equals("credit_debit")) {
			queryParams.put(" and x.typeAccount.description = ", "'" + description.getTypeAccount().getDescription() + "'");
		}

		return list(Description.class, queryParams, "x.typeAccount.description, x.description");
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public Description getByDescription(Map<String, String> request) throws Exception {
		String userId = null;
		String typeAccountId = null;
		String description = null;
		Iterator it = request.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> pairs = (Entry<String, String>) it.next();

			switch (pairs.getKey()) {
				case "user":
					userId = pairs.getValue();
					break;
				case "typeAccount":
					typeAccountId = pairs.getValue();
					break;
				case "description":
					description = pairs.getValue();
					break;
			}

			it.remove(); // avoids a ConcurrentModificationException
		}
		User user = new User();
		user.setId(Long.decode(userId));
		TypeAccount typeAccount = typeAccountBO.getById(Long.decode(typeAccountId));
		Description descriptionTemp = new Description();
		descriptionTemp.setTypeAccount(typeAccount);
		descriptionTemp.setUser(user);

		Map<String, Object> map = typeAccountBO.getTypeAccountDescriptionByUserAndDescription(user, typeAccount.getDescription(), true);
		user = (User) map.get("user");
		typeAccount = (TypeAccount) map.get("typeAccount");

		Map<String, String> queryParams = new LinkedHashMap<>();

		queryParams.put(" where x.user = ", user.getId() + "");
		queryParams.put(" and x.typeAccount = ", typeAccount.getId() + "");
		queryParams.put(" and lower(x.description) = '", Crypt.encrypt(description) + "'");

		return findByParameter(Description.class, queryParams);
	}
}
