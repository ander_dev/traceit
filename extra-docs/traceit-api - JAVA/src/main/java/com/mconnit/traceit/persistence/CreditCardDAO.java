package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.CreditCard;

public interface CreditCardDAO extends GenericDAO<CreditCard> {

}
