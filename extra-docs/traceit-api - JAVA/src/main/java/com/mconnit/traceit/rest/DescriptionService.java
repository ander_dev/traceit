package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.DescriptionBO;
import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/rest/description")
@CrossOrigin(origins = "http://localhost:3000")
public class DescriptionService {

	@Autowired
	private DescriptionBO descriptionBO;

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/getall", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Description> listByParameter(@RequestBody Description description) {

		List<Description> list = new ArrayList<>();
		try {
			list = descriptionBO.listByParameter(description);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/getbydescription", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn getDescriptionByDescription(@RequestBody Map<String, String> request) {
		MessageReturn ret = new MessageReturn();
		try {
			ret.setDescription(descriptionBO.getByDescription(request));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn getDescriptionById(@PathVariable Long id) {
		MessageReturn ret = new MessageReturn();
		try {
			Description desc = descriptionBO.getById(id);
			ret.setDescription(desc);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn saveDescription(@RequestBody Description description) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = descriptionBO.save(description);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn delete(@RequestBody ArrayList<Long> ids) {
		MessageReturn ret = new MessageReturn();
		try {
			for (Long id : ids) {
				ret = descriptionBO.delete(Long.valueOf(id));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
