package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Currency;

public interface CurrencyDAO extends GenericDAO<Currency> {

}
