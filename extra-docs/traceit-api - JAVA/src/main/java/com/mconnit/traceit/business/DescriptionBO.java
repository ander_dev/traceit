package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Description;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("descriptionBO")
public interface DescriptionBO extends GenericBO<Description> {

	List<Description> list() throws Exception;

	List<Description> listByParameter(Description description) throws Exception;

	MessageReturn save(final Description description) throws Exception;

	MessageReturn delete(Long id);

	Description getById(Long id);

	Description getByDescription(Map<String, String> request) throws Exception;

}

