package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserDAO extends GenericDAO<User>, UserDetailsService {

	MessageReturn getByUsername(String username) throws Exception;

}
