package com.mconnit.traceit.business;

import java.util.List;
import java.util.Map;

public interface GenericBO<T> {

	<T> T findById(final Class<T> type, final Long id) throws Exception;

	<T> T findByParameter(final Class<T> type, final Map<String, String> queryParams) throws Exception;

	List<T> listPaginated(final Class<T> type, int startRow, int pageSize, Map<String, String> queryParams, String orderByField) throws Exception;

	List<T> list(final Class<T> type, Map<String, String> queryParams, String orderByField) throws Exception;

	Integer count(final Class<T> type, Map<String, String> queryParams) throws Exception;

	T saveGeneric(T obj) throws Exception;

	Boolean remove(T obj) throws Exception;

}
