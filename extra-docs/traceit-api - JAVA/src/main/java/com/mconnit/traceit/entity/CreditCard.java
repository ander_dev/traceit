package com.mconnit.traceit.entity;

import com.mconnit.traceit.utils.Utils;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "creditCard")
@XmlRootElement
public class CreditCard extends Audit implements Serializable {

	private static final long serialVersionUID = -3160404156422105911L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	private Long id;

	private String description;

	@Column(name = "payday")
	private Integer payday;

	@Column(name = "lastDayToBuy")
	private Integer lastDayToBuy;

	@Column(name = "expireDate")
	private Date expireDate;

	@Transient
	private String expire;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = User.class)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_CREDITCARD_USER"))
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getPayday() {
		return payday;
	}

	public void setPayday(Integer payday) {
		this.payday = payday;
	}

	public Integer getLastDayToBuy() {
		return lastDayToBuy;
	}

	public void setLastDayToBuy(Integer lastDayToBuy) {
		this.lastDayToBuy = lastDayToBuy;
	}

	public Date getExpireDate() {
		setExpire(Utils.dateToStringMonthYear(expireDate));
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getExpire() {
		return expire;
	}

	public void setExpire(String expire) {
		this.expire = expire;
	}

}
