package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Account;
import com.mconnit.traceit.persistence.AccountDAO;
import org.springframework.stereotype.Repository;

@Repository("accountDAO")
public class AccountDAOImpl extends GenericDAOImpl<Account> implements AccountDAO {

}
