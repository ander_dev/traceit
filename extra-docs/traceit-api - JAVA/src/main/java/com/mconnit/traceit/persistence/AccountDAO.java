package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Account;

public interface AccountDAO extends GenericDAO<Account> {

}
