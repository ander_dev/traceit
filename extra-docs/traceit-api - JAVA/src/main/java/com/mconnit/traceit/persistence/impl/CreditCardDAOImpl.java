package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.CreditCard;
import com.mconnit.traceit.persistence.CreditCardDAO;
import org.springframework.stereotype.Repository;

@Repository("creditCardDAO")
public class CreditCardDAOImpl extends GenericDAOImpl<CreditCard> implements CreditCardDAO {


}
