package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.ConfigBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.Config;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.utils.MessageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class ConfigBOImpl extends GenericBOImpl<Config> implements ConfigBO {

	@Autowired
	private UserBO userBO;

	private User getUser(Config config) {
		return userBO.getSuperUser(config.getUser());
	}

	@Override
	@Transactional
	public MessageReturn save(Config config) {
		MessageReturn libReturn = new MessageReturn();
		if (config.getId() != null) {
			config.setUpdatedBy(config.getUser().getUsername());
		}
		User user = getUser(config);
		if (user != null || config.getAccount() != null) {
			Config conf = new Config();
			try {
				conf = saveGeneric(config);
			} catch (Exception e) {
				e.printStackTrace();
				libReturn.setConfig(config);
				libReturn.setMessage(e.getMessage());
			}
			if (libReturn.getMessage() == null && config.getId() == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_config_saved", user.getLanguage()));
				libReturn.setConfig(conf);
			} else if (libReturn.getMessage() == null && config.getId() != null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_config_updated", user.getLanguage()));
				libReturn.setConfig(conf);
			}
		} else {
			libReturn.setMessage(MessageFactory.getMessage("lb_config_not_found", "en"));
		}

		return libReturn;
	}

	public List<Config> list() throws Exception {
		return list(Config.class, null, null);
	}

	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		Config config = null;
		try {
			config = findById(Config.class, id);
			if (config == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_config_not_found", "en"));
			} else {
				String locale = config.getUser().getLanguage();
				remove(config);
				libReturn.setMessage(MessageFactory.getMessage("lb_config_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public Config getById(Long id) {
		try {
			return findById(Config.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
