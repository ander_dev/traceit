package com.mconnit.traceit.rest;

import com.mconnit.traceit.business.RegisterBO;
import com.mconnit.traceit.entity.Register;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.entity.xml.PaginationReturn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/rest/register")

public class RegisterService {

	@Autowired
	private RegisterBO registerBO;

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/all", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	List<Register> list(@RequestBody Register register) {

		List<Register> list = new ArrayList<>();
		try {
			list = registerBO.list(register);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/getLists", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn getLists(@RequestBody User user) {

		MessageReturn ret = new MessageReturn();
		try {
			ret = registerBO.getLists(user);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	PaginationReturn listByParameter(@RequestBody Register register) {

		PaginationReturn paginationReturn = new PaginationReturn();
		try {
			paginationReturn = registerBO.listByParameter(register);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return paginationReturn;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/getbydescription", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn getByDescription(@RequestBody Map<String, String> request) {

		MessageReturn ret = new MessageReturn();
		try {
			Register register = registerBO.getByDescription(request);
			ret.setRegister(register);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.APPLICATION_JSON})
	@RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn save(@RequestBody Register register) {
		MessageReturn ret = new MessageReturn();
		try {
			ret = registerBO.save(register);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@Produces({MediaType.APPLICATION_JSON})
	@RequestMapping(value = "/delete", method = RequestMethod.PUT, headers = "Accept=application/json")
	public
	@ResponseBody
	MessageReturn delete(@RequestBody ArrayList<Long> ids) {
		MessageReturn ret = new MessageReturn();
		try {
			for (Long id : ids) {
				ret = registerBO.delete(id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
}
