package com.mconnit.traceit.entity.xml;

import com.mconnit.traceit.entity.Account;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * Created by anderson on 18/04/16.
 */
public class TransferBtAccounts {

	private BigDecimal ammount;

	private Calendar date;

	private Account accountFrom;

	private Account accountTo;

	public BigDecimal getAmmount() {
		return ammount;
	}

	public void setAmmount(BigDecimal ammount) {
		this.ammount = ammount;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public Account getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(Account accountFrom) {
		this.accountFrom = accountFrom;
	}

	public Account getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(Account accountTo) {
		this.accountTo = accountTo;
	}
}
