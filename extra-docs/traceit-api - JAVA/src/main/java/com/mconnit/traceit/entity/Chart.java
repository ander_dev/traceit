package com.mconnit.traceit.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "chart")
@XmlRootElement
public class Chart implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, insertable = true, updatable = false)
	private Long id;

	private Date date;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = Description.class)
	@JoinColumn(name = "description_id", foreignKey = @ForeignKey(name = "FK_CHART_DESCRIPTION"))
	private Description description;

	@ManyToOne(cascade = {CascadeType.REFRESH}, targetEntity = User.class)
	@JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_CHART_USER"))
	private User user;

	@Column(name = "visible", columnDefinition = "bit(1) default false")
	private Boolean visible;

	@Column(name = "paid", columnDefinition = "bit(1) default false")
	private Boolean paid;

	@Column(name = "iscredit", columnDefinition = "bit(1) default false")
	private Boolean isCredit;

	@Column(length = 13, precision = 13, scale = 2)
	private BigDecimal amount;

	@Column(length = 13, precision = 13, scale = 2)
	private BigDecimal currentAmount;

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(BigDecimal currentAmount) {
		if (currentAmount == null) {
			currentAmount = BigDecimal.ZERO;
		}
		this.currentAmount = currentAmount;
	}

	public Boolean getIsCredit() {
		return isCredit;
	}

	public void setIsCredit(Boolean isCredit) {
		this.isCredit = isCredit;
	}

	public Boolean getPaid() {
		return paid;
	}

	public void setPaid(Boolean payed) {
		this.paid = payed;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Description getDescription() {
		return description;
	}

	public void setDescription(Description description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
