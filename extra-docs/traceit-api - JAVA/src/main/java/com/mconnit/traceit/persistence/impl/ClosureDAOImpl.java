package com.mconnit.traceit.persistence.impl;

import com.mconnit.traceit.entity.Account;
import com.mconnit.traceit.entity.Closure;
import com.mconnit.traceit.entity.Register;
import com.mconnit.traceit.persistence.ClosureDAO;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Collection;

@Repository("closureDAO")
public class ClosureDAOImpl extends GenericDAOImpl<Closure> implements ClosureDAO {

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Register> getRegisters(Account account, String startDate, String endDate, boolean closed, Long typeAccountId) {
		StringBuilder sql = new StringBuilder();
		sql.append(" select d from Register d ");
		sql.append(" where d.user = ").append(account.getUser().getId());
		sql.append(" and d.account = ").append(account.getId());
		sql.append(" and d.closed = ").append(closed);
		sql.append(" and d.typeAccount = ").append(typeAccountId);
		sql.append(" and d.date between ");
		sql.append(" str_to_date('").append(startDate).append("', '%d/%m/%Y') and str_to_date('").append(endDate).append("', '%d/%m/%Y') ");

		System.out.println("Query: " + sql.toString());

		Query query = em.createQuery(sql.toString());
		Collection<Register> res = query.getResultList();

		return res;
	}


}
