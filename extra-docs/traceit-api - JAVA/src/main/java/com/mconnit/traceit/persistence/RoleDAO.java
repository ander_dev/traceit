package com.mconnit.traceit.persistence;

import com.mconnit.traceit.entity.Role;

public interface RoleDAO extends GenericDAO<Role> {

}
