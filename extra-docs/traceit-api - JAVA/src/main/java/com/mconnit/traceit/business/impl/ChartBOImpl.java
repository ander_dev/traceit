package com.mconnit.traceit.business.impl;

import com.mconnit.traceit.business.ChartBO;
import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.Chart;
import com.mconnit.traceit.entity.PlanningItem;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.persistence.ChartDAO;
import com.mconnit.traceit.persistence.PlanningItemDAO;
import com.mconnit.traceit.utils.MessageFactory;
import com.mconnit.traceit.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ChartBOImpl extends GenericBOImpl<Chart> implements ChartBO {

	@Autowired
	private UserBO userBO;

	@Autowired
	private ChartDAO chartDAO;

	@Autowired
	private PlanningItemDAO planningItemDAO;

	public User getUser(User user) {
		return userBO.getSuperUser(user);
	}

	@Override
	@Transactional
	public MessageReturn save(Chart chart) {
		MessageReturn libReturn = new MessageReturn();

		try {
			if (chart.getId() == null) {
				if (Utils.isCurrentMonth(chart.getDate())) {
					chart.setVisible(Boolean.TRUE);
				} else {
					chart.setVisible(Boolean.FALSE);
				}
				chart.setPaid(Boolean.FALSE);
			} else {
				Map<String, String> queryParams = new LinkedHashMap<>();
				queryParams.put(" where x.visible = ", true + "");
				queryParams.put(" and x.description.id = ", chart.getDescription().getId() + "");

				Chart tempChart = findByParameter(Chart.class, queryParams);
				if (tempChart != null && chart.getVisible() && !chart.getId().equals(tempChart.getId())) {
					libReturn.setMessage(MessageFactory.getMessage("lb_chart_already_visible", chart.getUser().getLanguage()));
					return libReturn;
				}
			}

			//get superUser
			chart.setUser(getUser(chart.getUser()));
			libReturn.setChart(chartDAO.save(chart));
			if (chart.getId() == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_chart_saved", chart.getUser().getLanguage()));
			} else if (chart.getId() != null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_chart_updated", chart.getUser().getLanguage()));
			}
		} catch (Exception e) {
			libReturn.setMessage("Unable to save chart, please report issue. " + e.getMessage());
		}
		return libReturn;
	}

	@Override
	public List<Chart> listByUser(Long userId, Boolean visible, String orderBy) throws Exception {

		User user = new User();
		user.setId(userId);

		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where x.user = ", getUser(user).getId()+"");
		if (visible != null){
			queryParams.put(" and x.visible = ", Utils.setBooleanValue(visible)+"");
		}

		return list(Chart.class, queryParams, orderBy == null ? "x.date asc, x.description.description": orderBy);
	}


	@Override
	@Transactional
	public MessageReturn delete(Long id) {
		MessageReturn libReturn = new MessageReturn();
		Chart chart;
		PlanningItem planningItem;
		try {
			chart = findById(Chart.class, id);
			if (chart == null) {
				libReturn.setMessage(MessageFactory.getMessage("lb_chart_not_found", "en"));
			} else {
				Map<String, String> queryParams = new LinkedHashMap<>();
				queryParams.put(" where x.chart.id = ", id + "");
				planningItem = findByParameter(PlanningItem.class, queryParams);
				if (planningItem != null) {
					planningItem.setChart(null);
					planningItemDAO.save(planningItem);
				}

				String locale = chart.getUser().getLanguage();
				remove(chart);
				libReturn.setMessage(MessageFactory.getMessage("lb_chart_deleted", locale));
			}
		} catch (Exception e) {
			e.printStackTrace();
			libReturn.setMessage(e.getMessage());
		}
		return libReturn;
	}

	@Override
	public Chart getById(Long id) {
		try {
			return findById(Chart.class, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Chart> listByParameter(Chart chart) throws Exception {
		User user = getUser(chart.getUser());
		// TODO add error message to the bundle
		// in case of user not informed

		chart.setUser(user);

		Map<String, String> queryParams = new LinkedHashMap<>();
		queryParams.put(" where ", " 1=1 ");
		queryParams.put(" and x.user = ", chart.getUser().getId() + "");

		return list(Chart.class, queryParams, "x. date, x.description.description");
	}

	@Override
	public List<Chart> listByParameter(Map<String, String> queryParams, String orderBy) throws Exception {

		return list(Chart.class, queryParams, orderBy == null ? "x.date asc, x.description.description": orderBy);
	}

}
