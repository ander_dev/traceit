package com.mconnit.traceit.business;

import com.mconnit.traceit.entity.Register;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.entity.xml.PaginationReturn;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("registerBO")
public interface RegisterBO extends GenericBO<Register> {

	List<Register> list(Register register) throws Exception;

	PaginationReturn listByParameter(Register register) throws Exception;

	MessageReturn save(final Register debit) throws Exception;

	MessageReturn delete(Long id);

	MessageReturn getLists(User user);

	Register getByDescription(Map<String, String> request);

}

