package com.mconnti.traceit;

import com.mconnit.traceit.business.*;
import com.mconnit.traceit.entity.*;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Anderson on 6/11/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-setup/applicationContext.xml"})
public class ChartTest {

	@Autowired
	private UserBO userBO;

	@Autowired
	private PlanningBO planningBO;

	@Autowired
	private PlanningGroupBO planningGroupBO;

	@Autowired
	private PlanningItemBO planningItemBO;

	@Autowired
	private DescriptionBO descriptionBO;

	@Autowired
	private ChartBO chartBO;

	private User user;

	private Description creditDescription;

	private Description debitDescription;

	private Planning planning;

	private PlanningGroup planningGroup;

	private List<Chart> chartList = new ArrayList<>();

//	@Before
	public void init() {
		try {
			Map<String, String> params = new HashMap<>();
			params.put(" where x.username = ", "'unit.test'");

			this.user = userBO.findByParameter(User.class, params);

			setDescription();

			savePlanning();

			savePlanningGroup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@After
	public void destroy() {
		try {
			planningBO.deletePlanning(this.planning);
			for (Chart chart : chartList) {
				chartBO.delete(chart.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void setDescription() {
		try {
			Map<String, String> params = new HashMap<>();
			params.put(" where x.description = ", "'Junit Credit'");
			this.creditDescription = descriptionBO.findByParameter(Description.class, params);

			params.clear();
			params.put(" where x.description = ", "'Junit Debit'");
			this.debitDescription = descriptionBO.findByParameter(Description.class, params);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void savePlanning() {
		try {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(new Date());

			Planning planning = new Planning();
			planning.setDescription("2015");
			planning.setUser(user);
			planning.setSelected(Boolean.TRUE);
			planning.setDate(calendar);
			this.planning = planningBO.saveGeneric(planning);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void savePlanningGroup() {
		try {
			PlanningGroup planningGroup = new PlanningGroup();
			planningGroup.setUser(this.user);
			planningGroup.setDescription(this.creditDescription);
			planningGroup.setIsCredit(Boolean.TRUE);
			planningGroup.setPlanning(this.planning);
			MessageReturn messageReturn = planningBO.saveGroup(planningGroup);
			this.planningGroup = messageReturn.getPlanningGroup();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void createChartVisibleSuccess() {
		try {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(new Date());

			Chart chart = new Chart();
			chart.setUser(getUser());
			chart.setDate(calendar.getTime());
			chart.setDescription(this.creditDescription);
//            chart.setPlannedValue(new BigDecimal(300.00));
//            chart.setRealizedValue(new BigDecimal(150.00));
			MessageReturn msgReturn = chartBO.save(chart);
			chartList.add(msgReturn.getChart());

			Map<String, String> params = new HashMap<>();
			params.put(" where x.description.description = ", "'Junit Credit'");

			Chart savedChart = chartBO.findByParameter(Chart.class, params);

			Assert.assertEquals(msgReturn.getChart().getId(), savedChart.getId());

			Assert.assertEquals(Boolean.TRUE, savedChart.getVisible());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void createChartNotVisibleSuccess() {
		try {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(new Date());
			calendar.add(Calendar.MONTH, 1);

			Chart chart = new Chart();
			chart.setUser(getUser());
			chart.setDate(calendar.getTime());
			chart.setDescription(this.debitDescription);
//            chart.setPlannedValue(new BigDecimal(300.00));
//            chart.setRealizedValue(new BigDecimal(150.00));
			MessageReturn msgReturn = chartBO.save(chart);
			chartList.add(msgReturn.getChart());

			Map<String, String> params = new HashMap<>();
			params.put(" where x.description.description = ", "'Junit Debit'");

			Chart savedChart = chartBO.findByParameter(Chart.class, params);

			Assert.assertEquals(msgReturn.getChart().getId(), savedChart.getId());

			Assert.assertEquals(Boolean.FALSE, savedChart.getVisible());
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void createChartByPlanningItemSuccess() {
		try {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(new Date());

			PlanningGroup pGroup = planningGroupBO.saveGeneric(this.planningGroup);
			this.planningGroup = pGroup;
			PlanningItem visiblePlanningItem = null;
			PlanningItem notVisiblePlanningItem = null;
			for (PlanningItem planningItem : pGroup.getPlannigItemList()) {
				switch (planningItem.getMonth()) {
					case 11:
						planningItem.setDate(calendar);
						planningItem.setAmount(new BigDecimal(500.00));

						planningBO.saveItem(planningItem);
						Map<String, String> params = new HashMap<>();
						params.put(" where x.id = ", planningItem.getId() + "");
						visiblePlanningItem = planningBO.findByParameter(PlanningItem.class, params);
						chartList.add(visiblePlanningItem.getChart());
						break;
					case 12:
						calendar.add(Calendar.MONTH, 1);
						planningItem.setDate(calendar);
						planningItem.setAmount(new BigDecimal(500.00));

						planningBO.saveItem(planningItem);
						Map<String, String> paramss = new HashMap<>();
						paramss.put(" where x.id = ", planningItem.getId() + "");
						notVisiblePlanningItem = planningBO.findByParameter(PlanningItem.class, paramss);
						chartList.add(notVisiblePlanningItem.getChart());
						break;
				}
			}

			Assert.assertTrue(visiblePlanningItem.getChart().getVisible().equals(Boolean.TRUE));
			Assert.assertTrue(notVisiblePlanningItem.getChart().getVisible().equals(Boolean.FALSE));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void updateChartByPlanningItemSuccess() {
		try {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(new Date());

			PlanningGroup pGroup = planningGroupBO.saveGeneric(this.planningGroup);
			this.planningGroup = pGroup;

			PlanningItem visiblePlanningItem = null;
			PlanningItem visiblePlanningItemUpdate = null;
			//INSERT
			for (PlanningItem planningItem : pGroup.getPlannigItemList()) {
				switch (planningItem.getMonth()) {
					case 11:
						planningItem.setDate(calendar);
						planningItem.setAmount(new BigDecimal(500.00));

						planningBO.saveItem(planningItem);
						Map<String, String> params = new HashMap<>();
						params.put(" where x.id = ", planningItem.getId() + "");
						visiblePlanningItem = planningBO.findByParameter(PlanningItem.class, params);
						break;
				}
			}

//            Assert.assertFalse(visiblePlanningItem.getChart().getPlannedValue().equals(new BigDecimal(500.00)));

			//UPDATE
			for (PlanningItem planningItem : pGroup.getPlannigItemList()) {
				switch (planningItem.getMonth()) {
					case 11:
						planningItem.setDate(calendar);
						planningItem.setAmount(new BigDecimal(800.00));

						planningBO.saveItem(planningItem);
						Map<String, String> params = new HashMap<>();
						params.put(" where x.id = ", planningItem.getId() + "");
						visiblePlanningItemUpdate = planningBO.findByParameter(PlanningItem.class, params);
						chartList.add(visiblePlanningItemUpdate.getChart());
						break;
				}
			}

			Assert.assertTrue(visiblePlanningItemUpdate.getAmount() == new BigDecimal(800.00));
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test
	public void testDate() throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date strdate = sdf.parse("30/06/2016 23:58:00");
		long millis = strdate.getTime();
		System.out.println("30/06/2016 23:58:00 in millis is: "+millis);

		Calendar dateRet = Utils.timemillisToDate(millis);
		System.out.println(dateRet.getTime());

		String gmt = "GMT-0300";

		Calendar currentDate = Calendar.getInstance();
		System.out.println("Current Time @ "+TimeZone.getDefault().getID()+": "+currentDate.getTime());

		TimeZone.setDefault(TimeZone.getTimeZone(gmt));
		currentDate = Calendar.getInstance();
		System.out.println("Time @ "+gmt+": "+currentDate.getTime());
		System.out.println("Timezone ID: "+TimeZone.getDefault().getID());
		System.out.println("Calendar.DAY_OF_MONTH: "+currentDate.get(Calendar.DAY_OF_MONTH));
		System.out.println("currentDate.getActualMaximum(Calendar.DAY_OF_MONTH)): "+currentDate.getActualMaximum(Calendar.DAY_OF_MONTH));

		if (currentDate.get(Calendar.DAY_OF_MONTH) == currentDate.getActualMaximum(Calendar.DAY_OF_MONTH)){
			System.out.println(true);
		}

		SimpleDateFormat dt1 = new SimpleDateFormat("HH:mm");
		System.out.println(dt1.format(currentDate.getTime()));

	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Planning getPlanning() {
		return planning;
	}

	public void setPlanning(Planning planning) {
		this.planning = planning;
	}

	public PlanningGroup getPlanningGroup() {
		return planningGroup;
	}

	public void setPlanningGroup(PlanningGroup planningGroup) {
		this.planningGroup = planningGroup;
	}

	public List<Chart> getChartList() {
		return chartList;
	}

	public void setChartList(List<Chart> chartList) {
		this.chartList = chartList;
	}
}
