package com.mconnti.traceit;

import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.thread.EmailThread;
import com.mconnit.traceit.utils.Utils;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import org.junit.Test;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

public class EmailTest {

	@Test
	public void sendEmailThruSendGrid() {
		SendGrid sendgrid = new SendGrid("traceitmail", "@Tracepass79");

		SendGrid.Email email = new SendGrid.Email();
		email.addTo("ander.dev@gmail.com");
		email.setFrom("contact@traceit.nz");
		email.setSubject("Hello World");
		email.setText("My first email with SendGrid Java!");

		try {
			SendGrid.Response response = sendgrid.send(email);
			System.out.println(response.getMessage());
		} catch (SendGridException e) {
			System.err.println(e);
		}
	}

	@Test
	public void sendEmailThruGoogle() {
		try {
			Utils.sendEmail("ander.dev@gmail.com", "bruno.wunsche@gmail.com", "Anderson", "Email test", "This supposed to be in the body!.");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void ThreadMail() {
		User user = new User();
		user.setEmail("ander.dev@gmail.com");
		user.setLanguage("en");
		user.setName("Anderson Santos");
        user.setAuthenticationUrl("http://localhost:3000/signin");
        EmailThread emailThread =  new EmailThread(user,"SUPER-USER");
        emailThread.start();
	}

}
