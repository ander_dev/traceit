package com.mconnti.traceit;

import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.scheduler.CloseAccountJob;
import com.mconnit.traceit.utils.Utils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-setup/applicationContext.xml"})
public class CloseAccountTest {

	@Autowired
	private UserBO userBO;

	private User user;

	@Before
	public void init() {
	}

	@After
	public void destroy() {
	}


	@Test
	public void closeAccountTest() {
		try {
			List<User> userList = userBO.list();
			for (User user : userList) {
				if(user.getGmt() != null){

					TimeZone.setDefault(TimeZone.getTimeZone(user.getGmt()));
					Calendar currentDate = Calendar.getInstance();

					System.out.println("User: "+user.getName());
					System.out.println("User GMT: "+user.getGmt());

					System.out.println("User currentDate: "+currentDate.getTime());

					if (!Utils.isEndOfMonth(currentDate.getTime())){
						SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
						if(!sdf.format(currentDate.getTime()).equals("23:59")){
							new CloseAccountJob();
						} else {
							System.out.println("not yet");
						}
					} else {
						System.out.println("not last day of the month: "+currentDate.getTime());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
