package com.mconnti.traceit;

import com.mconnit.traceit.business.RegisterBO;
import com.mconnit.traceit.entity.Register;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anderson Santos on 13/10/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-setup/applicationContext.xml"})
public class RegisterTest {

	@Autowired
	private RegisterBO registerBO;

	private List<Register> toBeDeleted = new ArrayList<>();

	@After
	public void destroy() {
		try {
			for (Register register : toBeDeleted) {
				registerBO.delete(register.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	private void newRegisterWithNewDescriptionsTest() {
	}
}
