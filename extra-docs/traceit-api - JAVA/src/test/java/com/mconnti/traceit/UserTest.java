package com.mconnti.traceit;

import com.mconnit.traceit.business.UserBO;
import com.mconnit.traceit.entity.User;
import com.mconnit.traceit.entity.xml.MessageReturn;
import com.mconnit.traceit.utils.Crypt;
import com.mconnit.traceit.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anderson on 5/10/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/test-setup/applicationContext.xml"})
public class UserTest {

	@Autowired
	private UserBO userBO;

	@Before
	public void init() {
		User user = new User();
		user.setName("Unit Test");
		user.setEmail("ander.dev@gmail.com");
		user.setUsername("unit-test");
		user.setLanguage("en");
		user.setPassword("1234");
		user.setConfirmPassword("1234");
		try {
			userBO.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@After
	public void destroy() {
		try {
			Map<String, String> map = new HashMap<>();
			map.put("where username = ", "'unit-test'");

			User user = userBO.findByParameter(User.class, map);
			userBO.delete(user.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void validateUserSuccess() {
		try {
			Map<String, String> map = new HashMap<>();
			map.put("where x.username = ", "'unit-test'");

			User user = userBO.findByParameter(User.class, map);

			String id = user.getId() + "";

			Calendar registerDate = Calendar.getInstance();
			registerDate.setTime(user.getRegister());

			String date = Utils.dateToString(registerDate);

			String token = Crypt.encrypt(id + "#" + date);

			MessageReturn ret = userBO.validateNewUser(token);
			System.out.println(ret.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void loginTest(){
		MessageReturn ret = userBO.login("ander.dev","@Esportivo79");
	}
}
